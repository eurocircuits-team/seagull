﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sparrowFeApp.core
{
    [Serializable]
    public class ConfiguratorCl
    {
        public string sessionid;
        public string service;
        public string language;
        public string custid;
        public string delCountry;
        public string invCountry;
        public string status;
        public string type;
        public string ConfiguratorType;
        public string isCustomerView;
        public string currency;
        public string PerentBasketId;
        public string requestKey;
        public string IsFromECC;
        public long ECCUserId;
        public string IsException;
        public long ExceptionId;
        public string FromPage;
        public long ECCCustUserId;
        public string Unit;
        public string exchangeRate;
        public string IsStencilOnly;
        public string Deliveryterm;
        public long ArticalId;
        public string ConfigAction;
        public string IsFromExistingOrder;
        public string ConfParantNumber;
        public bool IsCalc;
        public string handlingComp;
        public string source;
        public string eagleQuery;

        #region ISerializable Members

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            info.AddValue("sessionid", sessionid);
            info.AddValue("service", service);
            info.AddValue("language", language);
            info.AddValue("custid", custid);
            info.AddValue("delCountry", delCountry);
            info.AddValue("invCountry", invCountry);
            info.AddValue("status", status);
            info.AddValue("type", type);
            info.AddValue("ConfiguratorType", ConfiguratorType);
            info.AddValue("isCustomerView", isCustomerView);
            info.AddValue("currency", currency);
            info.AddValue("requestKey", requestKey);
            info.AddValue("PerentBasketId", PerentBasketId);
            info.AddValue("IsFromECC", IsFromECC);
            info.AddValue("ECCUserId", ECCUserId);
            info.AddValue("IsException", IsException);
            info.AddValue("ExceptionId", ExceptionId);
            info.AddValue("FromPage", FromPage);
            info.AddValue("ECCCustUserId", ECCCustUserId);
            info.AddValue("Unit", Unit);
            info.AddValue("exchangeRate", exchangeRate);
            info.AddValue("IsStencilOnly", IsStencilOnly);
            info.AddValue("Deliveryterm", Deliveryterm);
            info.AddValue("ArticalId", ArticalId);
            info.AddValue("ConfigAction", ConfigAction);
            info.AddValue("IsFromExistingOrder", IsFromExistingOrder);
            info.AddValue("ConfParantNumber", ConfParantNumber);
            info.AddValue("IsCalc", IsCalc);
            info.AddValue("handlingComp", handlingComp);
            info.AddValue("source", source);
            info.AddValue("eagleQuery", eagleQuery);

            throw new NotImplementedException();
        }

        public ConfiguratorCl(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext ctxt)
        {
            sessionid = (string)info.GetValue("sessionid", typeof(string));
            service = (string)info.GetValue("service", typeof(string));
            language = (string)info.GetValue("language", typeof(string));
            custid = (string)info.GetValue("custid", typeof(string));
            delCountry = (string)info.GetValue("delCountry", typeof(string));
            invCountry = (string)info.GetValue("invCountry", typeof(string));
            invCountry = (string)info.GetValue("status", typeof(string));
            type = (string)info.GetValue("type", typeof(string));
            ConfiguratorType = (string)info.GetValue("ConfiguratorType", typeof(string));
            isCustomerView = (string)info.GetValue("isCustomerView", typeof(string));
            currency = (string)info.GetValue("currency", typeof(string));
            requestKey = (string)info.GetValue("requestKey", typeof(string));
            PerentBasketId = (string)info.GetValue("PerentBasketId", typeof(string));
            IsFromECC = (string)info.GetValue("IsFromECC", typeof(string));
            ECCUserId = (long)info.GetValue("ECCUserId", typeof(long));
            IsException = (string)info.GetValue("IsException", typeof(string));
            ExceptionId = (long)info.GetValue("ExceptionId", typeof(long));
            FromPage = (string)info.GetValue("FromPage", typeof(string));
            ECCCustUserId = (long)info.GetValue("ECCCustUserId", typeof(long));
            Unit = (string)info.GetValue("Unit", typeof(string));
            exchangeRate = (string)info.GetValue("exchangeRate", typeof(string));
            IsStencilOnly = (string)info.GetValue("IsStencilOnly", typeof(string));
            Deliveryterm = (string)info.GetValue("Deliveryterm", typeof(string));
            ArticalId = (long)info.GetValue("ArticalId", typeof(long));
            ConfigAction = (string)info.GetValue("ConfigAction", typeof(string));
            IsFromExistingOrder = (string)info.GetValue("IsFromExistingOrder", typeof(string));
            ConfParantNumber = (string)info.GetValue("ConfParantNumber", typeof(string));
            IsCalc = (bool)info.GetValue("IsCalc", typeof(bool));
            handlingComp = (string)info.GetValue("handlingComp", typeof(string));
            handlingComp = (string)info.GetValue("source", typeof(string));
            handlingComp = (string)info.GetValue("eagleQuery", typeof(string));
        }

        public ConfiguratorCl()
        {

        }
        #endregion
    }
}