﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sparrowFeApp.core
{
    public static class AppSessions
    {
        private const string _EccUserId = "EccUserId";
        private const string _CurrencySymbol = "CurrencySymbol";
        private const string _usrid = "usrid";

        public static string EccUserId
        {
            get
            {
                try
                {
                    if (HttpContext.Current.Session[_EccUserId] == null)
                        return string.Empty;
                    else
                        return HttpContext.Current.Session[_EccUserId].ToString();
                }
                catch (Exception ex)
                {

                    if (HttpContext.Current != null)
                        HttpContext.Current.Response.Redirect("~/shop/sessionexpired.aspx");
                    return "";
                }
            }
            set
            {
                try
                {
                    HttpContext.Current.Session.Add(_EccUserId, value);
                }
                catch (Exception ex)
                {

                    if (HttpContext.Current != null)
                        HttpContext.Current.Response.Redirect("~/shop/sessionexpired.aspx");

                }
            }
        }

        public static string FEUserId
        {
            get
            {
                try
                {
                    if (HttpContext.Current.Session[_usrid] == null)
                        return string.Empty;
                    else
                        return HttpContext.Current.Session[_usrid].ToString();
                }
                catch (Exception ex)
                {

                    if (HttpContext.Current != null)
                        HttpContext.Current.Response.Redirect("~/shop/sessionexpired.aspx");
                    return "";
                }
            }
            set
            {
                try
                {
                    HttpContext.Current.Session.Add(_usrid, value);
                }
                catch (Exception ex)
                {

                    if (HttpContext.Current != null)
                        HttpContext.Current.Response.Redirect("~/shop/sessionexpired.aspx");

                }
            }
        }


        public static string CurrencySymbol
        {
            get
            {
                try
                {
                    return (string)HttpContext.Current.Session[_CurrencySymbol];
                }
                catch (Exception)
                {
                    return "EUR";
                }

            }
            set
            {
                try
                {
                    HttpContext.Current.Session.Add(_CurrencySymbol, value);
                }
                catch (Exception)
                {
                    HttpContext.Current.Session.Add(_CurrencySymbol, "EUR");
                }

            }
        }
    }
}