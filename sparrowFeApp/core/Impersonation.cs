﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Web;

namespace sparrowFeApp.core
{
    public class Impersonation
    {

     
        public static WindowsImpersonationContext ImpersonateFileServer(string Username, string Password, string v)
        {
            string Domain = Constant.FileServerDomain;
            IntPtr tokenHandle = new IntPtr(0);
            int dwLogonProvider = Convert.ToInt16(Constant.DwLogonProvider);
            int dwLogonType = Convert.ToInt16(Constant.DwLogonType);
            bool returnValue = LogonUser(Username, Domain, Password, dwLogonType, dwLogonProvider, ref tokenHandle);
            WindowsIdentity ImpersonatedIdentity = new WindowsIdentity(tokenHandle);
            WindowsImpersonationContext MyImpersonation = ImpersonatedIdentity.Impersonate();
            return MyImpersonation;
        }
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

    }
}