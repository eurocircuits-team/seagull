﻿
function uncheckOthers(id) {
    var elm = document.getElementsByTagName('input');
    var IsOdd = true;
    for (var i = 0; i < elm.length; i++) {
        if (elm.item(i).id.substring(id.id.lastIndexOf('_')) == id.id.substring(id.id.lastIndexOf('_'))) {
            if (elm.item(i).type == "checkbox" && elm.item(i) != id) {


                elm.item(i).checked = false;

                elm.item(i).parentNode.parentNode.style.color = 'black';

                if (IsOdd) {
                    IsOdd = false;
                    elm.item(i).parentNode.parentNode.className = 'normal';
                }
                else {
                    IsOdd = true;
                    elm.item(i).parentNode.parentNode.className = 'alternate';

                }
            }
            else {
              
                if (IsOdd) {
                    IsOdd = false;
                }
                else {
                    IsOdd = true;
                }
                elm.item(i).checked = true;
                // elm.item(i).parentNode.parentNode.className = 'selectedrow';
                //                elm.item(i).parentNode.parentNode.style.color = '#000000';

            }
        }
    }
}

function SelectMultipleChk(id) {


    var elm = document.getElementsByTagName('input');
    var IsOdd = true;

    for (var i = 0; i < elm.length; i++) {

        if (elm.item(i).id.substring(id.id.lastIndexOf('_')) == id.id.substring(id.id.lastIndexOf('_'))) {
            if (elm.item(i).type == "checkbox" && elm.item(i) != id) {


                //elm.item(i).parentNode.parentNode.style.color = 'black';
                if (IsOdd) {
                    IsOdd = false;
                    //elm.item(i).parentNode.parentNode.className = 'normal';
                }
                else {
                    //elm.item(i).parentNode.parentNode.className = 'alternate';
                    IsOdd = true;
                }

                if (elm.item(i).checked == true) {
                   // alert("checked1");
                    if (IsOdd) {
                        IsOdd = false;
                    }
                    else {
                        IsOdd = true;
                    }
                    elm.item(i).checked = true;
                   // elm.item(i).parentNode.parentNode.className = 'selectedrow';
                }


            }
            else {

                if (elm.item(i).checked == false) {

                    elm.item(i).checked = false;
                    if (IsOdd) {
                        IsOdd = false;
                        //elm.item(i).parentNode.parentNode.className = 'normal';
                    }
                    else {
                        //elm.item(i).parentNode.parentNode.className = 'alternate';
                        IsOdd = true;
                    }
                }
                else {
                    //alert("checked2");
                    if (IsOdd) {
                        IsOdd = false;
                    }
                    else {
                        IsOdd = true;
                    }

                    elm.item(i).checked = true;
                    //elm.item(i).parentNode.parentNode.className = 'selectedrow';
                }

                //                elm.item(i).parentNode.parentNode.style.color = '#000000';

            }

        }
    }


}





function ValidateCheckBox(id, lbl, msg) {
    
    var TargetBaseControl = document.getElementById('ctl00_mainContent_' + id);

    if (TargetBaseControl == null) return false;

    var TargetChildControl = "chkSelect";

    var Inputs = TargetBaseControl.getElementsByTagName("input");
    
    for (var n = 0; n < Inputs.length; ++n)
        if (Inputs[n].type == 'checkbox' &&
            Inputs[n].id.indexOf(TargetChildControl, 0) >= 0 &&
            Inputs[n].checked) {
            
            document.getElementById('ctl00_mainContent_' + lbl).innerHTML = "";
            document.getElementById('ctl00_mainContent_' + lbl).value = "";
            
            return true;
        }

    document.getElementById('ctl00_mainContent_' + lbl).innerHTML = msg;
    document.getElementById('ctl00_mainContent_' + lbl).value = msg;

    return false;
}


function ValidateCheckBoxFordelete(id, lbl, msg, msgdelete) {

    var TargetBaseControl = document.getElementById('ctl00_mainContent_' + id);

    if (TargetBaseControl == null) return false;

    var TargetChildControl = "chkSelect";

    var Inputs = TargetBaseControl.getElementsByTagName("input");

    for (var n = 0; n < Inputs.length; ++n)
        if (Inputs[n].type == 'checkbox' &&
            Inputs[n].id.indexOf(TargetChildControl, 0) >= 0 &&
            Inputs[n].checked) {
            document.getElementById('ctl00_mainContent_' + lbl).innerHTML = "";
            document.getElementById('ctl00_mainContent_' + lbl).value = "";
            return confirm(msgdelete);

        }

    document.getElementById('ctl00_mainContent_' + lbl).innerHTML = msg;
    document.getElementById('ctl00_mainContent_' + lbl).value = msg;

    return false;
}



function OpenQna(qnapath) {


    window.open(qnapath, '', 'resizable=yes,width=600,height=450,top=300,left=300,toolbar=no,menubar=no,status=yes,scrollbars=yes');

}








var counter = 0;
var pattern = '';
function GetGrid(Grid) {
    counter = 0;
    pattern = Grid;

}

// Get the checkboxes inside the Gridview which is part of the template column
function GetChildCheckBoxCount() {
    var checkBoxCount = 0;

    var elements = document.getElementsByTagName("INPUT");
    //alert(elements);

    for (i = 0; i < elements.length; i++) {
        if (IsCheckBox(elements[i]) && IsMatch(elements[i].id)) checkBoxCount++;
    }

    return parseInt(checkBoxCount);
}

// A function that checks if the checkboxes are the one inside the GridView 
function IsMatch(id) {
    var regularExpresssion = new RegExp(pattern);

    if (id.match(regularExpresssion)) return true;
    else return false;
}

function IsCheckBox(chk) {
    if (chk.type == 'checkbox')
        return true;
    else
        return false;
}


function AttachListener() {
    var elements = document.getElementsByTagName("INPUT");
    //alert("elements");
    for (i = 0; i < elements.length; i++) {
        if (IsCheckBox(elements[i]) && IsMatch(elements[i].id)) {
            AddEvent(elements[i], 'click', CheckChild);

        }
    }
}

function CheckChild(e) {
    var evt = e || window.event;

    var obj = evt.target || evt.srcElement

    if (obj.checked) {
        if (counter < GetChildCheckBoxCount()) {
            counter++;
        }
    }

    else {
        if (counter > 0) {
            counter--;
        }
    }

    if (counter == GetChildCheckBoxCount()) {
        //alert(counter);
        document.getElementById("chkAll").checked = true;
    }
    else if (counter < GetChildCheckBoxCount()) {
        document.getElementById("chkAll").checked = false;
    }

}

function AddEvent(obj, evType, fn) {
    if (obj.addEventListener) {
        obj.addEventListener(evType, fn, true);
        return true;
    }

    else if (obj.attachEvent) {
        var r = obj.attachEvent("on" + evType, fn);
        return r;
    }
    else {
        return false;
    }
}

function Check(parentChk) {
    var elements = document.getElementsByTagName("INPUT");
    for (i = 0; i < elements.length; i++) {
        if (parentChk.checked == true) {

            if (IsCheckBox(elements[i]) && IsMatch(elements[i].id)) {
                elements[i].checked = true;
            }
        }
        else {

            elements[i].checked = false;
            // reset the counter 
            counter = 0;
        }
    }

    if (parentChk.checked == true) {
        counter = GetChildCheckBoxCount();
    }

}

function trackEvent(Number, ObjEventDetail) {

 
    //var EventTypeID = '';
    //var EventTypeValue = '';
    //if (ObjEventDetail != null)
    //{
    //    EventTypeID = ObjEventDetail.EventTypeID;
    //    EventTypeValue = ObjEventDetail.EventTypeValue;
    //}
    //var M = '';
    //var pltform = '';
    //navigator.sayswho = (function () {
    //    var N = navigator.appName, ua = navigator.userAgent, tem;
    //    M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    //    if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
    //    M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
    //    pltform = navigator.platform;
    //})();
    //var brw = M.toString();
    //$.ajax({
    //    type: 'POST',
    //    url: "../../EC09WebService/Order.asmx/TrackEvent",
    //    data: {
    //        "Number": Number, "EventTypeID": EventTypeID, "EventTypeValue": EventTypeValue, "OS": pltform, "Browser": brw
    //    },
    //});
}
        
var BOMUtility = {
    sanatizeKeywordJSON: function (keyword) {
        var sanatizedObj = BOMUtility.sanatizeKeyword(keyword, true);
        return JSON.stringify(sanatizedObj)
    },
    sanatizeKeyword: function (keyword, manualSearch) {
        /*
            Resistance: Gigaohm (GΩ), Kiloohm (kΩ), Megaohm (MΩ), Microohm (µΩ), Milliohm (mΩ), Nanoohm (nΩ), Ohm (Ω), Volt per ampere (V/A)
            Capacitance: Picofarad (pF), Nanofarad (nF), Microfarad (µF),
        */
        var sanatizedObj = {
            specs: [],
            keyword: ""
        };

        keyword = keyword.replace(/µ/g, "u")
            .replace(/\u03BC/g, "u")
            .replace(/\u2126/g, "ohm")
            .replace(/\±/g, "")
            .replace(/°/g, "");

        if (!manualSearch) {
            keyword = keyword.replace(/[`~!@#$^()_|+\-=?;:'"<>\{\}\[\]\\]/gi, " ");
        }

        var sWords = keyword.match(/\S+/g) || [];

        if (sWords.length == 0) {
            return sanatizedObj;
        }

        var packages = ['0402', '0603', '0805', '402', '603', '805', '1206'];
        var dielectric = ['c0g', 'x5r', 'x7r'];

        //Remove white space between value and unit        
        for (var i = sWords.length - 1; i >= 0 ; i--) {
            var searchWord = sWords[i];
            var value = 0.0;
            var strVal = "";

            var capacitanceUnits = searchWord.match(/^[p|n|u]{1}(f)(,)?$/gi) || [];
            if (capacitanceUnits.length > 0 && sWords[i - 1]) {
                //Handle comma decimal system                
                strVal = sWords[i - 1].replace(",", ".");
                value = isNaN(strVal) == false ? parseFloat(strVal) : 0;

                if (value != 0) {
                    sWords[i - 1] = value + capacitanceUnits[0];
                    sWords.splice(i, 1);
                    continue;
                }
            }

            var resistanceUnits = searchWord.match(/^(((k|m|g)?(ohm|ohms))|(k|m|g|r))(,)?$/gi) || [];
            if (resistanceUnits.length > 0 && sWords[i - 1]) {
                strVal = sWords[i - 1].replace(",", ".");
                value = isNaN(strVal) == false ? parseFloat(strVal) : 0;

                //Donot take package value with "R" combination as resistance value
                //For instance 0402_R, 0402 R should not taken as resistance value
                var isPackageValue = false;
                if (packages.indexOf(strVal) != -1 && resistanceUnits[0].toLowerCase() == "r") {
                    isPackageValue = true;
                }

                if (!isPackageValue && value != 0) {
                    sWords[i - 1] = value + resistanceUnits[0];
                    sWords.splice(i, 1);
                    continue;
                }
            }

            // var powerUnits = searchWord.match(/^((k|m|g)?(w))(,)?$/gi) || [];
            // if(powerUnits.length > 0 && sWords[i-1]) {
            //     strVal = sWords[i-1].replace(",", ".");                 
            //     value = parseFloat(strVal) || 0;         
            //     if(value != 0) {
            //         sWords[i-1] = value + powerUnits[0];                                                               
            //         sWords.splice(i, 1);
            //         continue;
            //     }
            // }  

            var voltUnits = searchWord.match(/^(v)(,)?$/gi) || [];
            if (voltUnits.length > 0 && sWords[i - 1]) {
                strVal = sWords[i - 1].replace(",", ".");
                value = parseFloat(strVal) || 0;
                if (value != 0) {
                    sWords[i - 1] = value + voltUnits[0];
                    sWords.splice(i, 1);
                    continue;
                }
            }

            var tolUnits = searchWord.match(/^(%)(,)?$/gi) || [];
            if (tolUnits.length > 0 && sWords[i - 1]) {
                strVal = sWords[i - 1].replace(",", ".");
                value = parseFloat(strVal) || 0;
                if (value != 0) {
                    sWords[i - 1] = value + tolUnits[0];
                    sWords.splice(i, 1);
                    continue;
                }
            }
        }

        //Make unique word list
        var searchWords = [];
        $.each(sWords, function (i, el) {
            //Ignore single char word. It does not have any meaning in search.
            if (el.length > 1) {
                if ($.inArray(el, searchWords) === -1) searchWords.push(el);
            }
        });

        //Generate object based on Cap, Res, Package, Voltage value 
        for (var i = 0; i < searchWords.length; i++) {
            var searchWord = searchWords[i];

            if (searchWord === "") {
                continue;
            }
            searchWord = searchWord.toLowerCase();
            var specValue = "";
            var spec = "";
            var unit = "";

            //Only check if word length is <= 5 to avoid wrong search for MPN which contains package word.
            if (searchWord.length <= 5) {
                for (var pi = 0; pi < packages.length; pi++) {
                    if (searchWord.indexOf(packages[pi]) != -1) {
                        specValue = packages[pi];

                        if (specValue == "402" || specValue == "603" || specValue == "805") {
                            specValue = "0" + specValue;
                        }

                        spec = "pack";
                        break;
                    }
                }
            }

            //Only check if word length is <= 3 to avoid wrong search for MPN which contains dielectric word.
            if (searchWord.length <= 3) {
                for (var di = 0; di < dielectric.length; di++) {
                    if (searchWord.indexOf(dielectric[di]) != -1) {
                        specValue = dielectric[di];
                        spec = "diel";
                        break;
                    }
                }
            }

            /*
             * () represent the group
             * ? is used after group when it is optional
             * $ is used to to end expression
             * (\.|\,) It will check decimal system by "," or ".".
             */
            //Farad notation should available            

            //Cap input: 1.2nf            
            var capacitanceCase1 = searchWord.match(/^[0-9]+((\.|\,)[0-9]{1,3})?(((p|n|u)?(f))|(p|n|u))(,)?$/gi);
            //Cap input: 1n2
            var capacitanceCase2 = searchWord.match(/^[0-9]+((p|n|u)[0-9]{1,3})(,)?$/gi);

            if (capacitanceCase1 != null || capacitanceCase2 != null) {
                spec = "cap";

                if (capacitanceCase1 != null) {
                    capacitanceCase1[0] = capacitanceCase1[0].replace(/,/, ".");

                    specValue = capacitanceCase1[0].replace(/[^0-9(\.|\,)]+/g, function (x) {
                        unit = x;
                        return "";
                    });
                    // unit = capacitanceCase1[0].split(specValue)[1] || "";                                
                }
                else {
                    var capCase2Data = capacitanceCase2[0].split(/(p|n|u)/g);
                    specValue = capCase2Data[0] + '.' + capCase2Data[2];
                    unit = capCase2Data[1];
                }

                if (specValue != "") {
                    specValue = parseFloat(specValue) || 0;
                }

                if (unit.indexOf("f") == -1) {
                    unit = unit + 'f';
                }
            }

            //Voltage
            var voltage = searchWord.match(/^[0-9]+((\.|\,)[0-9]{1,3})?(v)(,)?$/gi)
            if (voltage != null) {
                specValue = voltage[0];
                if (specValue != "") {
                    specValue = parseFloat(specValue) || 0;
                }
                spec = "volt";
                unit = "v"
            }

            //Resistance value with "ohm/ohms" or with "kohm|mohm|gohm"
            var resistanceCase1 = searchWord.match(/^[0-9]+((\.|\,)[0-9]{1,3})?(((k|m|g)?(ohm|ohms))|(r|k|m|g))(,)?$/gi);
            var resistanceCase2 = searchWord.match(/^[0-9]+((r|k|m|g)[0-9]{1,3})(,)?$/gi);

            if (resistanceCase1 != null || resistanceCase2 != null) {
                spec = "res";

                if (resistanceCase1 != null) {
                    resistanceCase1[0] = resistanceCase1[0].replace(/,/, ".");
                    // e.g. Value = 1,2k, and find unit from this value so simply replace blank instaed of digit|comma|dot so here x is unit value.
                    specValue = resistanceCase1[0].replace(/[^0-9(\.|\,)]+/g, function (x) {
                        unit = x;
                        return "";
                    });
                    // unit = resistanceCase1[0].split(specValue)[1] || "";  
                }
                else {
                    var resCase2Data = resistanceCase2[0].split(/(r|k|m|g)/g);
                    specValue = resCase2Data[0] + '.' + resCase2Data[2];
                    unit = resCase2Data[1];
                }

                if (specValue != "") {
                    specValue = parseFloat(specValue) || 0;
                }

                unit = unit.replace("ohms", "ohm");

                if (unit == "r") {
                    unit = "ohm";
                }
                if (unit.indexOf("ohm") == -1) {
                    unit = unit + 'ohm';
                }
            }

            // //Power ratting value with "w" or with "kw|mw|gw"
            // var powerRatting = searchWord.match(/^[0-9]+((\.|\,)[0-9]{1,3})?((k|m|g)?(w))(,)?$/gi);
            // if (powerRatting != null) {
            //     specValue = powerRatting[0];
            //     spec = "res";
            // }            

            //Tolerance
            var tolerance = searchWord.match(/^(\+|-|±)?[0-9]+((\.|\,)[0-9]{1,2})?%(,)?$/gi);
            if (tolerance != null) {
                specValue = parseFloat(tolerance[0].replace(/%,/, "").substring(0, tolerance[0].length - 1).replace(/,/, "."));
                spec = "tol";
                unit = "%";
            }

            if (specValue != "") {
                sanatizedObj.specs.push({
                    "value": specValue,
                    "spec": spec,
                    "unit": unit
                });
                sanatizedObj.keyword += specValue + unit + " ";
            }
            else {
                sanatizedObj.keyword += searchWord + " ";
            }
        }

        return sanatizedObj;
    }
}

function PCBVisTooltip(gridId,childPosition,startsWith) {
    $("[id" + startsWith + "=" + gridId + "] tbody tr td:nth-child(" + childPosition + ") div").hover(function () {
        var assembly_des = $(this).closest("tr").children().find('.tooltip').attr('assembly_desc');
        var hklPCBVisualizer = $(this).closest("tr").children().find('.hklPCBVisualizer').text();
        if (hklPCBVisualizer != "" && assembly_des != "") {
            var isBOM = $(this).closest("tr").children().find('.cls_noBOM').attr('is_BOM_part');
            if (isBOM == 0) {
                $(this).closest("tr").children().find('.BOM_seperator').css('height', '30px');
                $(this).closest("tr").children().find('#divAsm_Desc_Vis').show();//fadeIn(200)
            }
            else {
                $(this).closest("tr").children().find('#divAsm_Desc_Vis').show();//fadeIn(200)
            }
        }
        if (hklPCBVisualizer != "" && assembly_des == "") {
            $(this).closest("tr").children().find('.BOM_seperator').css('height', '30px');
            $(this).closest("tr").children().find('#divAsm_Desc_Vis').show()//fadeIn(200)
        }
    }, function () {
        $(this).closest("tr").children().find('#divAsm_Desc_Vis').hide();
    });
    $('.tooltip_close_position').on('click', function () {
        $(this).closest("tr").children().find('#divAsm_Desc_Vis').hide();
    });
}