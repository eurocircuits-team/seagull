﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Runtime.InteropServices" %> 
<%@ Import Namespace="System.IO" %>
<script language="c#" runat="server">

    const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16"; 
    string configurl = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CofiguratorUrl"]);
    public void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod.ToString().Trim().ToUpper() == "POST")
        {
            try
            {
                string responsecontent = "";
                if (Request.QueryString["key"] != null && Request["FileContent"] != null)// 
                {
                    responsecontent = Request["FileContent"].ToString();
                    downloadcomponentsfile(Request.QueryString["key"], responsecontent);
                    return;
                }
                else {
                     Response.Write("FAILED");
                }
            }
            catch { }
        }
    }
    private void downloadcomponentsfile(string fileName, string fileContent)
    {
        try
        {
                byte[] byteArray = Encoding.UTF8.GetBytes(fileContent);
                MemoryStream stream = new MemoryStream(byteArray);
                Response.Clear();
                Response.ContentType = "application/download";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "");
                Response.BinaryWrite(byteArray);
        }
        catch 
        { 
            Response.Write("FAILED");
       
        }
    }
</script>
