﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml;
using System.Configuration;

namespace sparrowFeApp.shop.assembly
{   public class ComponentPrice
    {
        private List<Dictionary<string, object>> reqPriceList = new List<Dictionary<string, object>>();

        public string ComponentXMLPath { get; set; }

        public Exception Exception { get; set; }

        public dynamic PriceObj { get; set; }

        public string latestPrice { get; set; }

        public ComponentPrice(string latestPrice)
        {
            this.latestPrice = latestPrice;
        }

        public void AddComponent(string mpn, int orderQty, int prodQty, string isGeneric, string category)
        {
            Dictionary<string, object> reqPriceData = new Dictionary<string, object>();
            reqPriceData.Add("mpn", mpn);
            reqPriceData.Add("ordQty", orderQty);
            reqPriceData.Add("prodQty", prodQty);
            reqPriceData.Add("isGen", isGeneric);
            reqPriceData.Add("cat", category);
            reqPriceList.Add(reqPriceData);
        }

        public XmlDocument UpdatePrice(XmlDocument xmlDoc, string partSelector, string preferedSupplier, string orderNumber)
        {
            if (reqPriceList.Count == 0)
            {
                return xmlDoc;
            }
            
            string reqPriceJson = JsonConvert.SerializeObject(reqPriceList);
            Dictionary<string, object> priceParameters = new Dictionary<string, object>();
            priceParameters.Add("data", reqPriceJson);
            priceParameters.Add("latest_price", latestPrice);
            priceParameters.Add("source_ref", orderNumber);
            priceParameters.Add("supplier_preference", preferedSupplier);

            string priceResult = Component.GetDataFromSparrow("get_parts_price", priceParameters);
            dynamic priceObj = JsonConvert.DeserializeObject(priceResult);


            xmlDoc = UpdateXMLDoc(xmlDoc, priceObj, partSelector);
            return xmlDoc;
        }
        public XmlDocument UpdatePriceV1(XmlDocument xmlDoc, string partSelector, string preferedSupplier, string orderNumber)
        {
            if (reqPriceList.Count == 0)
            {
                return xmlDoc;
            }
            string reqPriceJson = JsonConvert.SerializeObject(reqPriceList);
            Dictionary<string, object> priceParameters = new Dictionary<string, object>();
            priceParameters.Add("data", reqPriceJson);
            priceParameters.Add("latest_price", latestPrice);
            priceParameters.Add("source_ref", orderNumber);
            priceParameters.Add("supplier_preference", preferedSupplier);

            string priceResult = Component.GetDataFromSparrow("get_parts_price_v1", priceParameters);

            PartPriceData obj = new PartPriceData();
            obj = JsonConvert.DeserializeObject<PartPriceData>(priceResult);
            XmlDocument docNew = new XmlDocument();
            XmlElement Parts = docNew.CreateElement("Parts");
            docNew.AppendChild(Parts);
            XmlElement Part;
            Part = docNew.CreateElement("Part");
            XmlElement Supplier;
            XmlElement SPN;
            if (obj.Code == 1)
            {
                //string sql = "";
                foreach (KeyValuePair<string, SPLPartData[]> ele1 in obj.Data)
                {
                    Part = docNew.CreateElement("Part");
                    Part.SetAttribute("mpn", ele1.Key);
                    XmlNodeList nodes = xmlDoc.SelectNodes(partSelector);
                    Part.SetAttribute("gpn", "0");
                    foreach (XmlNode node in nodes)
                    {
                        XmlElement xmlElement = (XmlElement)node;
                        string mpn = xmlElement.Attributes["mpn"].Value;
                        if (ele1.Key.ToUpper() == mpn.ToUpper())
                        {
                            Part.SetAttribute("gpn", xmlElement.Attributes["gpn"].Value);
                            break;
                        }
                    }
                    foreach (SPLPartData data in ele1.Value)
                    {
                        Supplier = docNew.CreateElement("Supplier");
                        Supplier.SetAttribute("name", data.Suppl);
                        SPN = docNew.CreateElement("SPN");
                        SPN.SetAttribute("sku", data.Sku);
                        SPN.SetAttribute("val", data.ExpiredOn);
                        SPN.SetAttribute("stock", data.Stock);
                        SPN.SetAttribute("ordQty", data.OrdQty);
                        SPN.SetAttribute("prodQty", data.ProdQty);
                        SPN.SetAttribute("selected", data.Selected.ToString());
                        SPN.SetAttribute("purchQty", data.PurchQty);
                        SPN.SetAttribute("moq", data.Moq);
                        SPN.SetAttribute("price", data.Price);
                        SPN.SetAttribute("status", data.Status);
                        bool selected = false;
                        bool.TryParse(data.Selected.ToString(), out selected);
                        if (selected)
                        {
                            Part.SetAttribute("suppl", data.Suppl);
                            Part.SetAttribute("sku", data.Sku);
                            Part.SetAttribute("val", data.ExpiredOn);
                            Part.SetAttribute("stock", data.Stock);
                            Part.SetAttribute("ordQty", data.OrdQty);
                            Part.SetAttribute("prodQty", data.ProdQty);
                            Part.SetAttribute("purchQty", data.PurchQty);
                            Part.SetAttribute("moq", data.Moq);
                            Part.SetAttribute("price", data.Price);
                            Part.SetAttribute("status", data.Status);
                            //if (data.Price.ToString() == "-1" && data.Status == "")
                            //{
                            //    sql += string.Format("exec sp_InsertPartMissingPrice '{0}','{1}','{2}' ", ele1.Key, "", orderNumber) + ";";
                            //}
                        }
                        Supplier.AppendChild(SPN);
                        Part.AppendChild(Supplier);
                    }
                    Parts.AppendChild(Part);
                }
                //if (sql != "")
                //{
                //    //EC09WebApp.API.gd objAPI = new EC09WebApp.API.gd();
                //    var objgd = new Database.DA();
                //    DataSet dsOrderRC = objgd.GetDataSet(sql);
                //}
            }
            return docNew;
        }

        public XmlDocument UpdateXMLDoc(XmlDocument xmlDoc, dynamic priceObj, string partSelector)
        {
            foreach (dynamic item in priceObj.data)
            {
                string mpn = item.mpn;
                string orderQty = item.ord_qty;
                XmlNodeList nodes = xmlDoc.SelectNodes(partSelector + "[@mpn='" + mpn + "' and @ordQty='" + orderQty + "']");
                foreach (XmlNode node in nodes)
                {
                    XmlElement xmlElement = (XmlElement)node;
                    xmlElement.SetAttribute("prodQty", item.prod_qty.ToString());
                    xmlElement.SetAttribute("purchQty", item.purch_qty.ToString());
                    xmlElement.SetAttribute("moq", item.moq.ToString());
                    xmlElement.SetAttribute("price", item.price.ToString());
                    xmlElement.SetAttribute("suppl", item.suppl.ToString());
                    xmlElement.SetAttribute("sku", item.sku.ToString());
                    xmlElement.SetAttribute("val", item.expired_on.ToString());
                    xmlElement.SetAttribute("stock", item.stock.ToString());
                    xmlElement.SetAttribute("havePrice", "1");
                }
            }
            
            XmlNodeList matchNodes = xmlDoc.SelectNodes(partSelector);
            foreach (XmlNode node in matchNodes)
            {
                XmlElement xmlElement = (XmlElement)node;
                if(xmlElement.GetAttribute("havePrice") == "1")
                {
                    xmlElement.RemoveAttribute("havePrice");                    
                }
                else
                {                    
                    xmlElement.SetAttribute("price", "0");                    
                }
            }

            return xmlDoc;
        }


    }

    public partial class PartPriceData
    {
        [JsonProperty("code")]
        public long Code { get; set; }

        [JsonProperty("data")]
        public Dictionary<string, SPLPartData[]> Data { get; set; }
    }
    public partial class SPLPartData
    {
        [JsonProperty("selected")]
        public bool Selected { get; set; }

        [JsonProperty("ord_qty")]
        public string OrdQty { get; set; }

        [JsonProperty("mpn")]
        public string Mpn { get; set; }

        [JsonProperty("moq")]
        public string Moq { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("prod_qty")]
        public string ProdQty { get; set; }

        [JsonProperty("purch_qty")]
        public string PurchQty { get; set; }

        [JsonProperty("expired_on")]
        public string ExpiredOn { get; set; }

        [JsonProperty("suppl")]
        public string Suppl { get; set; }

        [JsonProperty("unit_price")]
        public string UnitPrice { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("stock")]
        public string Stock { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
};