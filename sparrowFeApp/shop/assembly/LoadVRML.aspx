﻿<%@ Page Language="C#" AutoEventWireup="true" Debug="true" %>    
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">
    const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
    public static bool isLive = Convert.ToBoolean(ConfigurationManager.AppSettings["IsLive"]);
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string mpn =  HttpUtility.UrlEncode(Request.QueryString["mpn"]);
            if(mpn.Trim() == "")
            {
                Response.Write("");
            }

            string postData = "mpn" + "=" +  mpn;
            var wrlURL = ConfigurationManager.AppSettings["seagullAppAPI"].ToString() + "/eda/" + "get_package_wrl/";
                                    
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(wrlURL);
            myHttpWebRequest.UseDefaultCredentials = true;
            myHttpWebRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            myHttpWebRequest.Method = "POST";
            byte[] data = Encoding.ASCII.GetBytes(postData);
            myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
            myHttpWebRequest.ContentLength = data.Length;

            using (Stream requestStream = myHttpWebRequest.GetRequestStream())
            {
                requestStream.Write(data, 0, data.Length);                
            }            

            using(HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse())
            {
                using(Stream responseStream = myHttpWebResponse.GetResponseStream())
                {
                    using(StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default))
                    {
                        string pageContent = myStreamReader.ReadToEnd();
                        byte[] datas = Encoding.ASCII.GetBytes(pageContent);
                        Response.BinaryWrite(datas);          
                    }
                }                
            }
        }
        catch
        {
            Response.Write("");
        }
    }
</script>
