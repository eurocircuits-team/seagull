﻿<%@ Page Language="C#" AutoEventWireup="true" Debug="true"  %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="sparrowFeApp.shop.assembly" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        string mpn = (Request.QueryString["mpn"] ?? "").ToLower();

        Dictionary<string, object> parameters = new Dictionary<string, object>();
        parameters.Add("mpn", mpn);

        string result = Component.GetDataFromSparrow("equivalent_search", parameters);
        XmlDocument xmlDoc = (XmlDocument)JsonConvert.DeserializeXmlNode(result.ToString(), "data");

        XmlDocument docNew = new XmlDocument();
        XmlElement partsElement = docNew.CreateElement("parts");
        docNew.AppendChild(partsElement);

        XmlNodeList genericNodes = xmlDoc.SelectNodes("data/data/part/gen_parts");
        if (genericNodes.Count > 0)
        {
            XmlElement genericElement = docNew.CreateElement("generics");
            partsElement.AppendChild(genericElement);

            foreach (XmlNode genericNode in genericNodes)
            {
                XmlElement genericPartElement = GetPartElement(docNew, genericNode, "generic_part");
                genericElement.AppendChild(genericPartElement);

                XmlNodeList physicalNodes = genericNode.SelectNodes("phy_parts");
                foreach (XmlNode physicalNode in physicalNodes)
                {
                    XmlElement physicalPartElement = GetPartElement(docNew, physicalNode, "part");
                    genericPartElement.AppendChild(physicalPartElement);
                }
            }
        }

        XmlNodeList equivalentNodes = xmlDoc.SelectNodes("data/data/part/eq_parts");
        if (equivalentNodes.Count > 0)
        {
            XmlElement equivalentElement = docNew.CreateElement("equivalents");
            partsElement.AppendChild(equivalentElement);

            foreach (XmlNode equivalentNode in equivalentNodes)
            {
                XmlElement equivalentPartElement = GetPartElement(docNew, equivalentNode, "part");
                equivalentElement.AppendChild(equivalentPartElement);
            }
        }

        Response.ContentType = "text/xml";
        Response.Write(docNew.OuterXml);
    }

    private XmlElement GetPartElement(XmlDocument doc, XmlNode partNode, string elementName)
    {
        XmlElement partElement = doc.CreateElement(elementName);
        partElement.SetAttribute("mpn", partNode.SelectSingleNode("mpn").InnerText);
        partElement.SetAttribute("price", partNode.SelectSingleNode("price").InnerText);
        partElement.SetAttribute("stock", partNode.SelectSingleNode("stock").InnerText);
        partElement.SetAttribute("suppl", partNode.SelectSingleNode("suppliers").InnerText);
        //partElement.SetAttribute("spn", partNode.SelectSingleNode("spn").InnerText);
        partElement.SetAttribute("ver", partNode.SelectSingleNode("verified").InnerText);
        partElement.SetAttribute("desc", partNode.SelectSingleNode("descr").InnerText);
        partElement.SetAttribute("ipc", partNode.SelectSingleNode("ipc_name").InnerText);
        partElement.SetAttribute("url", partNode.SelectSingleNode("data_url").InnerText);
        partElement.SetAttribute("img", partNode.SelectSingleNode("image").InnerText);
        partElement.SetAttribute("manuf", partNode.SelectSingleNode("manuf").InnerText);
        partElement.SetAttribute("gpn", partNode.SelectSingleNode("is_generic").InnerText);
        return partElement;
    }
</script>
