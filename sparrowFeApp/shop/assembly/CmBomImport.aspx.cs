﻿using EC09WebApp.shop.rb;
using Newtonsoft.Json;
using sparrowFeApp.core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Xml;

namespace sparrowFeApp.shop.assembly
{
    public partial class CmBomImport : sparrowFeApp.core.BasePage 
    {
        //static ecgdService.gdSoapClient objgd = new ecgdService.gdSoapClient(); 
        const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";

        protected void Page_Load(object sender, EventArgs e)
        {
            ResourceId = 90;
            if (!IsPostBack)
            {
                Dictionary<string, string> captions = SetLanguageData();
                DataTable bomColumns = sparrowFeApp.core.Common.GetBOMColumns(1);
                //DataSet dsColumns = new DataSet();
                //dsColumns.Tables.Add(bomColumns);
                //dsColumns.WriteXml(Server.MapPath("\\shop\\visualizer\\data\\bomColumns1.xml"));
                string noAlternatives = Request.QueryString["assemblyNoAlternatives"];
                string data = JsonConvert.SerializeObject(new { BOMCols = bomColumns, Captions = captions, NoAlternatives = noAlternatives });
                txtData.Value = data;
                Session["visSession"] = Request.QueryString["Session_id"] ?? "";
            }
        }

        private Dictionary<string, string> SetLanguageData()
        {
            Dictionary<string, string> captions = new Dictionary<string, string>();
            captions.Add("RequiredMappingColumns", GetLocalResourceObject("RequiredMappingColumns"));
            captions.Add("Error", GetLocalResourceObject("Error"));
            captions.Add("ErrorInFileUpload", GetLocalResourceObject("ErrorInFileUpload"));
            captions.Add("RequiredFile", GetLocalResourceObject("RequiredFile"));
            captions.Add("RequiredDelimitor", GetLocalResourceObject("RequiredDelimitor"));
            captions.Add("StartRowGraterHeaderRow", GetLocalResourceObject("StartRowGraterHeaderRow"));
            captions.Add("EndRowGraterStartRow", GetLocalResourceObject("EndRowGraterStartRow"));
            captions.Add("RequireOrderOrBasket", GetLocalResourceObject("RequireOrderOrBasket"));
            captions.Add("RowNumber", GetLocalResourceObject("RowNumber"));
            captions.Add("RowNumberContainsInvalidData", GetLocalResourceObject("RowNumberContainsInvalidData"));
            captions.Add("UploadNewBOM", GetLocalResourceObject("UploadNewBOM"));
            captions.Add("Back", GetLocalResourceObject("Back"));
            captions.Add("Next", GetLocalResourceObject("Next"));
            captions.Add("Submit", GetLocalResourceObject("Submit"));
            captions.Add("Cancel", GetLocalResourceObject("Cancel"));
            captions.Add("FileSizeLong", GetLocalResourceObject("MsgFileSizeLongOrderFail.Text"));
            captions.Add("MsgBOMFileSelect", GetLocalResourceObject("MsgBOMFileSelect.Text"));
            captions.Add("ConfirmMessage", GetLocalResourceObject("ConfirmMessage"));
            captions.Add("Save", GetLocalResourceObject("Save"));
            captions.Add("FileSizeLess300", GetLocalResourceObject("MsgBOMFileSizeLong.Text"));
            captions.Add("InvalidSuplyData", GetLocalResourceObject("InvalidSuplyData.Text"));

            btnOverwrite.Value = GetLocalResourceObject("reloadBOM");
            btnCancel.InnerText = GetLocalResourceObject("uploadNew");
            confirmFileMsg.InnerText = GetLocalResourceObject("BOMXMLExist");
            ctl00_lblLoading.InnerText = GetLocalResourceObject("ctl00_lblLoading.Text");
            h3BomWorkSheet.InnerText = GetLocalResourceObject("h3BomWorkSheet.Text");
            divMulWorkSheet.InnerText = GetLocalResourceObject("divMulWorkSheet.Text");
            h3BomDataFile.InnerText = GetLocalResourceObject("h3BomDataFile.Text");
            spnBrowse.InnerText = GetLocalResourceObject("spnBrowse.Text");
            divAccepFormat.InnerText = GetLocalResourceObject("divAccepFormat.Text");
            h3ChooseFileType.InnerText = GetLocalResourceObject("h3ChooseFileType.Text");
            lblelimited.InnerText = GetLocalResourceObject("lblelimited.Text");
            lblFixedWidth.InnerHtml = GetLocalResourceObject("lblFixedWidth.Text");
            h3ChooseDel.InnerText = GetLocalResourceObject("h3ChooseDel.Text");
            lblTab.InnerText = GetLocalResourceObject("lblTab.Text");
            lblSemicolon.InnerText = GetLocalResourceObject("lblSemicolon.Text");
            lblPipe.InnerText = GetLocalResourceObject("lblPipe.Text");
            lblComma.InnerText = GetLocalResourceObject("lblComma.Text");
            lblSpace.InnerText = GetLocalResourceObject("lblSpace.Text");
            lblOther.InnerText = GetLocalResourceObject("lblOther.Text");
            lblseeHow.InnerText = GetLocalResourceObject("lblseeHow.Text");
            h3ChooseScope.InnerText = GetLocalResourceObject("h3ChooseScope.Text");
            divDataHeader.InnerText = GetLocalResourceObject("divDataHeader.Text");
            divDataStartFrom.InnerText = GetLocalResourceObject("divDataStartFrom.Text");
            divDataEnds.InnerText = GetLocalResourceObject("divDataEnds.Text");
            divChooseColumns.InnerText = GetLocalResourceObject("divChooseColumns.Text");
            divMapping.InnerText = GetLocalResourceObject("divMapping.Text");
            divUnitOfMeasureX.InnerText = GetLocalResourceObject("divUnitOfMeasureX.Text");
            divUnitOfMeasureY.InnerText = GetLocalResourceObject("divUnitOfMeasureY.Text");
            divMPN.InnerText = GetLocalResourceObject("divMPN.Text");
            divDefineMount.InnerText = GetLocalResourceObject("divDefineMount.Text");
            divDefineLayers.InnerText = GetLocalResourceObject("divDefineLayers.Text");
            divDefinePeriod.InnerText = GetLocalResourceObject("divDefinePeriod.Text");
            optInch_X.InnerText = GetLocalResourceObject("optInch_X.Text");
            optMM_X.InnerText = GetLocalResourceObject("optMM_X.Text");
            optInch_Y.InnerText = GetLocalResourceObject("optInch_Y.Text");
            optMM_Y.InnerText = GetLocalResourceObject("optMM_Y.Text");
            optDays.InnerText = GetLocalResourceObject("optDays.Text");
            optWeeks.InnerText = GetLocalResourceObject("optWeeks.Text");
            divNoFileChoosen.InnerText = GetLocalResourceObject("divNoFileChoosen.Text");
            divDataHeaderAll.InnerHtml = GetLocalResourceObject("divDataHeaderAll.Text");
            h5EnterDataPdfOrDoc.InnerHtml = GetLocalResourceObject("h5EnterDataPdfOrDocBOM.Text");
            btnSubmitPdfDoc.Value = GetLocalResourceObject("btnSubmitPdfDoc.Text");
            editBOM.Value = GetLocalResourceObject("EditBOM");
            btnDelete.Value = GetLocalResourceObject("deleteRows");
            txtOther.Attributes["placeholder"] = GetLocalResourceObject("placeHolderOther.Text");
            headerRow.Attributes["placeholder"] = GetLocalResourceObject("placeHolderHeaderRow.Text");
            startRow.Attributes["placeholder"] = GetLocalResourceObject("placeHolderStartRow.Text");
            endRow.Attributes["placeholder"] = GetLocalResourceObject("placeHolderEndRow.Text");
            return captions;
        }

        //private static API.gd gd = new API.gd();
        
        public static string GetAssemblyPath(string entityNumber)
        {
            //string RootPath = Constant.FileServerPath;
            string folderPath = null;
            if (entityNumber.Contains("-A"))
            {
                entityNumber = entityNumber.Split('-')[0].ToString();
            }
            //test

            folderPath = Common.getFilePath(entityNumber);
            folderPath = folderPath + "\\assembly\\" + entityNumber;

            return folderPath;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [WebMethod(EnableSession = true)]
        public static string SaveBOM(string entityNumber, string searchData, string xmlData, string mapData, string noAlternatives)
        {
            try
            {
                string filePath = GetAssemblyPath(entityNumber);
                if (filePath == null)
                {
                    throw new Exception("FileServer path not found.");
                }
                DataTable dtSearchdata = (DataTable)JsonConvert.DeserializeObject(searchData, (typeof(DataTable)));
                DataTable dtSearchLive = new DataTable();
                dtSearchLive = dtSearchdata.Clone();
                xmlData = SanitizeXml(xmlData); //test from EcWEB
                DataTable dtBOM = (DataTable)JsonConvert.DeserializeObject(xmlData, (typeof(DataTable)));

                UpdateMapColumns(mapData);
                bool isLocal = true;  //bool isLocal = true;
                XmlDocument dtPrice1 = new XmlDocument();
                XmlDocument dtPrice = GetMatchedPartsV1(searchData, 1, 1, "0", noAlternatives, isLocal); 
                if (dtPrice == null)
                {
                    isLocal = false;
                    dtPrice1 = GetMatchedPartsV1(searchData, 1, 1, "0", noAlternatives, isLocal);   
                }
                else
                {
                    XmlDocument xDoc = new XmlDocument();
                    XmlNodeList xnFounds;
                    XmlElement xElement;
                    for(int i=0; i< dtSearchdata.Rows.Count;i++)
                    {

                        xElement = xDoc.CreateElement("part");

                        foreach (DataColumn column in dtSearchdata.Columns)
                        {
                            if (column.ColumnName == "index")
                            {
                                continue;
                            }
                            if (column.ColumnName == "refdes")
                            {
                                string refdes = Regex.Replace(dtSearchdata.Rows[i][column].ToString(), "[^0-9A-Za-z , _ -]", "");
                                xElement.SetAttribute(column.ColumnName, refdes);
                            }
                            else
                            {
                                xElement.SetAttribute(column.ColumnName, dtSearchdata.Rows[i][column].ToString());
                            }
                        }

                        xnFounds = dtPrice.SelectNodes("/data/data[index/text()='" + dtSearchdata.Rows[i]["index"].ToString() + "']");
                        if (xnFounds == null || xnFounds.Count == 0 || xnFounds.Count > 1)
                        {
                            dtSearchLive.Rows.Add(dtSearchdata.Rows[i].ItemArray);
                        }
                        else {
                            if (xnFounds != null && xnFounds.Count == 1)
                            {
                                string matchMPN = xnFounds.Item(0).SelectSingleNode("mpn").InnerXml;
                                string matchSPN = xnFounds.Item(0).SelectSingleNode("spn").InnerXml;

                                string appr = "0";
                                foreach (XmlAttribute attribute in xElement.Attributes)
                                {
                                    //Check if result is match with input MPN
                                    if (attribute.Name == "mpn")
                                    {
                                        if (attribute.Value.ToLower() == matchMPN.ToLower())
                                        {
                                            appr = "1";
                                            break;
                                        }
                                    }

                                    //Check if result is match with any of input SPN
                                    if (appr == "0")
                                    {
                                        if (attribute.Name.StartsWith("spn"))
                                        {
                                            if (attribute.Value.ToLower() == matchSPN.ToLower())
                                            {
                                                appr = "1";
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                if (appr == "0")
                                {
                                    dtSearchLive.Rows.Add(dtSearchdata.Rows[i].ItemArray);
                                }
                            }
                        }
                    }
                    if (dtSearchLive.Rows.Count > 0)
                    {
                        string newSearchData = JsonConvert.SerializeObject(dtSearchLive);
                        isLocal = false;
                        dtPrice1 = GetMatchedPartsV1(newSearchData, 1, 1, "0", noAlternatives, isLocal);
                    }
                }

                if (dtPrice1 != null && dtPrice1.HasChildNodes)
                {
                    var partlist = new List<dynamic>();
                    XmlNodeList xmlNodelist = dtPrice1.SelectNodes("/data/data");
                    foreach (XmlNode xnode in xmlNodelist)
                    {
                        bool aprroved = false;
                        foreach (DataRow dr in dtSearchLive.Rows)
                        {
                            if (dr["mpn"].ToString().ToLower() == xnode.SelectSingleNode("mpn").InnerXml.ToString().ToLower())
                            {
                                aprroved = true;
                                break;
                            }
                            if(dr["spn"]!=null && xnode.SelectSingleNode("spn") != null && dr["spn"].ToString().ToLower() == xnode.SelectSingleNode("spn").InnerXml.ToString().ToLower())
                            {
                                aprroved = true;
                                break;
                            }
                        }
                        if (aprroved)
                        {
                            dynamic DyObj = new ExpandoObject();
                            DyObj.mpn = xnode.SelectSingleNode("mpn").InnerXml;
                            DyObj.ipc = xnode.SelectSingleNode("ipc_name").InnerXml;
                            partlist.Add(DyObj);
                        }
                    }
                    if (partlist.Count > 0)
                    {
                        var strPartList = JsonConvert.SerializeObject(partlist);
                        var feApp = new { fun_name = "sync_vcdb_parts", entity_number = entityNumber, type = "vcdbparts", data = strPartList };
                        string jsonFeApp = JsonConvert.SerializeObject(feApp);
                        Common feAppData = new Common();
                        feAppData.postDataToSeagull(jsonFeApp);
                    }
                }


                XmlDocument docNew = new XmlDocument();
                XmlElement partsElement = docNew.CreateElement("parts");
                docNew.AppendChild(partsElement);
                XmlNodeList xnMatches;
                XmlElement partElement;
                XmlElement matchElement;

                for (int i = 0; i < dtBOM.Rows.Count; i++)
                {
                    partElement = docNew.CreateElement("part");
                    partsElement.AppendChild(partElement);

                    foreach (DataColumn column in dtBOM.Columns)
                    {
                        if (column.ColumnName == "index")
                        {
                            continue;
                        }
                        if (column.ColumnName == "refdes")
                        {
                            string refdes = Regex.Replace(dtBOM.Rows[i][column].ToString(), "[^0-9A-Za-z , _ -]", "");
                            partElement.SetAttribute(column.ColumnName, refdes);
                        }
                        else
                        {
                            partElement.SetAttribute(column.ColumnName, dtBOM.Rows[i][column].ToString());
                        }
                    }

                    //In case sparrow does not give data for anypart
                    if (!dtPrice.HasChildNodes && !dtPrice1.HasChildNodes)
                    {
                        continue;
                    }
                    xnMatches = dtPrice.SelectNodes("/data/data[index/text()='" + dtBOM.Rows[i]["index"].ToString() + "']");
                    if (xnMatches == null || xnMatches.Count == 0|| xnMatches.Count>1)
                    {
                        xnMatches = dtPrice1.SelectNodes("/data/data[index/text()='" + dtBOM.Rows[i]["index"].ToString() + "']");
                    }
                    //Set approve attribute and get prices
                    if (xnMatches != null && xnMatches.Count == 1)
                    {
                        string matchMPN = xnMatches.Item(0).SelectSingleNode("mpn").InnerXml;
                        string matchSPN = xnMatches.Item(0).SelectSingleNode("spn").InnerXml;

                        string appr = "0";
                        foreach (XmlAttribute attribute in partElement.Attributes)
                        {
                            //Check if result is match with input MPN
                            if (attribute.Name == "mpn")
                            {
                                if (attribute.Value.ToLower() == matchMPN.ToLower())
                                {
                                    appr = "1";
                                    break;
                                }
                            }

                            //Check if result is match with any of input SPN
                            if (appr == "0")
                            {
                                if (attribute.Name.StartsWith("spn"))
                                {
                                    if (attribute.Value.ToLower() == matchSPN.ToLower())
                                    {
                                        appr = "1";
                                        break;
                                    }
                                }
                            }
                        }
                        partElement.SetAttribute("appr", appr);
                        if(appr == "0" && dtPrice1.HasChildNodes)
                        {
                            xnMatches = dtPrice1.SelectNodes("/data/data[index/text()='" + dtBOM.Rows[i]["index"].ToString() + "']");
                            if (xnMatches != null && xnMatches.Count == 1)
                            {
                                 matchMPN = xnMatches.Item(0).SelectSingleNode("mpn").InnerXml;
                                 matchSPN = xnMatches.Item(0).SelectSingleNode("spn").InnerXml;

                                 appr = "0";
                                foreach (XmlAttribute attribute in partElement.Attributes)
                                {
                                    //Check if result is match with input MPN
                                    if (attribute.Name == "mpn")
                                    {
                                        if (attribute.Value.ToLower() == matchMPN.ToLower())
                                        {
                                            appr = "1";
                                            break;
                                        }
                                    }

                                    //Check if result is match with any of input SPN
                                    if (appr == "0")
                                    {
                                        if (attribute.Name.StartsWith("spn"))
                                        {
                                            if (attribute.Value.ToLower() == matchSPN.ToLower())
                                            {
                                                appr = "1";
                                                break;
                                            }
                                        }
                                    }
                                }
                                partElement.SetAttribute("appr", appr);
                            }
                         }
                    }

                    foreach (XmlNode xnMatch in xnMatches)
                    {
                        XmlElement xmlElement = (XmlElement)xnMatch;
                        matchElement = docNew.CreateElement("match");
                        matchElement.SetAttribute("mpn", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("mpn").InnerXml.ToString()));
                        matchElement.SetAttribute("suppl", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("supplier").InnerXml.ToString()));
                        matchElement.SetAttribute("spn", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("spn").InnerXml.ToString()));
                        matchElement.SetAttribute("ver", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("verified").InnerXml.ToString()));
                        matchElement.SetAttribute("desc", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("descr").InnerXml.ToString()));
                        matchElement.SetAttribute("ipc", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("ipc_name").InnerXml.ToString()));
                        matchElement.SetAttribute("url", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("data_url").InnerXml.ToString()));
                        matchElement.SetAttribute("img", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("image").InnerXml.ToString()));
                        matchElement.SetAttribute("manuf", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("manuf").InnerXml.ToString()));
                        matchElement.SetAttribute("gpn", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("is_generic").InnerXml.ToString()));
                        matchElement.SetAttribute("pol", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("polarized").InnerXml.ToString()));
                        matchElement.SetAttribute("stock", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("stock").InnerXml.ToString()));
                        matchElement.SetAttribute("cat", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("category").InnerXml.ToString()));
                        matchElement.SetAttribute("type", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("mounting").InnerXml.ToString()));
                        matchElement.SetAttribute("pins", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("solder_pins").InnerXml.ToString()));
                        matchElement.SetAttribute("marking", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("marking").InnerXml.ToString()));
                        matchElement.SetAttribute("remark", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("remark").InnerXml.ToString()));
                        matchElement.SetAttribute("partId", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("rel_id").InnerXml.ToString()));
                        matchElement.SetAttribute("ec_stock", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("ec_stock").InnerXml.ToString()));
                        matchElement.SetAttribute("footprint", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("footprint").InnerXml.ToString()));
                        matchElement.SetAttribute("step3d", HttpUtility.HtmlDecode(xnMatch.SelectSingleNode("step3d").InnerXml.ToString()));
                        matchElement.SetAttribute("prodQty", "0");
                        matchElement.SetAttribute("purchQty", "0");
                        matchElement.SetAttribute("moq", "0");
                        matchElement.SetAttribute("price", "0");

                        partElement.AppendChild(matchElement);
                        xnMatch.SelectSingleNode("index").InnerXml = "processed";
                        XmlNodeList xmlsuppliers = xnMatch.SelectNodes("suppliers");

                        List<string> spl = new List<string>();
                        foreach (XmlNode suppliers in xmlsuppliers)
                        {
                            XmlElement xnspl = (XmlElement)suppliers;
                            string strspl = xnspl.SelectSingleNode("supplier").InnerText.ToString();
                            if (!spl.Contains(strspl))
                            {
                                spl.Add(strspl);
                            }
                        }
                        foreach (string strspn in spl)
                        {
                            XmlElement Supplier = docNew.CreateElement("Supplier");
                            Supplier.SetAttribute("name", strspn);
                            foreach (XmlNode suppliers in xmlsuppliers)
                            {
                                XmlElement xnspl = (XmlElement)suppliers;
                                string supplier = xnspl.SelectSingleNode("supplier").InnerText.ToString();
                                string sku = xnspl.SelectSingleNode("sku").InnerText.ToString();
                                if (supplier.ToUpper() == strspn.ToUpper())
                                {
                                    XmlElement SPN = docNew.CreateElement("SPN");
                                    SPN.SetAttribute("sku", sku);
                                    Supplier.AppendChild(SPN);
                                }
                            }
                            matchElement.AppendChild(Supplier);
                        }
                    }
                }

               
                if (ReportBuilderUtilities.IsImpersonation())
                {
                    filePath = filePath + "\\BOM\\";

                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }


                    if (Directory.Exists(filePath + "CM\\"))
                    {
                        string[] fileList = System.IO.Directory.GetFiles(filePath + "CM\\");
                        bool checkUploadedFile = false;
                        foreach (string files in fileList)
                        {
                            if (files.Contains("UPLOADED_"))
                            {
                                checkUploadedFile = true;
                            }
                        }
                        if (checkUploadedFile)
                        {
                            foreach (string files in fileList)
                            {
                                if (files.Contains("SUBMITTED_"))
                                {
                                    File.Delete(files);
                                }
                            }
                            foreach (string files in fileList)
                            {
                                if (files.Contains("UPLOADED_"))
                                {
                                    File.Copy(files, files.Replace("UPLOADED_", "SUBMITTED_"));
                                    File.Delete(files); ;

                                    try
                                    {
                                        long crm_rel_id = 0;
                                        if (sparrowFeApp.core.AppSessions.FEUserId != null)
                                        {
                                            long userid = 0;
                                            long.TryParse(sparrowFeApp.core.AppSessions.FEUserId, out userid);
                                            crm_rel_id = sparrowFeApp.core.Common.GetCrmRelIdFromUserid(userid);
                                        }
                                    }
                                    catch { }
                                }
                            }
                        }
                    }
                }
                filePath = filePath + "\\bom.xml";
                using (TextWriter sw = new StreamWriter(filePath, false, Encoding.UTF8))
                {
                    docNew.Save(sw);
                }
                SaveBomHistory(entityNumber, "SUBMITBOMFILE");
                HttpContext.Current.Session["BOMImported"] = "true";

                string fileName = Path.GetFileName(filePath);
                string filePathnw = filePath.Replace(Constant.FileServerPath, "").Replace(fileName, "");
                long crm_rel_id1 = 0;
                if (sparrowFeApp.core.AppSessions.FEUserId != null)
                {
                    long userid = 0;
                    long.TryParse(sparrowFeApp.core.AppSessions.FEUserId, out userid);
                    crm_rel_id1 = sparrowFeApp.core.Common.GetCrmRelIdFromUserid(userid);
                }
                return JsonConvert.SerializeObject(new { Status = 1, FileName = "bom.xml" });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { Status = 0, Msg = ex.Message });
            }
        }

        public static string SanitizeXml(string str)
        {
            var builder = new StringBuilder();

            foreach (var ch in str)
            {
                if (XmlConvert.IsXmlChar(ch))
                    builder.Append(ch);
            }

            return builder.ToString();
        }

        public static void UpdateMapColumns(string mapData)
        {
            DataTable dtMappedCols = (DataTable)JsonConvert.DeserializeObject(mapData, (typeof(DataTable)));
            string sql = "";
            foreach (DataRow dr in dtMappedCols.Rows)
            {
                //Donot insert mapping info if column name is system generated like column1, column2...
                if (!dr["userCol"].ToString().ToLower().Contains("column"))
                {
                    sql += "exec bom_user_columns_insert " + dr["bomColId"] + ", '" + dr["userCol"].ToString().ToLower() + "';";  //pending
                }
            }

            if (sql != "")
            {
                //objgd.PutData(sql, Keydata);
            }
        }
        public static void SaveBomHistory(string entityNumber, string actionCode)
        {
            try
            {
                string ip = Common.GetClientIPType();
                //long actionId = admCode.GetCodeId(actionCode);
                //var objgd = new EC09WebApp.API.gd();
                //string strsql = string.Format("exec BOMCPLModified '{0}',{1},{2},'{3}','{4}','{5}'", entityNumber, 0, actionId, "-", ip, "");
                //objgd.PutData(strsql, Keydata);
            }
            catch
            {
            }


        }

        //[System.Web.Script.Services.ScriptMethod()]
        //[WebMethod(EnableSession = true)]
        //public static string SendChangedPart(string data)
        //{
        //    try
        //    {
        //        var baseAddress = ConfigurationManager.AppSettings["SparrowAPI"].ToString() + "replace_mpn/";
        //        var client = new RestSharp.RestClient(baseAddress);
        //        var restRequest = new RestRequest(Method.POST);
        //        restRequest.AddParameter("key", "yhlC0v6OTSY0lys9MHmFWkhYyzyaEG09");
        //        restRequest.AddParameter("data", data);
        //        string userId = "68792";
        //        string username = "System";
        //        if (EC09Sessions.UserId != null)
        //        {
        //            userId = EC09Sessions.UserId.ToString();
        //            username = EC09Sessions.UserName;
        //        }
        //        restRequest.AddParameter("user_id", userId);
        //        restRequest.AddParameter("email", username);
        //        restRequest.AddParameter("ip", EC09WebService.EC09WebService.GetClientIP());

        //        var result = client.Execute(restRequest).Content;
        //        return JsonConvert.SerializeObject(new { Status = 1 });
        //    }
        //    catch (Exception ex)
        //    {
        //        return JsonConvert.SerializeObject(new { Status = 0, Msg = ex.Message });
        //    }
        //}


        /// <param name="searchData">JSON search data</param>
        /// <param name="mode">        
        /// If mode = 1 - No lookup on external API,
        /// If mode = 2 - Lookup on external API if part does not exist in our system
        /// </param>        
        /// <param name="logData">1 if need to log search data</param>
        /// <param name="internalRequest">If mode =2 and internalRequest = 1 then only use Octopart API  </param>
        /// <returns></returns>
        public static DataTable GetMatchedParts(string data, int mode, int apiLookup, string internalRequest, string noAlternatives = "0")
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("data", data);
            parameters.Add("mode", mode);
            parameters.Add("no_alternatives", noAlternatives);
            parameters.Add("internal", internalRequest);
            parameters.Add("api_lookup", apiLookup);

            string result = Component.GetDataFromSparrow("component_search", parameters);
            if (result == "")
            {
                return null;
            }

            XmlDocument docPrice = (XmlDocument)JsonConvert.DeserializeXmlNode(result.ToString(), "data");

            using (XmlReader xmlReaderPrice = new XmlNodeReader(docPrice))
            {
                DataSet dsPrice = new DataSet();
                dsPrice.ReadXml(xmlReaderPrice);
                if (dsPrice != null && dsPrice.Tables.Count > 0)
                {
                    return dsPrice.Tables[0];
                }
            }

            return null;
        }

        public static XmlDocument GetMatchedPartsV1(string data, int mode, int apiLookup, string internalRequest, string noAlternatives = "0", bool isLocal = true)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("data", data);
            parameters.Add("mode", mode);
            parameters.Add("no_alternatives", noAlternatives);
            parameters.Add("internal", internalRequest);
            parameters.Add("api_lookup", apiLookup);

            string result = Component.GetDataFromSparrow("component_search", parameters, isLocal);
            if (result == "")
            {
                return null;
            }

            XmlDocument docPrice = (XmlDocument)JsonConvert.DeserializeXmlNode(result.ToString(), "data");
            return docPrice;
        }

        public static dynamic GetMatchedPartsV2(string data, int mode, int apiLookup, string internalRequest, string noAlternatives = "0")
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("data", data);
            parameters.Add("mode", mode);
            parameters.Add("no_alternatives", noAlternatives);
            parameters.Add("internal", internalRequest);
            parameters.Add("api_lookup", apiLookup);
            string result = Component.GetDataFromSparrow("component_search", parameters);
            if (result == "")
            {
                return null;
            }
            dynamic partresult = JsonConvert.DeserializeObject(result);
            foreach (Newtonsoft.Json.Linq.JObject row in partresult.data)
            {
                row.Add("partId", partresult.data[0].rel_id);
                row.Property("rel_id").Remove();
            }
            return partresult.data;
        }

        public static DataTable GetopenComponentparts(string data, ref int total, long page_start, long page_length, bool isMulti = false)
        {
            string type = "single";

            if (isMulti)
                type = "multi";
            total = 0;
            string searchData = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            //parameters.Add("params", data);
            parameters.Add("search_data", data);
            parameters.Add("type", type);
            parameters.Add("page_start", page_start);
            parameters.Add("page_length", page_length);

            string result = Component.GetDataFromSparrow("open_component_search", parameters);
            if (result == "")
            {
                return null;
            }

            try
            {
                XmlDocument docresult = (XmlDocument)JsonConvert.DeserializeXmlNode(result.ToString(), "data");
                System.Xml.XmlNode dataNode = docresult.SelectSingleNode("/data/code");
                if (dataNode.InnerText == "0")
                {
                    return null;
                }
                System.Xml.XmlNode dataTotal = docresult.SelectSingleNode("/data/total");
                total = int.Parse(dataTotal.InnerText);
            }
            catch { }

            DataTable dtxml = new DataTable();
            dtxml.Columns.Add("mpn", typeof(string));
            dtxml.Columns.Add("rohs", typeof(string));
            dtxml.Columns.Add("cat", typeof(string));
            dtxml.Columns.Add("ft_eagle", typeof(string));
            dtxml.Columns.Add("ver", typeof(string));
            dtxml.Columns.Add("remark", typeof(string));
            dtxml.Columns.Add("desc", typeof(string));
            dtxml.Columns.Add("pol", typeof(string));
            dtxml.Columns.Add("manuf", typeof(string));
            dtxml.Columns.Add("ft_fpx", typeof(string));
            dtxml.Columns.Add("is_polarized", typeof(string));
            dtxml.Columns.Add("ipc", typeof(string));
            dtxml.Columns.Add("type", typeof(string));
            dtxml.Columns.Add("ft_altium", typeof(string));
            dtxml.Columns.Add("gpn", typeof(string), "0");
            dtxml.Columns.Add("wrl_3d", typeof(string));
            dtxml.Columns.Add("step_3d", typeof(string));
            dtxml.Columns.Add("url", typeof(string));
            dtxml.Columns.Add("category_id", typeof(string));
            dtxml.Columns.Add("ft_kicad", typeof(string));
            dtxml.Columns.Add("img", typeof(string));
            dtxml.Columns.Add("stock", typeof(string));
            dtxml.Columns.Add("index", typeof(string));
            dtxml.Columns.Add("partId", typeof(string));
            dtxml.Columns.Add("marking", typeof(string));
            dtxml.Columns.Add("external", typeof(string));

            //test
            dtxml.Columns.Add("resis", typeof(string));
            dtxml.Columns.Add("pack", typeof(string));
            dtxml.Columns.Add("toler", typeof(string));
            dtxml.Columns.Add("power", typeof(string));
            dtxml.Columns.Add("comp", typeof(string));
            dtxml.Columns.Add("op_temp", typeof(string));
            dtxml.Columns.Add("temp", typeof(string));
            dtxml.Columns.Add("rate", typeof(string));
            dtxml.Columns.Add("pack_type", typeof(string));
            dtxml.Columns.Add("capacitance", typeof(string));
            dtxml.Columns.Add("oper_volt", typeof(string));
            dtxml.Columns.Add("spn", typeof(string));
            //bool isColumnadded = false;

            result = result.Replace("3d_wrl", "wrl_3d");
            result = result.Replace("3d_step", "step_3d");
            XmlDocument docPrice = (XmlDocument)JsonConvert.DeserializeXmlNode(result.ToString(), "data");
            try
            {
                System.Xml.XmlNodeList dataNodeList = docPrice.SelectNodes("/data/data");
                foreach (System.Xml.XmlNode datanode in dataNodeList)
                {
                    string Index = datanode.SelectSingleNode("index").InnerXml;
                    foreach (System.Xml.XmlNode resultnode in datanode.SelectNodes("result"))
                    {
                        DataRow dtrow = dtxml.NewRow();
                        dtrow["index"] = Index;
                        foreach (System.Xml.XmlNode child in resultnode.ChildNodes)
                        {



                            if (child.Name.ToLower() == "category".ToLower())
                                dtrow["cat"] = child.InnerXml;

                            if (child.Name.ToLower() == "ft_eagle".ToLower())
                                dtrow["ft_eagle"] = child.InnerXml;

                            if (child.Name.ToLower() == "descr".ToLower())
                                dtrow["desc"] = child.InnerXml;

                            if (child.Name.ToLower() == "is_polarized".ToLower())
                                dtrow["is_polarized"] = child.InnerXml;

                            if (child.Name.ToLower() == "mpn".ToLower())
                                dtrow["mpn"] = HttpUtility.HtmlDecode(child.InnerXml);

                            if (child.Name.ToLower() == "manufacturer".ToLower())
                                dtrow["manuf"] = child.InnerXml;

                            if (child.Name.ToLower() == "ft_fpx".ToLower())
                                dtrow["ft_fpx"] = child.InnerXml;

                            if (child.Name.ToLower() == "is_poolable".ToLower())
                                dtrow["pol"] = child.InnerXml;

                            if (child.Name.ToLower() == "ipcname".ToLower())
                                dtrow["ipc"] = child.InnerXml;

                            if (child.Name.ToLower() == "mount_type".ToLower())
                            {
                                if (child.InnerXml.Trim().ToUpper() == "QFN_FINEPITCH".ToUpper())
                                {
                                    dtrow["type"] = "QFN FINEPITCH";
                                }
                                else if (child.InnerXml.Trim().ToUpper() == "SMD_FINEPITCH".ToUpper())
                                {
                                    dtrow["type"] = "SMD FINEPITCH";
                                }
                                else if (child.InnerXml.Trim().ToUpper() == "BGA_FINEPITCH".ToUpper())
                                {
                                    dtrow["type"] = "BGA FINEPITCH";
                                }
                                else
                                {
                                    dtrow["type"] = child.InnerXml;
                                }
                            }


                            if (child.Name.ToLower() == "ft_altium".ToLower())
                                dtrow["ft_altium"] = child.InnerXml;

                            if (child.Name.ToLower() == "is_generic".ToLower())
                                dtrow["gpn"] = child.InnerXml;

                            if (child.Name.ToLower() == "part_id".ToLower())
                                dtrow["partId"] = child.InnerXml;

                            if (child.Name.ToLower() == "3d_wrl".ToLower())
                                dtrow["wrl_3d"] = child.InnerXml;

                            if (child.Name.ToLower() == "3d_step".ToLower())
                                dtrow["step_3d"] = child.InnerXml;

                            if (child.Name.ToLower() == "datasheet_url".ToLower())
                                dtrow["url"] = child.InnerXml;

                            if (child.Name.ToLower() == "category_id".ToLower())
                                dtrow["category_id"] = child.InnerXml;

                            if (child.Name.ToLower() == "ft_kicad".ToLower())
                                dtrow["ft_kicad"] = child.InnerXml;

                            if (child.Name.ToLower() == "imgurl".ToLower())
                                dtrow["img"] = child.InnerXml;

                            if (child.Name.ToLower() == "stock".ToLower())
                                dtrow["stock"] = child.InnerXml;

                            if (child.Name.ToLower() == "marking".ToLower())
                                dtrow["marking"] = child.InnerXml;

                            if (child.Name.ToLower() == "is_verified".ToLower())
                            {
                                dtrow["ver"] = "0";
                                if (child.InnerXml == "true")
                                {
                                    dtrow["ver"] = "1";
                                }
                            }

                            if (child.Name.ToLower() == "external".ToLower())
                            {
                                dtrow["external"] = "0";
                                if (child.InnerXml == "true")
                                {
                                    dtrow["external"] = "1";
                                }
                            }


                            //test

                            if (child.Name.ToLower() == "spn".ToLower())
                                dtrow["spn"] = child.InnerXml;
                        }
                        foreach (System.Xml.XmlNode specs in resultnode.SelectNodes("specs"))
                        {
                            foreach (System.Xml.XmlNode child in specs.ChildNodes)
                            {

                                if (child.Name.ToLower() == "rohs_status".ToLower())
                                    dtrow["rohs"] = child.InnerXml;
                                if (child.Name.ToLower() == "resistance".ToLower())
                                    dtrow["resis"] = child.InnerXml;
                                if (child.Name.ToLower() == "packaging_style".ToLower())
                                    dtrow["pack"] = child.InnerXml;
                                if (child.Name.ToLower() == "tolerance".ToLower())
                                    dtrow["toler"] = child.InnerXml;
                                if (child.Name.ToLower() == "power_dissipation".ToLower())
                                    dtrow["power"] = child.InnerXml;
                                if (child.Name.ToLower() == "composition".ToLower())
                                    dtrow["comp"] = child.InnerXml;
                                if (child.Name.ToLower() == "operating_temperature".ToLower())
                                    dtrow["op_temp"] = child.InnerXml;
                                if (child.Name.ToLower() == "temperature_coefficient".ToLower())
                                    dtrow["temp"] = child.InnerXml;
                                if (child.Name.ToLower() == "failure_rate".ToLower())
                                    dtrow["rate"] = child.InnerXml;
                                if (child.Name.ToLower() == "package_type_imperial".ToLower())
                                    dtrow["pack_type"] = child.InnerXml;
                                if (child.Name.ToLower() == "capacitance".ToLower())
                                    dtrow["capacitance"] = child.InnerXml;
                                if (child.Name.ToLower() == "max_operation_voltage".ToLower())
                                    dtrow["oper_volt"] = child.InnerXml;
                            }
                        }
                        dtxml.Rows.Add(dtrow);
                    }
                }

            }
            catch { }

            return dtxml;
            //using (XmlReader xmlReaderPrice = new XmlNodeReader(docPrice))
            //{
            //    DataSet dsPrice = new DataSet();
            //    dsPrice.ReadXml(xmlReaderPrice);
            //    if (dsPrice != null && dsPrice.Tables.Count > 0)
            //    {

            //        return dsPrice.Tables[0];
            //    }
            //} 
        }


    }
        public class Part
    {
        public string part { get; set; }
        public string ipc { get; set; }
    }
   
}