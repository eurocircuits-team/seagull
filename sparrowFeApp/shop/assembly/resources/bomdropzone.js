﻿var pcbdropzone = {
    sessionId: "",
    init: function () { 
        var companyid = $('#ctl00_mainContent_companyid').val(); 
        if (companyid != "0") { 
            $("#ctl00_licountry").hide();
        }
        pcbdropzone.events();
        pcbdropzone.fileDrop("#drop-icon-home");
        pcbdropzone.fileDrop("#drop-title-home");
        pcbdropzone.fileDrop("#drop-btn-home");
        pcbdropzone.fileDrop("#drop-zone-home-ecpart");

        var fileselecticon = $("#drop-icon-home");
        fileselecticon[0].addEventListener("dragover", pcbdropzone.fileSelectHandler, false);
        fileselecticon[0].addEventListener("dragleave", pcbdropzone.fileSelectHandlerdragleave, false);
        fileselecticon[0].addEventListener("drop", pcbdropzone.fileSelectHandlerdrop, false);

        var fileselecttitle = $("#drop-title-home");
        fileselecttitle[0].addEventListener("dragover", pcbdropzone.fileSelectHandler, false);
        fileselecttitle[0].addEventListener("dragleave", pcbdropzone.fileSelectHandlerdragleave, false);
        fileselecttitle[0].addEventListener("drop", pcbdropzone.fileSelectHandlerdrop, false);

        var fileselectbtn = $("#drop-btn-home");
        fileselectbtn[0].addEventListener("dragover", pcbdropzone.fileSelectHandler, false);
        fileselectbtn[0].addEventListener("dragleave", pcbdropzone.fileSelectHandlerdragleave, false);
        fileselectbtn[0].addEventListener("drop", pcbdropzone.fileSelectHandlerdrop, false);

        var fileselecthome = $("#drop-zone-home-ecpart");
        fileselecthome[0].addEventListener("dragover", pcbdropzone.fileSelectHandler, false);
        fileselecthome[0].addEventListener("dragleave", pcbdropzone.fileSelectHandlerdragleave, false);
        fileselecthome[0].addEventListener("drop", pcbdropzone.fileSelectHandlerdrop, false);

    },

    fileSelectHandler: function (e) { 
        pcbdropzone.fileDragHover(e);
    },
    fileSelectHandlerdragleave: function (e) {
        pcbdropzone.filedragleave(e);
    },
    fileSelectHandlerdrop: function (e) {
        pcbdropzone.filedrop(e);
    },
    fileDragHover: function (e) { 
        e.stopPropagation();
        e.preventDefault();  
        //e.target.className = (e.type == "dragover" ? "dropzone-border-red" : "");
        $("#drop-zone-home-ecpart").addClass('dropzone-border-red');
    },
    filedragleave: function (e) {
        e.stopPropagation();
        e.preventDefault(); 
        $("#drop-zone-home-ecpart").removeClass('dropzone-border-red');
    },
    filedrop: function (e) {
        e.stopPropagation();
        e.preventDefault(); 
        $("#drop-zone-home-ecpart").removeClass('dropzone-border-red');
    },
    events: function () {
        $("#btn_processing").click(function () { 
            var sessionid = GetParameterValues('sessionid');
            if (sessionid != "") {
                pcbdropzone.sessionId = sessionid;
                pcbdropzone.checkStatus(pcbdropzone.sessionId);
            }
        }); 
    },
    getParameterByName: function (param) {
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < url.length; i++) {
            var urlparam = url[i].split('=');
            if (urlparam[0] == param) {
                return urlparam[1];
            }
        }
    },
    fileDrop: function (id) {
        var bomsessionid = "";
        bomsessionid = $('#ctl00_mainContent_hdbomsessionId').val();
        var userid = "";
        userid = $('#ctl00_hdnmasteruserid').val();
        var myDropzone1 = new Dropzone(id, {
            url: "/shop/assembly/ecpartbom.ashx?isfromsearchui=true&bomsessionid=" + bomsessionid + "&userid=" + userid,
            maxFiles: 1,
            maxFilesize: 10,
            acceptedFiles: ".zip,.xls,.xlsx,.txt",
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            createImageThumbnails: false,
            init: function () {
                this.on("error", function (file) {
                    if (file.size > 10485760) {
                        $('.dz-error').show();
                    }
                    else if (!file.accepted) { 
                        //$('.file-format-error').show();
                        $('.dz-error').hide();
                        $("#modalerror").modal('show');
                        $("#lblMasterError").html("Unable to extract parts from document. Please try again.<br> Supported formats: .zip, .xls, .xlsx, .txt <br>Note: uploaded file needs to be less than 5MB in size. ");
                        //setTimeout(function () { $('.file-format-error').hide(); }, 3000);
                    } 
                });
            },
            sending: function (file, xhr, formData) {
                $(".dz-preview").hide();
                $("#divImportBody").css("width", "40%");
                $('#mdlImport').modal('show');
                $('#divProcessing').show();
                $('#iframeImport').hide();
                //$('#drop-zone-home').hide();
                //$('#imgProcess-home').show();
                //if ($('#btnupload').length) {
                //    $("#btnupload").attr('disabled', 'disabled');
                //}
            },
            success: function (file, response) {
                $(".dz-preview").hide();

                setTimeout(function () {
                    $("#iframeImport").attr("src", '/shop/assembly/ecpartbomimport.aspx?isfromsearchui=true&bomsessionid=' + bomsessionid + "&userid=" + userid);
                    $("#divImportBody").css("width", "90%");
                    $('#divProcessing').hide();
                    $('#iframeImport').show();
                   
                    var data = JSON.parse(response);
                    myDropzone1.removeAllFiles(true);

                }, 3000);
              
                //window.location.href = '/shop/assembly/ecpartbomimport.aspx?isfromsearchui=true&bomsessionid=' + bomsessionid;
            }
        });
    },

}
$(document).ready(function () {
    pcbdropzone.init();
});