﻿var CPLBOM = {
    attached: false,
    bomTable: null,
    basketNr: "",
    currentStep: 1,
    delimiter: 'none',
    tempDelimiter: 'none',
    endRow: 0,
    excel: false,
    headerRow: 0,
    mappedColumnName: {},
    mapDropdownOptions: '',
    nextStep: 0,
    previousStep: 0,
    startRow: 0,
    errorMSGInterval: 10,
    serverData: null,
    fixdLenthColumn: '',
    selectedSheet: '',
    multipleExcelSheet: false,
    captions: '',
    removeRows: [],
    fileExtension: '',
    layer: '',
    merge: false,
    previousStepFLColumn: "",
    autoMapColumns: function () {
        if (CPLBOM.mappedColumnName == null) {
            return;
        }

        jQuery.each(CPLBOM.mappedColumnName, function (key, val) {
            $("select[col='" + val + "']").val(key).attr('oldval', key);
            $("select[col='" + val + "']").parent().addClass('drpMapped');
            $("select").find('option[value="' + key + '"]').addClass('opt-map');
            if (key.toLowerCase() == "comp_x" || key.toLowerCase() == "comp_y" || key.toLowerCase() == "side" || key.toLowerCase() == "mounting") {
                //var mapLink = '<a class="mapColumn ' + value + '_th">Map</a>';
                var mapLink = '&nbsp;<img src="/shop/images/editCell.png?v=0.1" class="mapColumn ' + key + '_th"/>';
                $('#bomTable thead tr').find('.' + val).append(mapLink);
                var headerIndex = $('#bomTable tbody tr.selectedHeader td:first-child').find('span').data('index');
                CPLBOM.addDistinctMapValue(key.toLowerCase(), val, headerIndex);
                var ele = $('#bomTable thead tr').find('.' + val).find('.mapColumn');
                CPLBOM.openMapBox(ele, null);
                if (key.toLowerCase() == "comp_x" || key.toLowerCase() == "comp_y") {
                    CPLBOM.autoMapUnit(key.toLowerCase(), val);
                }
            }
        });
    },

    autoReMapColumns: function () {
        if (CPLBOM.mappedColumnName == null) {
            return;
        }

        jQuery.each(CPLBOM.mappedColumnName, function (key, val) {
            $("select[col='" + val + "']").val(key).attr('oldval', key);
            $("select[col='" + val + "']").parent().addClass('drpMapped');
            $("select").find('option[value="' + key + '"]').addClass('opt-map');
            if (key.toLowerCase() == "side") {
                //var mapLink = '<a class="mapColumn ' + value + '_th">Map</a>';
                //var mapLink = '&nbsp;<img src="/shop/images/editCell.png?v=0.1" class="mapColumn ' + key + '_th"/>';
                //$('#bomTable thead tr').find('.' + val).append(mapLink);
                var headerIndex = $('#bomTable tbody tr.selectedHeader td:first-child').find('span').data('index');
                CPLBOM.addDistinctMapValue(key.toLowerCase(), val, headerIndex);
                var ele = $('#bomTable thead tr').find('.' + val).find('.mapColumn');
                CPLBOM.openMapBox(ele, null);
            }
        });
    },

    addDistinctMapValue: function (column, headerClass, headerIndex) {
        
        var optionArray = [];
        if (column == "mounting" || column == "side") {
            $("td." + headerClass).each(function (index) {

                if (index > headerIndex) {
                    var value = "";
                    if ($(this).parent('tr').hasClass('selectedRow')) {
                        value = $.trim($(this).text());
                    }
                    if (jQuery.inArray(value, optionArray) == -1) {
                        optionArray.push(value);
                        console.log(index + ": " + value);
                    }
                }
            });
            optionArray = $.grep(optionArray, function (n) {
                return (n);
            });

            if (column == "mounting") {

                $('.arrow_box.mounting_th .mounting').empty();
                $('.distinctOption').removeAttr('style');
                if (optionArray.length > 3) {
                    $('.distinctOption').css({ 'margin-right': '-6px' });
                }
                var mount = ["TH", "THD", "TH+SMD", "SMD", "SMD FinePitch", "BGA", "BGA FinePitch", "QFN", "QFN FinePitch", "LGA", "LGA FinePitch", "Pressfit", "FinePitch"];
                jQuery.each(optionArray, function (key, obj) {

                    var option = CPLBOM.addOption(obj, mount);
                    $('.arrow_box.mounting_th .mounting').append(option);
                });
            }
            else {
                $('.arrow_box.side_th .layerOp').empty();
                $('.distinctOption').removeAttr('style');
                if (optionArray.length > 3) {
                    $('.distinctOption').css({ 'margin-right': '-6px' });
                }

                var layers = ["Top", "Bottom"];
                jQuery.each(optionArray, function (key, obj) {
                    var option = CPLBOM.addOption(obj, layers);
                    $('.arrow_box.side_th .layerOp').append(option);
                });
            }
        }
    },
    convertInchToMM: function (inch) {
        var inches = parseFloat(inch, 10);
        var mm = inches * 25.4;
        return mm;
    },
    convertMilToMM: function (mil) {
        var mil = parseFloat(mil, 10);
        var mm = mil * 0.0254;
        return mm;
    },
    clearAll: function () {
        CPLBOM.basketNr = '';
        CPLBOM.endRow = 0;
        CPLBOM.headerRow = 0;
        CPLBOM.delimiter = 'none';
        CPLBOM.startRow = 0;
        CPLBOM.excel = false;
        CPLBOM.selectedSheet = '';
        CPLBOM.previousStepFLColumn = '';
        CPLBOM.multipleExcelSheet = false;
        $('#rdoDelimited').prop('checked', true);
        $('input[type=radio][name=delimiter]').prop('checked', false);
        $('.range').val('');
        $('#txtOther').val('');
        $('divtxtNuberOfC').addClass('hide');
        $('.Fixedwidthimage').hide();
        $('#divdelimiters').show();
        $('.stepMultipleSheet').addClass('hide');
        $('.btnBack').removeClass('hide');
        $('.stepTextBoxData').addClass('hide');
        //$('#textAreaPdf').val('');
    },
    events: function () {
        //TODO: handle using css and if required then put in init
        //$('.ec-modal').css('margin-left', $(window).width() / 2 - 160 + 'px');

        $('#btnSideOk').on('click', function () {
            if ($('input[type=radio][name=side]').is(':checked')) {
                CPLBOM.layer = $("input[type=radio][name=side]:checked").val();
                $('.sideModal').addClass('hide');
                CPLBOM.generateXml();
            }
            else {
                return;
            }
        });
        $('#btnSideCancel,.ec-close').on('click', function () {
            $('.sideModal').addClass('hide');
        });
        //$('#bomTable tbody').on('mouseenter', 'tr td.td-index', function (e) {
        //    $(this).find('span').hide();
        //    $(this).find('.imgDelete').show();
        //})
        //$('#bomTable tbody').on('mouseleave', 'tr td.td-index', function (e) {
        //    $(this).find('span').show();
        //    $(this).find('.imgDelete').hide();
        //});
        //$(document).on('click', '#bomTable tbody tr td .imgDelete', function () {
        //    if (confirm(CPLBOM.captions.ConfirmMessage)) {
        //        //$(this).closest('tr').addClass('tr-remove');
        //        //$(this).closest('tr').removeClass('selectedRow');
        //        var srNo = $(this).closest('tr').children().eq(0).text();
        //        //CPLBOM.removeRows.push(srNo);
        //        CPLBOM.loadTable(false, false, false, false, false, false, srNo);
        //    }
        //    else {
        //        return false;
        //    }
        //});
        $('#btnDelete').on('click', function () {
            var isCheckBox = false;
            var removeRows = "";
            $('#bomTable tbody tr td.td-index').each(function () {
                var sno = $(this).find('span').text();
                var row = $(this);
                if (row.find('input[type="checkbox"]').is(':checked')) {
                    isCheckBox = true;
                    removeRows = removeRows + sno + ',';
                }
            });

            if (isCheckBox) {
                if (confirm(CPLBOM.captions.ConfirmMessage)) {
                    CPLBOM.loadTable(false, false, false, false, false, false, removeRows.slice(0, -1));
                }
                else {
                    return false;
                }
            }
            else {
                //Core.showMessage("errorMsg", true, 'Please select at least one row.', BOM.errorMSGInterval);
                $('#rowSelectMsg').show();
                $('#rowSelectMsg').delay(10000).fadeOut();
                return false;
            }
        });

        $('#aUploadFile').on('click', function () {
            CPLBOM.clearAll();
            $('#fileUploader').val('');
            $('.file-caption-name').text('No file chosen');
            CPLBOM.currentStep = CPLBOM.steps.uploadData;
            $('#stepActionbar').hide();
            $('.step2').addClass('hide');
            $('.step1').removeClass('hide');
            $('.stepTextBoxData').addClass('hide');
            $('#btnSubmitPdfDoc').hide();
            $('#btnCancelPdfDoc').hide();
            $('#textAreaPdf').val('');
            $('#textAreaPdf').hide();
            $('#blockLoading').hide();
        });
        $('.fileUploader').on('change', function (e) {
            $('.btnBack').val(CPLBOM.captions.Back);
            $('.btnBack').removeClass('btnbig');
            CPLBOM.uploadFile(e);
        });
        $('#drpWorkShee').on('change', function (e) {
            CPLBOM.headerRow = 0;
            CPLBOM.selectedSheet = $("option:selected", this).text();
            CPLBOM.loadTable(null, false, true, false, false, true);
        });

        $('.btnNext').on('click', function (e) {

            CPLBOM.showNextStep($(this));
            e.preventDefault();
        });
        $('#downloadFile').on('click', function (e) {
            $('#downloadFile').attr("href", "/shop/assembly/uploadHandler.ashx?isDownload=true&entitynr=" + CPLBOM.basketNr + "&bomfiletype=CPL");
        });
        $('#bomTable').on('click', '.mapColumn', function (e) {
            $('.arrow_box').hide();
            CPLBOM.openMapBox(this, e);
            e.stopPropagation();
        });
        $('.btnBack').on('click', function (e) {
            CPLBOM.showPreviousStep($(this));
            e.preventDefault();
        });
        $(document).on('click', 'span.SpanIndex,span.SpanCol', function () {
            if ($(this).hasClass('fwseparater')) {
                var att = $(this).attr('data-charnumber');
                $('span[data-charnumber=' + att + ']').each(function () {
                    var value = $(this).text();
                    $(this).text(value.replace('|', ''));
                    $(this).removeClass('fwseparater');
                });
            }
            else {
                var att = $(this).attr('data-charnumber');
                $('span[data-charnumber=' + att + ']').each(function () {
                    var value = $(this).text();
                    $(this).text(value + '|');
                    $(this).addClass('fwseparater');
                });
            }

        });
        $('input[type=radio][name=fileType]').change(function (e) {
            if ($('input[type=radio][name=delimiter]').is(':checked')) {
                CPLBOM.tempDelimiter = $("input[type=radio][name=delimiter]:checked").val();
            }
            var fileType = $(this).val();
            //$('#bomTable').removeClass('fixedLenthFont');
            CPLBOM.delimiter = 'none';
            $('#txtOther').val('');
            if (fileType == 'FixedWidth') {
                $('input[type=radio][name=delimiter]').each(function () {
                    $(this).prop("checked", false);
                });
                $('#divtxtNuberOfC').removeClass('hide');
                CPLBOM.delimiter = 'FixedWidth';
                $('.Fixedwidthimage').show();
                $('#divdelimiters').hide();
                //$('#bomTable').addClass('fixedLenthFont');
            }
            else {
                if (CPLBOM.tempDelimiter != 'none') {
                    $('input[type=radio][name=delimiter]').each(function () {
                        if ($(this).val() == CPLBOM.tempDelimiter) {
                            CPLBOM.delimiter = CPLBOM.tempDelimiter;
                            $(this).prop("checked", true);
                        }
                    });
                }
                CPLBOM.previousStepFLColumn = "";
                $('span.SpanIndex.fwseparaterfirst.fwseparater').each(function (index, element) {
                    var value = $(element).attr('data-charnumber');
                    CPLBOM.previousStepFLColumn = CPLBOM.previousStepFLColumn + value + "/";
                });
                CPLBOM.previousStepFLColumn + "0";
                $('divtxtNuberOfC').addClass('hide');
                $('.Fixedwidthimage').hide();
                $('#divdelimiters').show();
            }
            CPLBOM.loadTable(CPLBOM.fixedLenthColumnMap);
            e.preventDefault();
        });

        //Reload table on delimitor change
        $('input[type=radio][name=delimiter]').change(function (e) {
            CPLBOM.delimiter = $(this).val();
            $('divtxtNuberOfC').addClass('hide');
            if (CPLBOM.delimiter == 'Other') {
                var value = $.trim($('.txtOther').val());
                if (value != "") {
                    CPLBOM.loadTable();
                }
            }
            else {
                $('.txtOther').val('');
                CPLBOM.loadTable();
            }
            e.preventDefault();
        });

        $(".txtOther").blur(function (e) {
            var value = $(this).val();
            if (value.length == 0) {
                CPLBOM.delimiter = 'none';
                CPLBOM.loadTable();
            }
            else if (CPLBOM.delimiter != value) {
                CPLBOM.delimiter = value;
                CPLBOM.loadTable();
            }
            e.preventDefault();
        });

        $(".txtOther").keyup(function (e) {
            var value = $(this).val();
            if (value.length == 0) {
                CPLBOM.delimiter = 'none';
                CPLBOM.loadTable();
            }
            else if (CPLBOM.delimiter != value) {
                CPLBOM.delimiter = value;
                CPLBOM.loadTable();
            }
        });

        $(".txtOther").focus(function () {
            $('#rdoOther').prop('checked', true);
        });

        // Set Header row on out of focus
        $('.headerRow').on('keyup', function (e) {
            if (e.keyCode == 13) {
                CPLBOM.headerRowAction(e);
            }
        });
        $('.headerRow').on('blur', function (e) {
            CPLBOM.headerRowAction(e);
        });
        //// Set Start row on out of focus
        //$(".startRow").blur(function (e) {
        //    $('.pageloader,.loaderBG').removeClass('hide');
        //    CPLBOM.startRow = parseInt($(this).val()) || 0;

        //    CPLBOM.selectedRows();
        //    e.preventDefault();
        //});
        //// Set End row on out of focus
        //$(".endRow").blur(function (e) {
        //    CPLBOM.endRow = parseInt($(this).val()) || 0;
        //    CPLBOM.selectedRows();
        //    e.preventDefault();
        //});

        // Set Start row on out of focus
        $(".startRow").keyup(function (e) {
            if (e.keyCode == 13) {
                CPLBOM.startRowAction(e);
            }
        });
        $(".startRow").blur(function (e) {
            CPLBOM.startRowAction(e);
        });
        // Set End row on out of focus
        $(".endRow").keyup(function (e) {
            CPLBOM.endRow = parseInt($(this).val()) || 0;
            CPLBOM.startRow = parseInt($(".startRow").val()) || 0;
            CPLBOM.selectedRows();
            CPLBOM.validateInputData();
            e.preventDefault();
        });
        $('#bomTable').on('change', '.dropdown', function (e) {
            e.stopPropagation();
            CPLBOM.getMappedColumn();
            var selectedVal = this.value;
            var oldVal = $(this).attr("oldVal");

            var userColumn = $.trim($(this).parent('td').attr('userColumn'));

            $('#bomTable tbody tr:first td select').each(function (index, element) {
                var value = $(this).val();

                $(this).find('option[value="' + oldVal + '"]').removeClass('opt-map');
                if (selectedVal != "none") {
                    $(this).find('option[value="' + selectedVal + '"]').addClass('opt-map');
                }
                if (value == selectedVal) {
                    $(this).val('');
                    $(this).parent().removeClass('drpMapped');
                    var cs = $.trim($(this).parent('td').attr('class'));
                    $('#bomTable thead tr .' + cs).find('.mapColumn').remove();
                }
            });
            $(this).val(selectedVal);
            $(this).attr("oldVal", selectedVal);

            if (selectedVal == "none") {
                $(this).parent().removeClass('drpMapped');
            }
            else {
                $(this).parent().addClass('drpMapped');
            }

            var headerIndex = $('#bomTable tbody tr.selectedHeader td:first-child').find('span').data('index');
            CPLBOM.addDistinctMapValue(selectedVal, userColumn, headerIndex);
            if (selectedVal == 'comp_x' || selectedVal == 'comp_y' || selectedVal == 'side' || selectedVal == 'mounting') {
                var mapLink = '&nbsp;<img height="14" src="/shop/images/editCell.png?v=0.1" class="mapColumn ' + selectedVal + '_th"/>';
                $('#bomTable thead tr').find('.' + userColumn).append(mapLink);
                var ele = $('#bomTable thead tr .' + userColumn).find('.mapColumn');
                CPLBOM.openMapBox(ele, e);
                if (selectedVal == 'comp_x' || selectedVal == 'comp_y') {
                    CPLBOM.autoMapUnit(selectedVal, userColumn);
                }
            }
        });

        $('#bomTable').on('click', '.mpnColumn', function (e) {
            var uId = $(this).attr('uid');
            var counts = $(this).attr('counts');
            $(this).closest('tr').addClass('selectedRow')
            e.stopPropagation(this, e);
        });

        //numeric text-box
        $(".range").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        // If an event gets to the body
        $("body").click(function (e) {
            if ($(e.target).hasClass("dropdown") || $(e.target).hasClass("btnNext")) { return; }
                $(".arrow_box").hide();
            CPLBOM.indicateSideColumn();
        });
        $('#btnSubmitPdfDoc').on('click', function (e) {
            $('.stepTextBoxData').removeClass('hide');
            if ($('#textAreaPdf').val() == '') {
                $('.step1').addClass('hide');
                $('.step2').removeClass('hide');
                $('#textAreaPdf').focus();
                return;
            }
            var selectedDelimiter = CPLBOM.delimiter
            var selectedpreviousStepFLColumn = CPLBOM.previousStepFLColumn;
            CPLBOM.clearAll();
            CPLBOM.delimiter = selectedDelimiter;
            CPLBOM.previousStepFLColumn = selectedpreviousStepFLColumn;
            $('input[type=radio][name=delimiter]').each(function () {
                if (CPLBOM.delimiter != 'none') {
                    if ($(this).val() == CPLBOM.delimiter) {
                        $(this).prop("checked", true);
                    }
                }
            });
            if (CPLBOM.delimiter.toLowerCase() == "fixedwidth") {
                $('#rdoFixedWidth').prop("checked", true);
                $('#divtxtNuberOfC').removeClass('hide');
                CPLBOM.delimiter = 'FixedWidth';
                $('.Fixedwidthimage').show();
                $('#divdelimiters').hide();
            }
            $('.stepTextBoxData').addClass('hide');
            CPLBOM.basketNr = Core.getParameterByName('id');
            CPLBOM.attached = true;

            CPLBOM.loadTable(CPLBOM.fixedLenthColumnMap, true, false, false, true);
            //CPLBOM.currentStep = CPLBOM.showDelimiterStep();BOM.fixedLenthColumnMap
            $('#btnSubmitPdfDoc').hide();
            $('#btnCancelPdfDoc').hide();
            //$('#textAreaPdf').val('');
            $('#textAreaPdf').hide();
            $('#blockLoading').hide();
        });
        $('#btnCancelPdfDoc').on('click', function (e) {
            CPLBOM.clearCheckBox();
            if (CPLBOM.fileExtension.toLowerCase() == 'pdf' || CPLBOM.fileExtension.toLowerCase() == 'doc' || CPLBOM.fileExtension.toLowerCase() == 'docx' || CPLBOM.fileExtension.toLowerCase() == 'zip' || CPLBOM.fileExtension.toLowerCase() == 'rar' || CPLBOM.fileExtension.toLowerCase() == 'brd') {
                $('#stepActionbar').hide();
                $('.step2').addClass('hide');
                $('.step1').removeClass('hide');
                CPLBOM.currentStep = CPLBOM.steps.uploadData;
                CPLBOM.multipleExcelSheet = false;
            }
            else {
                $('.divTable').removeClass('hide');
            }
            $('#fileUploader').val('');
            $('.file-caption-name').text('No file chosen');
            $('.stepTextBoxData').addClass('hide');
            $('#btnSubmitPdfDoc').hide();
            $('#btnCancelPdfDoc').hide();
            $('#textAreaPdf').val('');
            $('#textAreaPdf').hide();
            $('#blockLoading').hide();
        });
        $('#editBOM').on('click', function () {
            $("#textAreaPdf").height($(window).height() - 230);
            $('#blockLoading').show();
            $(".divTable").addClass('hide');
            $('.stepTextBoxData').addClass('hide');
            $('#textAreaPdf').focus();
            $('#textAreaPdf').show();
            $('#btnSubmitPdfDoc').val(CPLBOM.captions.Save);
            $('#btnCancelPdfDoc').val(CPLBOM.captions.Cancel);
            $('#btnSubmitPdfDoc').show();
            $('#btnCancelPdfDoc').show();
            if (CPLBOM.delimiter == "FixedWidth") {
                CPLBOM.previousStepFLColumn = "";
                $('span.SpanIndex.fwseparaterfirst.fwseparater').each(function (index, element) {
                    var value = $(element).attr('data-charnumber');
                    CPLBOM.previousStepFLColumn = CPLBOM.previousStepFLColumn + value + "/";
                });
                CPLBOM.previousStepFLColumn + "0";
            }
            CPLBOM.loadTable(false, false, false, true);
        });
        // Prevent events from getting pass .popup
        $(".arrow_box").click(function (e) {
            e.stopPropagation();
        });

    },

    editableCell: function () {
        if ($(this).children().first().is("input") || $(this).children().first().is("select") || $(this).children().first().is("span")) {
            return;
        }

        var originalContent = $(this).text();
        $(this).addClass("cellEditing");
        $(this).html("<input type='text' value='" + originalContent + "' />");
        $(this).children().first().focus();

        $(this).children().first().keypress(function (e) {
            if (e.which == 13) {
                var td = $(this).parent();
                td.removeClass("cellEditing");
                var newContent = $(this).val();
                td.text(newContent);
                if (td.hasClass('incorectData')) {
                    td.append("<img src='/shop/images/editCell.png' class='img-edit'/>");
                }
            }
        });

        $(this).children().first().blur(function () {
            var td = $(this).parent();
            td.removeClass("cellEditing");
            var newContent = $(this).val();
            td.text(newContent);
            if (td.hasClass('incorectData')) {
                td.removeClass('incorectData');
                //td.append("<img src='/shop/images/editCell.png' class='img-edit'/>");
            }
            var cellIndex = td.cellIndex
            console.log(td)
            var count = td.closest('table').find('th').eq(td.index()).find('.side_th').length;
            if (count > 0) {
                CPLBOM.autoReMapColumns();
            }
        });
    },
    generateXml: function (callback) {

        var isValidMap = CPLBOM.getMappedColumn();
        if (!isValidMap) {
            Core.showMessage("errorMsg", true, CPLBOM.captions.RequiredMappingColumns, CPLBOM.errorMSGInterval);
            return;
        }
        var xmlData = [];
        var mountingData = {}
        var layerData = {}

        var invalidRowa = ''
        $('.mounting.distinctOption').find('.option').each(function (index) {
            var mountingType = $.trim($(this).find('.mapValue select option:selected').val());
            var mountingVal = $.trim($(this).find('.mapTitle').text().toLowerCase());
            mountingData[mountingVal] = mountingType;
        });
        $('.layerOp.distinctOption').find('.option').each(function (index) {
            var layerType = $.trim($(this).find('.mapValue select option:selected').val());
            var layerVal = $.trim($(this).find('.mapTitle').text().toLowerCase());
            layerData[layerVal] = layerType;
        });
        var CenterXtype = $('.comp_x_th .comp_x option:selected').val().toLowerCase();
        var CenterYtype = $('.comp_y_th .comp_y option:selected').val().toLowerCase();
        $('#bomTable tbody').find('tr').each(function (index) {
            var isvalidXML = true;
            var row = $(this);
            row.find('.incorectData').removeClass('incorectData');
            row.find('.incorrectDataLink').remove();
            if (row.hasClass('selectedRow')) {
                xmlNode = {};
                var rowid = parseInt($.trim(row.find('.index').text()));
                jQuery.each(CPLBOM.mappedColumnName, function (columnName, mappedValue) {
                    if (columnName == 'ref_des') {
                        xmlNode.refdes = $.trim(row.find('.' + CPLBOM.mappedColumnName['ref_des'] + '').text());
                    }
                    if (columnName == 'supplier') {
                        xmlNode.suppl = $.trim(row.find('.' + CPLBOM.mappedColumnName['supplier'] + '').text());
                    }
                    if (columnName == 'library') {
                        xmlNode.libr = $.trim(row.find('.' + CPLBOM.mappedColumnName['library'] + '').text());
                    }
                    if (columnName == 'comment') {
                        xmlNode.comm = $.trim(row.find('.' + CPLBOM.mappedColumnName['comment'] + '').text());
                    }
                    if (columnName == 'url') {
                        xmlNode.url = $.trim(row.find('.' + CPLBOM.mappedColumnName['url'] + '').text());
                    }
                    if (columnName == 'value') {
                        xmlNode.val = $.trim(row.find('.' + CPLBOM.mappedColumnName['value'] + '').text());
                    }
                    else if (columnName == 'mpn') {
                        xmlNode.mpn = $.trim(row.find('.' + CPLBOM.mappedColumnName['mpn'] + '').text());
                    }
                    else if (columnName == 'manufacturer') {
                        xmlNode.manuf = $.trim(row.find('.' + CPLBOM.mappedColumnName['manufacturer'] + '').text());
                    }
                    else if (columnName == 'package') {
                        xmlNode.pack = $.trim(row.find('.' + CPLBOM.mappedColumnName['package'] + '').text());
                    }
                    else if (columnName == 'mounting') {
                        var value = $.trim(row.find('.' + CPLBOM.mappedColumnName['mounting'] + '').text());
                        var mounting_value = "";
                        if (value.toLowerCase() in mountingData) {
                            mounting_value = mountingData[value.toLowerCase()];
                        }
                        //if (mounting_value == "") {
                        //    CPLBOM.inCorrectDataLink(value, row, columnName);
                        //    isvalidXML = false;
                        //}
                        xmlNode.mnt = mounting_value;
                    }
                    else if (columnName == 'description') {
                        xmlNode.desc = $.trim(row.find('.' + CPLBOM.mappedColumnName['description'] + '').text());
                    }
                    else if (columnName == 'comp_y') {
                        var center_y_value = $.trim(row.find('.' + CPLBOM.mappedColumnName['comp_y'] + '').text()).toLowerCase();
                        center_y_value = center_y_value.replace('mm', '').replace('millimeters', '').replace('millimeter', '').replace('inches', '').replace('inch', '').replace('in', '').replace('mil', '').replace(/\,/g, '.');
                        center_y_value = $.trim(center_y_value);
                        if ($.isNumeric(center_y_value)) {
                            if (CenterYtype == 'inch') {
                                center_y_value = CPLBOM.convertInchToMM(center_y_value);
                            }
                            else if (CenterYtype == 'mil') {
                                center_y_value = CPLBOM.convertMilToMM(center_y_value);
                            }
                        }
                        else {
                            isvalidXML = false;
                            CPLBOM.inCorrectDataLink(center_y_value, row, columnName);
                        }
                        xmlNode.y = center_y_value;
                    }
                    else if (columnName == 'comp_x') {
                        var center_x_value = $.trim(row.find('.' + CPLBOM.mappedColumnName['comp_x'] + '').text()).toLowerCase();
                        center_x_value = center_x_value.replace('mm', '').replace('millimeters', '').replace('millimeter', '').replace('inches', '').replace('inch', '').replace('in', '').replace('mil', '').replace(/\,/g, '.');
                        center_x_value = $.trim(center_x_value);

                        if ($.isNumeric(center_x_value)) {
                            if (CenterXtype == 'inch') {
                                // Convert to Millimeter
                                center_x_value = CPLBOM.convertInchToMM(center_x_value);
                            }
                            else if (CenterYtype == 'mil') {
                                center_x_value = CPLBOM.convertMilToMM(center_x_value);
                            }
                        }
                        else {
                            isvalidXML = false;
                            CPLBOM.inCorrectDataLink(center_x_value, row, columnName);
                        }
                        xmlNode.x = center_x_value;
                    }
                    else if (columnName == 'side') {

                        var layer = $.trim(row.find('.' + CPLBOM.mappedColumnName['side'] + '').text());
                        var layer_value = "";
                        if (layer.toLowerCase() in layerData) {
                            layer_value = layerData[layer.toLowerCase()];
                        }
                        if (layer_value == "") {
                            isvalidXML = false;
                            CPLBOM.inCorrectDataLink(layer, row, columnName);
                        }
                        xmlNode.layer = layer_value;
                    }
                    else if (columnName == 'rotation') {
                        var rotation_value = $.trim(row.find('.' + CPLBOM.mappedColumnName['rotation'] + '').text());
                        rotation_value = rotation_value == "" ? 0 : rotation_value.replace(/\,/g, '.');
                        if (!$.isNumeric(rotation_value)) {
                            isvalidXML = false;
                            CPLBOM.inCorrectDataLink(rotation_value, row, columnName);
                        }
                        xmlNode.rot = rotation_value;
                    }
                });
                if (!CPLBOM.mappedColumnName.side) {
                    xmlNode.layer = CPLBOM.layer.toLowerCase();
                }
                xmlData.push(xmlNode);
                if (!isvalidXML) {
                    invalidRowa = invalidRowa + rowid + ' ,';
                }
            }
        });
        if (invalidRowa == "") {
            var jsonExport = JSON.stringify(xmlData);
            var url = "/shop/assembly/CPLBomImport.aspx/SaveBOM";
            var postData = {
                jsonExport: jsonExport,
                rootNode: 'components',
                fileName: CPLBOM.basketNr,
                number: CPLBOM.basketNr,
                mapData: JSON.stringify(CPLBOM.getSystemMappedCol()),
                layer: CPLBOM.layer,
                isMerge: CPLBOM.merge
            }
            Core.post(url, postData, function (result) {
                CPLBOM.layer = '';
                CPLBOM.merge = false;
                var dtObj = result;
                if (dtObj.Status == 0) {
                    Core.showMessage("errorMsg", true, CPLBOM.captions.FileUploadError, CPLBOM.errorMSGInterval);
                    jsonExport = "";
                    return;
                }
                else {
                    parent.cplLoaded(dtObj.FileName);
                    parent.hidePopup();
                    $('.bom-step, .btnNext').addClass('hide');
                    CPLBOM.currentStep = CPLBOM.steps.verifyParts;
                    CPLBOM.previousStep = CPLBOM.steps.showMapColumnStep;
                }
            });
        }
        else {
            $('.btnNext').removeClass('hide');
            Core.showMessage("errorMsg", true, CPLBOM.captions.RowNumber + invalidRowa + CPLBOM.captions.RowNumberContainsInvalidData);
        }
    },
    clearCheckBox: function () {
        $('#bomTable tbody tr td.td-index').each(function () {
            var row = $(this);
            if (row.find('input[type="checkbox"]').is(':checked')) {
                row.find('input[type="checkbox"]').removeAttr('checked');
            }
        });
    },
    getSystemMappedCol: function () {
        var data = [];
        jQuery.each(CPLBOM.mappedColumnName, function (systemCol, userCol) {
            var bomCol = $.grep(CPLBOM.serverData.BOMCols, function (element, index) {
                return element.name == systemCol;
            });
            data.push({ bomColId: bomCol[0].id, userCol: userCol })
        });
        return data;
    },
    getIndexBySNo: function (sno) {
        var index = 0;
        $('#bomTable tbody tr').each(function () {
            var row = $(this).find('td:first-child');

            var rowIndex = parseInt($.trim(row.text())) || 0;
            if (rowIndex == sno) {
                index = parseInt(row.find('span').data('index'));
            }
        });
        return index;
    },
    getDelimiterName: function (delimiter) {
        var name = "none";
        var radioBtnId = "";
        if (delimiter == ",") {
            name = "Comma";
            radioBtnId = "#rdoComma";
        }
        else if (delimiter == "|") {
            name = "Pipe";
            radioBtnId = "#rdoPipe";
        }
        else if (delimiter == ";") {
            name = "Semicolon";
            radioBtnId = "#rdoSemicolon";
        }
        else if (delimiter == "\t") {
            name = "Tab";
            radioBtnId = "#rdoTab";
        }
        else if (delimiter != "") {
            name = delimiter;
        }

        if (radioBtnId != "") {
            $(radioBtnId).prop('checked', true);
        }

        return name;
    },
    addOption: function (classname, options) {
        var opts = '';
        jQuery.each(options, function (key, obj) {
            if ($.trim(classname.toLowerCase()) == $.trim(obj.toLowerCase()) || ($.trim(classname.toLowerCase()) == 'bot' && $.trim(obj.toLowerCase()) == 'bottom') || ($.trim(classname.toLowerCase()) == 'b' && $.trim(obj.toLowerCase()) == 'bottom') || ($.trim(classname.toLowerCase()) == 't' && $.trim(obj.toLowerCase()) == 'top')) {
                opts += '<option selected value="' + $.trim(obj.toLowerCase()) + '">' + obj + '</option>';
            }
            else {
                opts += '<option  value="' + $.trim(obj.toLowerCase()) + '">' + obj + '</option>';
            }
        });
        var option =
           '<div class="option ">' +
              '<div class="mapTitle">' + classname + '</div>' +
              '<div class="mapValue">' +
              '<select value="' + classname + '" class="' + classname + '">' +
                  '<option value="">-- none --</option>' + opts
        '</select>' +
        '</div>' +
        '</div>';
        return option;
    },
    getMappedColumn: function () {
        CPLBOM.mappedColumnName = {};
        var count = 0;
        $('.dropdown').each(function (index, element) {
            var value = $(this).val();

            if (value == "ref_des" || value == "comp_x" || value == "comp_y" || value == "rotation") { //|| value == "side"
                count = parseInt(count) + 1;
            }
            if ($.trim(value) != '' && $.trim(value) != null && $.trim(value) != "none") {
                var column = $(this).attr('col');
                CPLBOM.mappedColumnName[value] = column;
            }
        });
        if (count > 3) {//3
            return true
        }
        return false;
    },
    getDropDown: function (key) {
        return '<select class="form-control dropdown" col="' + key + '">' + this.mapDropdownOptions + '</select>';
    },
    getParameterByName: function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    init: function () {
        CPLBOM.events();
        CPLBOM.clearAll();
        CPLBOM.onLoad();
    },
    inCorrectDataLink: function (text, row, columnName) {
        if (row.find('.' + CPLBOM.mappedColumnName[columnName] + '').find('img').length == 0) {
            row.find('.' + CPLBOM.mappedColumnName[columnName] + '').append("<img src='/shop/images/editCell.png' class='img-edit' />");
        }
        row.find('.' + CPLBOM.mappedColumnName[columnName] + '').addClass('incorectData');
        return;
    },
    loadTable: function (callback, forceColumnMap, checkCustFile, isReadData, isSaveTextArea, isMultipleExcelChange, deleteRowIndex) {
        var postData = new FormData();
        var fileUpload = $(".fileUploader").get(0);
        if (this.currentStep == this.steps.uploadData && CPLBOM.multipleExcelSheet == false) {
            var files = fileUpload.files;
            for (var i = 0; i < files.length; i++) {
                postData.append(files[i].name, files[i]);
            }
        }
        var isHeader = CPLBOM.headerRow > 0 ? 'YES' : 'NO';
        var headerRow
        if (CPLBOM.headerRow == 0) {
            headerRow = CPLBOM.headerRow == 0 ? 0 : CPLBOM.getIndexBySNo(CPLBOM.headerRow);
        }
        else {
            headerRow = CPLBOM.headerRow;
        }
        postData.append("isHeader", isHeader);
        postData.append("path", "/shop/assembly/uploads/"); // ecpath        
        postData.append("hRow", headerRow);
        postData.append("separator", CPLBOM.delimiter);
        postData.append("fixdlenthcolumn", CPLBOM.fixdLenthColumn);
        postData.append("entitynr", CPLBOM.basketNr);
        postData.append("bomfiletype", "CPL");
        postData.append("sheet", CPLBOM.selectedSheet);
        if (checkCustFile) {
            postData.append("checkCustFile", checkCustFile);
        }
        if ($("#textAreaPdf").val().trim() != '') {
            postData.append("clientData", $("#textAreaPdf").val());
            $('#textAreaPdf').val('');
        }
        if (forceColumnMap) {
            postData.append("forceColumnsMap", forceColumnMap);
        }
        if (isReadData) {
            postData.append("isReadData", isReadData);
        }
        if (isMultipleExcelChange) {
            postData.append("isMultipleExcelChange", isMultipleExcelChange);
        }
        if (deleteRowIndex != undefined) {
            postData.append("deleteRowIndex", deleteRowIndex);
        }
        var url = "/shop/assembly/uploadHandler.ashx";
        Core.post(url, postData, function (result) {
            if (CPLBOM.delimiter == 'none') {
                if (result.headers != null || result.headers != undefined) {
                    CPLBOM.getDelimiterName(result.headers[2]);
                }
            }
            var checkCustomer = checkCustFile;
            checkCustFile = false;
            if (result.Status == 0) {
                Core.showMessage("errorMsg", true, CPLBOM.captions.ErrorInFileUpload + " " + result.Msg, CPLBOM.errorMSGInterval);
                $(".divTable").removeClass('hide');
                return;
            }

            if (result.fileExtension != '' || result.fileExtension != undefined) {
                CPLBOM.fileExtension = result.fileExtension;
            }
            //if (result.Status == 1) {
            //    CPLBOM.fileExtension = result.fileExtension;
            //}
            //if (result.Status == 2 && checkCustFile == false) {
            //if (!confirm(CPLBOM.captions.XMLExist)) {
            //    return;
            //}
            //$('.confirmModal').removeClass('hide');                
            //}

            if (result.Status == 3 && checkCustomer == undefined) {
                CPLBOM.fileExtension = 'pdf';
                $('#stepActionbar').show();
                $("#divTable").hide();
                CPLBOM.showNextStep();
                $('.stepTextBoxData').removeClass('hide');
                $('#btnSubmitPdfDoc').val(CPLBOM.captions.Save);
                $('#btnCancelPdfDoc').val(CPLBOM.captions.Cancel);
                $('#btnSubmitPdfDoc').show();
                $('#btnCancelPdfDoc').show();
                $('#textAreaPdf').show();
                $('#textAreaPdf').focus();
                $('#blockLoading').show();
                return;
            }
            if (result.Status == 4) {
                if (result.data.length > 0) {
                    CPLBOM.fileExtension = 'xls';
                    $("#divTable").hide();
                    $('#btnSubmitPdfDoc').val(CPLBOM.captions.Save);
                    $('#btnCancelPdfDoc').val(CPLBOM.captions.Cancel);
                    $('#btnSubmitPdfDoc').show();
                    $('#btnCancelPdfDoc').show();
                    $('#textAreaPdf').show();
                    $('#textAreaPdf').val(result.data);
                    $('#textAreaPdf').focus();
                }
                return;
            }
            if (result.Status == 5) {
                $('#fileUploader').click();
                return;
            }

            CPLBOM.showConfirmDialog(result, callback, isSaveTextArea, checkCustomer, deleteRowIndex);
        }, false);
    },

    showConfirmDialog: function (result, callback, isSaveTextArea, checkCustomer, deleteRowIndex) {
        if (result.Status == 2 || (result.Status == 3 && checkCustomer == true)) {
            if (result.fileName) {
                var confirmMsgText = $('#confirmFileMsg').text();
                confirmMsgText = confirmMsgText.replace('##FileName', result.fileName);
                $('#confirmFileMsg').text(confirmMsgText);
            }
            $('.confirmModal').removeClass('hide');
        }
        else {
            CPLBOM.confirmCallback(result, callback, isSaveTextArea, deleteRowIndex);
        }
        $('#btnOverwrite').click(function () {
            $('.confirmModal').addClass('hide');
            if (result.extension.toLowerCase() == ".pdf" || result.extension.toLowerCase() == ".doc" || result.extension.toLowerCase() == ".docx" || result.extension.toLowerCase() == ".zip" || result.extension.toLowerCase() == ".rar" || result.extension.toLowerCase() == ".brd") {
                CPLBOM.fileExtension = 'pdf';
                $('#stepActionbar').show();
                $("#divTable").hide();
                CPLBOM.showNextStep();
                $('.stepTextBoxData').removeClass('hide');
                $('#btnSubmitPdfDoc').val(CPLBOM.captions.Save);
                $('#btnCancelPdfDoc').val(CPLBOM.captions.Cancel);
                $('#btnSubmitPdfDoc').show();
                $('#btnCancelPdfDoc').show();
                $('#textAreaPdf').show();
                $('#textAreaPdf').focus();
                $('#blockLoading').show();
                return;
            }
            CPLBOM.confirmCallback(result, callback, isSaveTextArea);
        });
        $('#btnMerge').on('click', function () {
            $('.confirmModal').addClass('hide');
            CPLBOM.merge = true;
            $('#fileUploader').click();
            return;
        });

        $('#btnCancel,.ec-close').on('click', function () {
            $('.confirmModal').addClass('hide');
            return;
        });
        $('#btnLoadNew').on('click', function () {
            $('.confirmModal').addClass('hide');
            $('#fileUploader').click();
            return;
        });
    },
    confirmCallback: function (result, callback, isSaveTextArea, deleteRowIndex) {

        if (result.data.length > 0) {
            CPLBOM.setTable(result.data);

            $('#bomTable tbody tr').each(function () {
                var row = parseInt($.trim($(this).find('td:first-child').text())) || 0;
                if (row == 0) {
                    $(this).addClass('hide');
                    return;
                }
            });
            CPLBOM.selectedRows(false);
            $(".divTable").removeClass('hide');
            $('#uploadFile').val("Reload");
            $('.sno').text('');
        }
        if (result.excelSheets != null && result.excelSheets.length > 0) {
            CPLBOM.excel = true;
        }
        if (result.excelSheets != null && result.excelSheets.length > 1) {
            //CPLBOM.selectedSheet = result.excelSheets[0].name;
            CPLBOM.multipleExcelSheet = true;
            var opts = '';
            $.each(result.excelSheets, function (index, sheet) {
                if (CPLBOM.selectedSheet == sheet.name) {
                    opts += '<option  value="' + sheet.number + '" selected>' + sheet.name + '</option>';
                }
                else {
                    opts += '<option  value="' + sheet.number + '">' + sheet.name + '</option>';
                }
            });
            $('#drpWorkShee').find('option').remove().end().append(opts);
            $('.stepMultipleSheet').removeClass('hide');
            $('.btnBack').addClass('bom_btnuploaddata');
            $('.btnNext').addClass('bom_btnnext');
            $('.btnBack').val(CPLBOM.captions.UploadNewCPL); //"Upload new BOM"
            $('.btnBack').addClass('btnbig');
            $('#uploadFile').val("Reload");
            $('#stepActionbar').show();
            $('.step1').addClass('hide');
            return
        }
        if (deleteRowIndex != undefined) {
            if (CPLBOM.currentStep == CPLBOM.steps.mapColumns) {
                CPLBOM.showMapColumnStep();
            }
        }
        if (callback) {
            callback(result.headers, isSaveTextArea);
        }
    },
    message: function (type, text, classname) {
        $('.message').removeClass('hide').text(text);
    },
    steps: {
        uploadData: 1,
        chooseDelimiter: 2,
        dataSelction: 3,
        mapColumns: 4,
        verifyParts: 5
    },
    openMapBox: function (ele, event) {
        var classname = $(ele).attr('class');
        classname = $.trim(classname.replace('mapColumn', ''));
        var offset = $(ele).parent('th').offset();
        var width = $(ele).parent('th').width();

        var popupLeft = (parseInt(width) / 2) + offset.left - 98;
        if (popupLeft < 31) {
            popupLeft = 0;
        }

        if (classname.indexOf("_th") >= 0) {
            var height = $('.arrow_box.' + classname).height();
            var headerHeight = $('.header ').height();
            var top = headerHeight - height;
            top = top < 0 ? 0 : top;
            var fullWidth = $(window).width();
            var pWidth = fullWidth - popupLeft;
            if (pWidth < 200) {
                popupLeft = fullWidth - 210;
            }
            $('.arrow_box.' + classname).hide();
            $('.arrow_box.' + classname).show();
            $('.arrow_box.' + classname).css({ 'left': popupLeft + 'px', 'top': top });
        }
    },
    onLoad: function () {
        $("#textAreaPdf").height($(window).height() - 268);
        CPLBOM.clearAll();
        this.serverData = $.parseJSON($('#txtData').val());
        CPLBOM.captions = this.serverData.Captions;
        this.setMapDropdownOptions(this.serverData.BOMCols);
        var height = ($(window).height() - $('.header').height()) - 40;
        $('.divTable').css({ 'height': height + 'px' });
        CPLBOM.basketNr = Core.getParameterByName('id');
        CPLBOM.attached = true;
        CPLBOM.loadTable(CPLBOM.loadTableCallback, false, true);
        CPLBOM.applyCssFirefox();
    },
    applyCssFirefox: function () {
        if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('.bom_btnuploaddata, .bom_btnnext, .bom_btntextedit, .bom_btndelete, .bom_adddata, .bom_back, .bom_submit, .bom_cancel, .bom_reload, .bom_save').css({
                'background-position-y': '7px',
            })
        }
    },
    setMapDropdownOptions: function (data) {
        this.mapDropdownOptions = '<option value="none">-- None --</option>';
        for (i = 0; i < data.length; i++) {
            this.mapDropdownOptions += '<option value="' + data[i].name + '">' + data[i].display_name + '</option>';
        };
    },
    selectedRows: function (isMap) {
        //CPLBOM.hideDeletedRows();
        if (CPLBOM.headerRow != 0 || CPLBOM.startRow != 0 || CPLBOM.endRow != 0 || isMap) {
            $('#bomTable tbody tr').removeClass('selectedRow');
            $('#bomTable tbody tr').each(function () {
                //var srNo = $(this).children().eq(0).text();
                var row = parseInt($.trim($(this).find('td:first-child').text()));
                $(this).removeClass('unselected');
                //var currentRow = $(this);
                //if (srNo != '') {
                //    if (CPLBOM.removeRows.length > 0) {
                //        for (var i = 0; i < CPLBOM.removeRows.length; i++) {
                //            if (srNo == CPLBOM.removeRows[i]) {
                //                currentRow.addClass('tr-remove');
                //                break;
                //            }
                //        }
                //    }
                //}
                var startingRow = CPLBOM.startRow;
                var endingRow = CPLBOM.endRow;
                var headRow = CPLBOM.headerRow;

                startingRow = startingRow == 0 ? headRow + 1 : startingRow;
                endingRow = endingRow == 0 ? -1 : endingRow;

                if (headRow == row) {
                    $(this).addClass('selectedHeader');
                }

                // UnSelected row upto header row 
                if (row < startingRow && isMap) {
                    $(this).addClass('unselected');
                }

                if (endingRow != -1 && endingRow < row && isMap) {
                    $(this).addClass('unselected');
                }

                if (endingRow == -1 && startingRow <= row) {
                    // selected all rows after start rows
                    if (!$(this).hasClass('hide')) {
                        $(this).addClass('selectedRow');
                    }
                }

                if (endingRow != -1 && startingRow <= row && endingRow >= row) {
                    // selected rows between start row and end row 
                    if (!$(this).hasClass('hide')) {
                        $(this).addClass('selectedRow');
                    }
                }
                //Unselect row if row is blank
                var blankRow = true;
                $(this).find('td').each(function () {
                    if (!$(this).hasClass('td-index') && $.trim($(this).html()) != "") {
                        blankRow = false;
                    }
                });

                if (blankRow) {
                    $(this).removeClass('selectedRow').addClass('unselected');
                }

                if (CPLBOM.currentStep == CPLBOM.steps.dataSelction) {
                    $(this).removeClass('tr-css');
                }
                else {
                    $(this).addClass('tr-css')
                }
            });
        }
        if (CPLBOM.currentStep != CPLBOM.steps.dataSelction) {
            $('#bomTable tbody tr').removeClass('selectedHeader');
        }
        $('.pageloader,.loaderBG').addClass('hide');
    },
    //hideDeletedRows: function () {
    //    $('#bomTable tbody tr').each(function () {
    //        var srNo = $(this).children().eq(0).text();
    //        var currentRow = $(this);
    //        if (srNo != '') {
    //            if (CPLBOM.removeRows.length > 0) {
    //                for (var i = 0; i < CPLBOM.removeRows.length; i++) {
    //                    if (srNo == CPLBOM.removeRows[i]) {
    //                        currentRow.addClass('tr-remove');
    //                        break;
    //                    }
    //                }
    //            }
    //        }
    //    });
    //},
    setTable: function (data) {
        var columnname = [];
        var colIndex = 0;
        var columns = ["Index"];
        columnname.push({
            data: "Index",
            title: ' Row Nr.',
            defaultContent: "",
            className: "td-index",
            render: function (data, type, row) {
                data = "";
                if (type === 'display') {
                    var visible = row.Isvisible;
                    if (visible != "true") {
                        return "";
                    }
                    colIndex = colIndex + 1;
                    var index = parseInt(row.Index) || 0;
                    if (row.Index > 0) {
                        data = colIndex;
                    }
                    return '<input class="chkDelete" type="checkbox" style="vertical-align: inherit !important;cursor:pointer;"> <span class="index bold" data-index="' + index + '">' + data;//+ '</span><img class="imgDelete" title="Delete" style="height:13px;cursor:pointer;display:none;" src="resources/dustbin.png?v=0.2"/>';
                }
                return data;
            }
        });

        var row = $('<tr>');
        row.append('<td class="sno"> </td>'); // Index column
        jQuery.each(data[0], function (key, obj) {
            var dtKey = $.trim(key).toLowerCase();
            if (dtKey != "index") {
                var title = key.replace(/_/g, ' ');
                if (dtKey == "isvisible") {
                    columnname.push({ "data": key, "title": title, "className": dtKey, "visible": false });
                }
                else {
                    columnname.push({ "data": key, "title": title, "className": dtKey });
                }
                row.append('<td userColumn=' + dtKey + '>' + CPLBOM.getDropDown(dtKey) + '</td>');
            }
        });
        if ($.fn.DataTable.isDataTable('#bomTable')) {
            CPLBOM.bomTable.destroy();
        }
        $('#bomTable thead, #bomTable tbody').empty();
        //TODO: make bomTable glob obj and refere everywher to shorten code
        CPLBOM.bomTable = $('#bomTable').DataTable({
            paging: false,
            ordering: false,
            searching: true,
            info: false,
            retrieve: true,
            data: data,
            columns: columnname
        });
        CPLBOM.bomTable.row.add(row);
        $('#bomTable tbody').prepend(row);
        CPLBOM.applyFirstTdCss();
    },
    applyFirstTdCss: function () {
        $('#bomTable tbody tr td.td-index').each(function () {
            $(this).css("background", "rgb(243, 243, 243)");
        });
    },
    showNextStep: function (btnNext) {
        CPLBOM.clearCheckBox();
        if (!CPLBOM.attached) {
            Core.showMessage("errorMsg", true, CPLBOM.captions.RequiredFile, CPLBOM.errorMSGInterval);
            return;
        }
        if (!CPLBOM.validateInputData()) {
            return;
        }
        $('#stepActionbarVerifyParts, #btnSave ,.message').addClass('hide');
        $('.btnBack').val(CPLBOM.captions.Back);
        $('.btnBack').removeClass("hide");
        $('.btnBack').removeClass('btnbig');
        $('#errorMsg').hide();

        if (this.currentStep == this.steps.uploadData) {
            if (!CPLBOM.excel) {
                CPLBOM.showDelimiterStep();
                $('.btnBack').addClass('bom_btnuploaddata');
                $('.btnNext').addClass('bom_btnnext');
            }
            else {
                CPLBOM.showDataSelectionStep(false, true);
                $('.btnBack').removeClass('bom_btnuploaddata').addClass('bom_back');
                if (CPLBOM.excel == true && CPLBOM.multipleExcelSheet == false) {
                    $('.btnBack').removeClass('bom_back').addClass('bom_btnuploaddata');
                }
            }
        }
        else if (this.currentStep == this.steps.chooseDelimiter) {
            CPLBOM.fixdLenthColumn = "";
            if ($('#rdoFixedWidth').is(':checked')) {
                $('span.SpanIndex.fwseparaterfirst.fwseparater').each(function (index, element) {

                    var value = $(element).attr('data-charnumber');
                    CPLBOM.fixdLenthColumn = CPLBOM.fixdLenthColumn + value + "/";
                });
                CPLBOM.fixdLenthColumn = CPLBOM.fixdLenthColumn + "0";
                this.showDataSelectionStep(true, true);
                $('.btnBack').removeClass('bom_btnuploaddata').addClass('bom_back');
            }
            else {
                var delimitor = '';
                if ($('input[type=radio][name=delimiter]').is(':checked')) {
                    delimitor = $("input[type=radio][name=delimiter]:checked").val();
                }

                if (delimitor == "") {
                    $('#stepActionbarVerifyParts, #btnSave, .message').removeClass('hide');
                    $('.btnBack').val(CPLBOM.captions.UploadNewCPL); //UploadNewBOM
                    $('.btnBack').removeClass("hide");
                    $('.btnBack').addClass('btnbig');
                    Core.showMessage("errorMsg", true, CPLBOM.captions.RequiredDelimitor, CPLBOM.errorMSGInterval);
                    return;
                }
                if (delimitor == 'Other') {
                    var delimitorValue = $('.txtOther').val();
                    if (delimitorValue.length == 0) {
                        Core.showMessage("errorMsg", true, CPLBOM.captions.RequiredDelimitor, CPLBOM.errorMSGInterval);
                        return;
                    }
                }
                this.showDataSelectionStep(false, true);
                $('.btnBack').removeClass('bom_btnuploaddata').addClass('bom_back');
            }
        }
        else if (this.currentStep == this.steps.dataSelction) {
            this.showMapColumnStep();
            $('.btnNext').removeClass('bom_btnnext').addClass('bom_submit');
            if (CPLBOM.excel == true && CPLBOM.multipleExcelSheet == false) {
                $('.btnBack').removeClass('bom_btnuploaddata').addClass('bom_back');
            }
        }
        else if (this.currentStep == this.steps.mapColumns) {
            this.showVerifyPartsStep();
        }
    },
    showPreviousStep: function (btnBack) {
        CPLBOM.clearCheckBox();
        if (!CPLBOM.attached) {
            Core.showMessage("errorMsg", true, CPLBOM.captions.RequiredFile, CPLBOM.errorMSGInterval);
            return;
        }
        $('.btnNext').removeClass('hide');
        $('#btnSave, #stepActionbarVerifyParts,.message').addClass('hide');
        $('.btnNext').val(CPLBOM.captions.Next);
        $('#errorMsg').hide();
        if (this.currentStep == this.steps.chooseDelimiter || this.currentStep == this.steps.uploadData) {
            this.showUploadStep();
            CPLBOM.clearAll();
            $('#btnSubmitPdfDoc').hide();
            $('#btnCancelPdfDoc').hide();
            $('#textAreaPdf').hide();
            $('#textAreaPdf').val('');
        }
        else if (this.currentStep == this.steps.dataSelction) {
            if (CPLBOM.excel) {
                //when exec is upload with multiple worksheet(for excel sheet section step)
                if ($('.btnBack').val() == CPLBOM.captions.UploadNewCPL || !CPLBOM.multipleExcelSheet) {
                    CPLBOM.showUploadStep();
                    CPLBOM.clearAll();
                    return;
                }
                $('.stepMultipleSheet').removeClass('hide');
                $('#uploadFile').val("Reload");
                $('#stepActionbar').show();
                $('.step1').addClass('hide');
                $('.step3').addClass('hide');
                $('.btnBack').val(CPLBOM.captions.UploadNewCPL);
                $('.btnBack').addClass('btnbig');
                CPLBOM.currentStep = CPLBOM.steps.uploadData;
                $('.btnBack').removeClass('bom_back').addClass('bom_btnuploaddata');
                //this.showUploadStep();
            }
            else {
                this.showDelimiterStep();
                $('.btnBack').removeClass('bom_back').addClass('bom_btnuploaddata');
                $('.btnNext').addClass('bom_btnnext');
            }
            $('#bomTable tbody tr').removeClass('selectedHeader').removeClass('selectedRow');
        }
        else if (this.currentStep == this.steps.mapColumns) {
            this.showDataSelectionStep(true);
            $('#bomTable').off('click', 'tbody td', CPLBOM.editableCell);
            $('.btnNext').removeClass('bom_submit').addClass('bom_btnnext');
        }
        else if (this.currentStep == this.steps.verifyParts) {
            this.showMapColumnStep();
        }
    },
    showUploadStep: function () {
        $('#bomTable').removeClass('fixedLenthFont');
        $('#editBOM').hide();
        this.currentStep = this.steps.uploadData;
        this.nextStep = this.steps.chooseDelimiter;
        if ($.fn.DataTable.isDataTable('#bomTable')) {
            CPLBOM.bomTable.destroy();
        }
        $('#stepActionbar').hide();
        $('#fileUploader').val('');
        $('.file-caption-name').text('No file chosen');
        $('#bomTable thead, #bomTable tbody').empty();
        $('.bom-step').addClass('hide');
        $('.step1').removeClass('hide');
        $('.btnBack,.stepMultipleSheet').addClass('hide');
    },
    showDelimiterStep: function () {
        $('#bomTable').addClass('fixedLenthFont');
        this.currentStep = this.steps.chooseDelimiter;
        this.nextStep = this.steps.dataSelction;
        this.previousStep = this.steps.uploadData;
        //CPLBOM.fixdLenthColumn = "";
        $('.btnBack').val(CPLBOM.captions.UploadNewCPL); //"Upload new BOM"
        $('.btnBack').addClass('btnbig');
        $('.bom-step').addClass('hide');
        $('.step2').removeClass('hide');
        $('#editBOM').show();
        if ($('#rdoFixedWidth').is(':checked')) {
            CPLBOM.previousStepFLColumn = CPLBOM.fixdLenthColumn;
            CPLBOM.fixdLenthColumn = "";
            CPLBOM.loadTable(CPLBOM.fixedLenthColumnMap);
        }
    },
    showDataSelectionStep: function (reloadTable, forceColumnMap) {
        $('#bomTable').removeClass('fixedLenthFont');
        $('#editBOM').hide();
        this.currentStep = this.steps.dataSelction;
        this.nextStep = this.steps.mapColumns;
        this.previousStep = this.steps.chooseDelimiter;
        CPLBOM.headerRow = 0; CPLBOM.startRow = 0; CPLBOM.endRow = 0;
        CPLBOM.loadTable(CPLBOM.dataSelectionCallback, true);
        //if (forceColumnMap && CPLBOM.headerRow == 0) {
        //    CPLBOM.loadTable(CPLBOM.dataSelectionCallback, true); //BOM.loadTableCallback instead of null
        //}
        //else if (reloadTable) {
        //    CPLBOM.loadTable();
        //}
        //else {
        //    CPLBOM.selectedRows(false);
        //}
        if (CPLBOM.excel && !CPLBOM.multipleExcelSheet) {
            $('.btnBack').val(CPLBOM.captions.UploadNewBOM);//"Upload new BOM"
            $('.btnBack').removeClass('bom_back').addClass('bom_btnuploaddata');
        }
        //if (CPLBOM.excel) {
        //    $('.btnBack').val(CPLBOM.captions.UploadNewCPL); //"Upload new BOM"
        //    $('.btnBack').addClass('btnbig');
        //}
        $('.bom-step').addClass('hide');
        $('.step3').removeClass('hide');
    },

    showMapColumnStep: function () {
        $('#bomTable').removeClass('fixedLenthFont');
        $('#editBOM').hide();
        this.currentStep = this.steps.mapColumns;
        this.nextStep = this.steps.verifyParts;
        this.previousStep = this.steps.dataSelction;

        $('.btnNext').val(CPLBOM.captions.Submit);
        $('.btnBack').val(CPLBOM.captions.Back).removeClass('btnbig');
        $('.bom-step').addClass('hide');
        $('.step4').removeClass('hide');

        $('#bomTable tbody tr:first-child').removeClass('hide');
        CPLBOM.selectedRows(true);
        CPLBOM.autoMapColumns();

        $('#bomTable').on('click', 'tbody td', CPLBOM.editableCell);
        $('.selectedRow').css({ 'cursor': 'pointer' });
    },
    showVerifyPartsStep: function () {

        console.log(CPLBOM.mappedColumnName);
        if (!CPLBOM.mappedColumnName.side) {
            $("#radioTop").prop("checked", true);
            $('.sideModal').removeClass('hide');
            return;
        }
        this.generateXml();
    },
    uploadFile: function (e) {

        $(".divTable").addClass('hide');
        //CPLBOM.clearAll();
        $('#textAreaPdf').val('');
        var num = Core.getParameterByName('id');
        CPLBOM.basketNr = num;
        if (CPLBOM.basketNr == "" || CPLBOM.basketNr == null) {
            Core.showMessage("errorMsg", true, CPLBOM.captions.RequireOrderOrBasket, CPLBOM.errorMSGInterval);
            return;
        }

        var filename = [].slice.call(e.target.files).map(function (f) {
            return f.name;
        });
        var fileUpload = $(".fileUploader").get(0);
        var files = fileUpload.files;

        if (files.length == 0) {
            Core.showMessage("errorMsg", true, CPLBOM.captions.RequiredFile, CPLBOM.errorMSGInterval);
            return;
        }
        if (files[0].size > 15360000) {
            Core.showMessage("errorMsg", true, CPLBOM.captions.FileSizeLong, CPLBOM.errorMSGInterval);
            return;
        }

        $('.file-caption-name').text(filename);
        $('.file-caption').attr("title", filename);
        CPLBOM.attached = true;
        var extension = filename[0].substr((filename[0].lastIndexOf('.') + 1)).toLowerCase();
        CPLBOM.fileExtension = extension;
        if (extension == 'ade' || extension == 'adp' || extension == 'bat' || extension == 'chm' || extension == 'cmd' || extension == 'com' || extension == 'cpl' || extension == 'exe' || extension == 'hta'
       || extension == 'ins' || extension == 'isp' || extension == 'jse' || extension == 'lib' || extension == 'lnk' || extension == 'mde' || extension == 'msc' || extension == 'msp' || extension == 'mst' ||
       extension == 'scr' || extension == 'pif' || extension == 'sct' || extension == 'shb' || extension == 'sys' || extension == 'vb' || extension == 'vbe' || extension == 'vbs' || extension == 'vxd' ||
       extension == 'wsc' || extension == 'wsf' || extension == 'wsh') {
            Core.showMessage("errorMsg", true, CPLBOM.captions.MsgBOMFileSelect, CPLBOM.errorMSGInterval);
            CPLBOM.clearAll();
            CPLBOM.currentStep = CPLBOM.steps.uploadData;
            $('#fileUploader').val('');
            $('.file-caption-name').text('No file chosen');
            $('.stepTextBoxData').addClass('hide');
            $('#btnSubmitPdfDoc').hide();
            $('#btnCancelPdfDoc').hide();
            $('#textAreaPdf').hide();
            return;
        }

        if (extension == 'pdf' || extension == 'doc' || extension == 'docx' || extension == 'zip' || extension == 'rar' || extension == 'brd') {
            $('.stepTextBoxData').removeClass('hide');
            $('#btnSubmitPdfDoc').val(CPLBOM.captions.Save);
            $('#btnCancelPdfDoc').val(CPLBOM.captions.Cancel);
            $('#btnSubmitPdfDoc').show();
            $('#btnCancelPdfDoc').show();
            $('#textAreaPdf').show();
            $('#textAreaPdf').focus();
            //return;
        }

        if (extension == "xls" || extension == "xlsx" || extension == "ods") {
            CPLBOM.excel = true;
        }
        CPLBOM.loadTable(CPLBOM.loadTableCallback);

    },
    loadTableCallback: function (headers, saveTextArea) {
        $('#stepActionbar').show('hide');
        if (headers == null) {
            CPLBOM.showNextStep();
        }
        else {
            CPLBOM.mappedColumnName = headers[0];

            CPLBOM.delimiter = CPLBOM.getDelimiterName(headers[2]) || "none";

            if (CPLBOM.mappedColumnName == null && CPLBOM.delimiter == "none") {
                if (saveTextArea) {
                    return;
                }
                CPLBOM.showNextStep();
                return;
            }

            CPLBOM.headerRow = headers[1];
            $('#headerRow').val(CPLBOM.headerRow);

            var startRow = CPLBOM.headerRow != 0 ? (CPLBOM.headerRow + 1) : 1;
            $('#startRow').val(startRow);
            if (saveTextArea) {
                return;
            }
            CPLBOM.showNextStep();
        }
    },

    fixedLenthColumnMap: function () {
        var columns = CPLBOM.previousStepFLColumn.split("/");
        $.each(columns, function (index, value) {
            if (value != "0") {
                $('span[data-charnumber=' + value + ']').each(function () {
                    var value = $(this).text();
                    $(this).text(value + '|');
                    $(this).addClass('fwseparater');
                });
            }
        });
    },

    dataSelectionCallback: function (headers) {
        $('#stepActionbar').show('hide');
        if (headers == null) {
            return;
        }
        CPLBOM.mappedColumnName = headers[0];
        CPLBOM.delimiter = CPLBOM.getDelimiterName(headers[2]) || "none";
        CPLBOM.headerRow = headers[1];
        $('#headerRow').val(CPLBOM.headerRow);
        var startRow = CPLBOM.headerRow != 0 ? (CPLBOM.headerRow + 1) : 1;
        $('#startRow').val(startRow);
        CPLBOM.selectedRows();
    },
    validateInputData: function () {
        if ((CPLBOM.startRow <= CPLBOM.headerRow) && (CPLBOM.headerRow != 0) && (CPLBOM.startRow != 0)) {
            Core.showMessage("errorMsg", true, CPLBOM.captions.StartRowGraterHeaderRow, CPLBOM.errorMSGInterval);
            $('.pageloader,.loaderBG').addClass('hide');
            return false;
        }
        if ((CPLBOM.startRow > CPLBOM.endRow) && (CPLBOM.startRow != 0) && (CPLBOM.endRow != 0)) {
            Core.showMessage("errorMsg", true, CPLBOM.captions.EndRowGraterStartRow, CPLBOM.errorMSGInterval);
            return false;
        }
        return true;
    },
    indicateSideColumn: function () {
        if (CPLBOM.currentStep == CPLBOM.steps.mapColumns) {
            console.log(CPLBOM.mappedColumnName)
            var isSideSeleRemain = false;
            $('.layerOp.distinctOption').find('.option').each(function (index) {
                var layerType = $.trim($(this).find('.mapValue select option:selected').val());
                if (isSideSeleRemain == false && layerType == "") {
                    isSideSeleRemain = true;
                    return false;
                }
            });
            if (CPLBOM.mappedColumnName) {
                if (CPLBOM.mappedColumnName.side) {
                    //$('.side_th').show();
                    if (isSideSeleRemain) {
                        $('.side_th').removeAttr('src').attr('height', '14').attr('src', '/shop/images/pencil_red.png');
                    }
                    else {
                        $('.side_th').removeAttr('src').attr('height', '14').attr('src', '/shop/images/editCell.png');
                    }
                }
            }
        }
    },
    headerRowAction: function (e) {
        var row = parseInt($(".headerRow").val()) || 0;
        if (CPLBOM.headerRow != row) {
            CPLBOM.headerRow = parseInt($(".headerRow").val()) || 0;
            CPLBOM.startRow = parseInt($(".startRow").val()) || 0;
            CPLBOM.loadTable();
        }
        CPLBOM.validateInputData();
        e.preventDefault();
    },
    startRowAction: function (e) {
        $('.pageloader,.loaderBG').removeClass('hide');
        CPLBOM.startRow = parseInt($('.startRow').val()) || 0;
        CPLBOM.selectedRows();
        CPLBOM.validateInputData();
        e.preventDefault();
    },
    autoMapUnit: function (selectedVal, selectedCol) {
        var iscenter_y_Mil = 0;
        var iscenter_y_Inch = 0;
        var iscenter_y_MM = 0;
        var iscenter_x_Mil = 0;
        var iscenter_x_Inch = 0;
        var iscenter_x_MM = 0;
        if (selectedVal == 'comp_y') {
            $('#bomTable tbody').find('tr.selectedRow  td.' + selectedCol + '').each(function (index) {
                var center_y_value = $.trim($(this).text()).toLowerCase();
                if (center_y_value.indexOf("mile") != -1 || center_y_value.indexOf("mil") != -1) {
                    iscenter_y_Mil++;
                }
                else if (center_y_value.indexOf("inches") != -1 || center_y_value.indexOf("inch") != -1 || center_y_value.indexOf("in") != -1) {
                    iscenter_y_Inch++;
                }
                else {
                    iscenter_y_MM++;
                }
            });
            if (iscenter_y_Inch > 0 && iscenter_y_Mil == 0 && iscenter_y_MM == 0) {
                $('.comp_y').val("Inch");
            }
            else if (iscenter_y_Mil > 0 && iscenter_y_Inch == 0 && iscenter_y_MM == 0) {
                $('.comp_y').val("Mil");
            }
            else {
                $('.comp_y').val("MM");
            }
        }

        if (selectedVal == 'comp_x') {
            $('#bomTable tbody').find('tr.selectedRow  td.' + selectedCol + '').each(function (index) {
                var center_x_value = $.trim($(this).text()).toLowerCase();
                if (center_x_value.indexOf("mile") != -1 || center_x_value.indexOf("mil") != -1) {
                    iscenter_x_Mil++;
                }
                else if (center_x_value.indexOf("inches") != -1 || center_x_value.indexOf("inch") != -1 || center_x_value.indexOf("in") != -1) {
                    iscenter_x_Inch++;
                }
                else {
                    iscenter_x_MM++;
                }
            });
            if (iscenter_x_Inch > 0 && iscenter_x_Mil == 0 && iscenter_x_MM == 0) {
                $('.comp_x').val("Inch");
            }
            else if (iscenter_x_Mil > 0 && iscenter_x_Inch == 0 && iscenter_x_MM == 0) {
                $('.comp_x').val("Mil");
            }
            else {
                $('.comp_x').val("MM");
            }
        }
    }
}
$(document).ready(function () {
    CPLBOM.init();
});
