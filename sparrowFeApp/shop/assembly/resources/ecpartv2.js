﻿Vue.component('partlist', {
    template: "#partlist-template",
    props: ['parts'],
    mounted: function () {
    },
    methods: {
    }
});
var app = new Vue({
    el: '#app',
    sessionId: "",
    data: {
        parts: [],
        alternativeParts: [],
        components: [],
        searchPart: null,
        showLoading: false,
        totalComponentsPrice: 0,
        totalAssemblyPrice: 0,
        boardQuantity: 0,
        currencySymbol: "&euro;",
        currencyfactor: 1,
        partWithprice: 0,
        partWithoutPrice: 0,
        reviewMode: false,
        priceLoading: false,
        mode: "update",
        waitMessage: "Loading...",
    },
    mounted: function () {
        ES6Promise.polyfill();
        var sessionId = this.getParameterByName("sessionid");
        this.sessionId = sessionId;
        this.filename = $("#ctl00_mainContent_spnlistName").text();
        this.events();
        this.getPartList(); 
        this.bindBreadcrumb();
        this.boardQuantity = $("#ctl00_mainContent_lblbomqty").text();
        $("#lbldownloadBOM").text(this.filename + ".CSV");
    },
    methods: {
        events: function () {
           
            var toggle = document.getElementById('container');
            var toggleContainer = document.getElementById('toggle-container');
            var toggleNumber;

            toggle.addEventListener('click', function () {
                toggleNumber = !toggleNumber;
                if (toggleNumber) {
                    toggleContainer.style.clipPath = 'inset(0 50% 0 0)';
                    toggleContainer.style.backgroundColor = '2a9f00';
                } else {

                    toggleContainer.style.clipPath = 'inset(0 0 0 50%)';
                    toggleContainer.style.backgroundColor = '#2a9f00';
                }
                console.log(toggleNumber)
            });

            $('body').click(function () {
                $('#list-detail').hide();
                $("#remove-list-detail").hide();
                $("#reset-review-detail").hide();
                //$(".popover").hide();
                //$(".popover").toggle();
                //$(".ondemand-list-detail").hide();
                $('.popover').popover('hide');
            });

            $('#chart').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {

                },
                tooltip: {
                    pointFormat: ''
                },
                yAxis: {
                    title: false,
                },
                xAxis: {
                    categories: ['EC stock', 'On-demand']
                },
                plotOptions: {
                    column: {
                        colors: ['#479347', '#e8791c'],
                        dataLabels: {
                            enabled: false,
                            fontWeight: 'bold',
                        },
                        startAngle: 0,
                        endAngle: 360,
                        center: ['50%', '50%'],
                        fontWeight: 'bold',
                        showInLegend: false
                    }

                },
                series: [{
                    showInLegend: false,
                    type: 'column',
                    name: 'Parts',
                    colorByPoint: true,
                    data: [{
                        name: 'EC stock',
                        y: 0

                    }, {
                        name: 'On-demand',
                        y: 0
                    }]
                }]
            });

            $('#chart-partStatus').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {

                },
                tooltip: {
                    pointFormat: ''
                },
                plotOptions: {
                    pie: {
                        colors: ['#479347', 'rgb(183, 59, 59)', 'rgb(116, 114, 189)', '#e8791c'],
                        dataLabels: {
                            enabled: false,
                            fontWeight: 'bold',
                        },
                        startAngle: 0,
                        endAngle: 360,
                        center: ['50%', '50%'],
                        fontWeight: 'bold',
                        showInLegend: false
                    }
                },
                series: [{
                    type: 'pie',
                    innerSize: '30%',
                    name: 'Parts',
                    colorByPoint: true,
                    data: [{
                        name: 'Matched eC stock parts',
                        y: 0
                    },
                    {
                        name: 'Unidentified Parts',
                        y: 0
                    },
                    {
                        name: "Matched parts without stocks",
                        y: 0
                    },
                    {
                        name: 'Suggested parts',
                        y: 0
                    }]
                }]
            });

            $(document).click(function (e) {
                var target = $(event.target);

                if (target.is(".type-li") || target.is(".cls-type-ul") || target.is("#lbltype") || target.is(".cls-type-caret") || target.is("#type-li"))  // if target div is not the one you want to exclude then add the class hidden
                {
                    $("#type-subli").show();
                }
                else {
                    $("#type-subli").hide();
                }

                if (target.is(".bottype-li") || target.is(".cls-bottype-ul") || target.is("#lblbottype") || target.is(".cls-bottype-caret") || target.is("#bottype-li"))  // if target div is not the one you want to exclude then add the class hidden
                {
                    $("#bottype-subli").show();
                }
                else {
                    $("#bottype-subli").hide();
                }
                if (target.is(".supply-li") || target.is(".cls-supply-ul") || target.is("#lblsupply") || target.is(".cls-supply-caret") || target.is("#supply-li"))  // if target div is not the one you want to exclude then add the class hidden
                {
                    $("#supply-subli").show();
                }
                else {
                    $("#supply-subli").hide();
                }
                if (target.is(".botsupply-li") || target.is(".cls-botsupply-ul") || target.is("#lblbotsupply") || target.is(".cls-botsupply-caret") || target.is("#botsupply-li"))  // if target div is not the one you want to exclude then add the class hidden
                {
                    $("#botsupply-subli").show();
                }
                else {
                    $("#botsupply-subli").hide();
                }

            }); 
        }, 
        bindBreadcrumb: function () {
            var url = '/shop/pcb/pcbservice.ashx?isBreadcrumb=true&sessionId=' + this.sessionId + '&bomstage=bomeditor';
            $.ajax({
                type: 'GET',
                url: url,
                dataType: "text",
                success: function (data) {
                    if (data == "bomeditor" || data == "addpcb" || data == "orderpreview") {
                        $("#bomeditor").attr("href", "/shop/assembly/bom.aspx?sessionid=" + app.sessionId);
                    }
                    if (data == "addpcb" || data == "orderpreview") {
                        $("#addpcb").attr("href", "/shop/assembly/ecpartspcb.aspx?bsessionid=" + app.sessionId);
                    }
                    if (data == "orderpreview") {
                        $("#orderpreview").attr("href", "/shop/pcb/board.aspx?sessionid=" + app.sessionId + '&fromecpart=true');
                    }
                },
                error: function (request, error) {
                    console.log(arguments);
                }
            });
        },
        saveListNamePopup: function (e) {
            var lst = $("#ctl00_mainContent_spnlistName").text();
            $("#txtlistName").val(lst);
            $("#SaveListNameModal").modal('show');
            e.preventDefault()
            e.stopPropagation();
        },
        saveListName: function (e) {
            if ($("#txtlistName").val() == "") {
                $("#lstErrorMsg").show();
                setTimeout(function () { $("#lstErrorMsg").hide(); }, 2000);
            }
            else {
                var listname = $("#txtlistName").val();
                axios.post('/shop/assembly/bom.aspx/savelistname', {
                    sessionId: this.sessionId,
                    listname: listname
                })
                .then(function (response) {
                    $("#ctl00_mainContent_spnlistName").text(listname);
                    $("#ctl00_mainContent_spnlistName").attr("title", listname);
                    $("#txtlistName").val("");
                    $("#SaveListNameModal").modal('hide');
                })
                .catch(function (error) {
                    console.log(error);
                });
            }
            e.preventDefault()
            e.stopPropagation();
        },
        gePartsFromComponents: function (mpn) {
            app.parts = [];
            if (Array.isArray(app.components.AssemblyData.BOM.Line)) {
                for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
                    if (app.components.AssemblyData.BOM.Line[i] != undefined && app.components.AssemblyData.BOM.Line[i] != "") {
                        var uniqid = app.getUniqid();
                        app.components.AssemblyData.BOM.Line[i].uniqid = uniqid;
                        var part = {};
                        part.uniqid = uniqid;
                        part.supply = app.getSupplyByFromvalue(app.components.AssemblyData.BOM.Line[i]._supply);
                        part.refdes = app.components.AssemblyData.BOM.Line[i]._refdes;
                        part.qty = app.components.AssemblyData.BOM.Line[i].Comp.length ? app.components.AssemblyData.BOM.Line[i].Comp.length : 1;
                        part.appr = 2;
                        part.review = app.components.AssemblyData.BOM.Line[i]._review;
                        part.price = 0;
                        part.mpn = "";
                        part.isHistory = 0;
                        part.stock = 0;
                        if (app.components.AssemblyData.BOM.Line[i].History != undefined) {
                            part.isHistory = 1;
                        }
                        if (app.components.AssemblyData.BOM.Line[i].MPN != undefined) {
                            part.mpn = app.components.AssemblyData.BOM.Line[i].MPN._mpn;
                            var objPart = app.getPart(part.mpn);
                            if (objPart != undefined) {
                                part.appr = app.components.AssemblyData.BOM.Line[i]._appr; //1;
                                part.category = objPart._cat;
                                part.desc = objPart._desc;
                                part.mount_type = objPart._type;
                                part.imgurl = objPart._img;
                                part.datasheet_url = objPart._url;
                                part.gpn = objPart._gpn;
                                part.stock = objPart._stock ? objPart._stock : 1;
                                part.ft_eagle = objPart._ft_eagle;
                                part.ft_fpx = objPart._ft_fpx;
                                part.ft_altium = objPart._ft_altium;
                                part.ft_kicad = objPart._ft_kicad;
                            }
                            //if (app.mode == "view") {
                            var objPrices = app.getPriceFromFile(part.mpn);
                            if (objPrices != undefined) {
                                if (objPrices._manPrice >= 0) {
                                    part.price = Math.round(objPrices._manPrice * 100) / 100;
                                }
                                else {
                                    if (app.mode == "view") {
                                        part.price = Math.round(objPrices._price * 100) / 100;
                                    }
                                    else {
                                        if (typeof mpn === "undefined" || mpn == "" || mpn == part.mpn) {
                                            part.price = -2;
                                        }
                                        else if (mpn != part.mpn) {
                                            part.price = Math.round(objPrices._price * 100) / 100;
                                        }
                                    }
                                }
                            }
                            // }
                            //else {
                            //    part.price = -2;
                            //}
                        }
                        app.parts.push(part);
                    }
                }
            }
            else { 
                if (app.components.AssemblyData.BOM.Line != undefined && app.components.AssemblyData.BOM.Line != "") {
                    var uniqid = app.getUniqid();
                    app.components.AssemblyData.BOM.Line.uniqid = uniqid;
                    var part = {};
                    part.uniqid = uniqid;
                    part.supply = app.getSupplyByFromvalue(app.components.AssemblyData.BOM.Line._supply);
                    part.refdes = app.components.AssemblyData.BOM.Line._refdes;
                    part.qty = app.components.AssemblyData.BOM.Line.Comp.length ? app.components.AssemblyData.BOM.Line.Comp.length : 1;
                    part.appr = 2;
                    part.price = 0;
                    part.mpn = "";
                    part.isHistory = 0;
                    part.stock = 0;
                    part.review = app.components.AssemblyData.BOM.Line._review;
                    if (app.components.AssemblyData.BOM.Line.History != undefined) {
                        part.isHistory = 1;//when history exists 
                    }
                    if (app.components.AssemblyData.BOM.Line.MPN != undefined) {
                        part.mpn = app.components.AssemblyData.BOM.Line.MPN._mpn;
                        var objPart = app.getPart(part.mpn);
                        if (objPart != undefined) {
                            part.appr = app.components.AssemblyData.BOM.Line._appr; //1;
                            part.category = objPart._cat;
                            part.desc = objPart._desc;
                            part.mount_type = objPart._type;
                            part.imgurl = objPart._img;
                            part.datasheet_url = objPart._url;
                            part.gpn = objPart._gpn;
                            part.stock = objPart._stock ? objPart._stock : 1;
                            part.ft_eagle = objPart._ft_eagle;
                            part.ft_fpx = objPart._ft_fpx;
                            part.ft_altium = objPart._ft_altium;
                            part.ft_kicad = objPart._ft_kicad;
                        }
                        if (app.mode == "view") {
                            var objPrices = app.getPriceFromFile(part.mpn);
                            if (objPrices != undefined) {

                                if (objPrices._manPrice >= 0) {
                                    part.price = Math.round(objPrices._manPrice * 100) / 100;
                                }
                                else {
                                    part.price = Math.round(objPrices._price * 100) / 100;
                                }
                            }
                        }
                        else {
                            part.price = -2;
                        }
                    }
                    app.parts.push(part);
                }
            }
            if (app.parts.length > 0) {
                //don't sort while replace alternative part 
                //if (typeof mpn === "undefined" || mpn == "") {
                    app.parts = app.sortByKeyDesc(app.parts, "stock");
                //}
            }
        },
        replaceAlternativeData: function (part) {
            var isReplaced = false;
            var oldmpn = $('#sp_mpn_alt_head').text();
            if (oldmpn != part.mpn) {
                if (Array.isArray(app.components.AssemblyData.BOM.Line)) {
                    for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
                        if (app.components.AssemblyData.BOM.Line[i].MPN != undefined) {
                            if (app.components.AssemblyData.BOM.Line[i].MPN._mpn == oldmpn) {
                                app.components.AssemblyData.BOM.Line[i]._review = false;
                                isReplaced = true;
                                var olduniqid = app.components.AssemblyData.BOM.Line[i].uniqid;
                                //var uniqid = app.getUniqid();
                                part.uniqid = olduniqid;//uniqid;
                                app.ReplaceAlternativeCurrentList(part, olduniqid);
                                app.components.AssemblyData.BOM.Line[i].uniqid = olduniqid;//uniqid;
                                app.components.AssemblyData.BOM.Line[i].MPN._mpn = part.mpn;
                                app.maintainPart(part, oldmpn, true);
                                app.maintainPrice(part, oldmpn, true);
                                var d = new Date();
                                var NewHistory = {};
                                NewHistory._mpn = oldmpn;
                                NewHistory._id = d.getTime();
                                if (app.components.AssemblyData.BOM.Line[i].History) {
                                    if (Array.isArray(app.components.AssemblyData.BOM.Line[i].History)) {
                                        app.components.AssemblyData.BOM.Line[i].History.push(NewHistory);
                                    }
                                    else {
                                        var History = [];
                                        History.push(app.components.AssemblyData.BOM.Line[i].History);
                                        History.push(NewHistory);
                                        app.components.AssemblyData.BOM.Line[i].History = History;
                                    }
                                }
                                else {
                                    app.components.AssemblyData.BOM.Line[i].History = NewHistory;
                                }
                                break;
                            }
                        }
                    }
                }
                else {
                    //var uniqid = app.getUniqid();
                    var olduniqid = app.components.AssemblyData.BOM.Line.uniqid;
                    part.uniqid = olduniqid;//uniqid;
                    app.ReplaceAlternativeCurrentList(part,olduniqid);
                    app.components.AssemblyData.BOM.Line.uniqid = olduniqid;//uniqid;
                    app.components.AssemblyData.BOM.Line.MPN._mpn = part.mpn;
                    app.components.AssemblyData.BOM.Line._review = false;
                    app.maintainPart(part, oldmpn, true);
                    app.maintainPrice(part, oldmpn, true);
                    isReplaced = true;
                }
            }
            if (isReplaced) {
                //app.gePartsFromComponents(part.mpn); 
                app.saveBOM();
                app.bindChart();
            }
            $(".chkmpn").prop('checked', false);
            $(".chkmpn").removeAttr('checked');
            $("#popupunidentified").modal("hide");
        },
        ReplaceAlternativeCurrentList: function (part, uniqid) {
            for (var i = 0; i < app.parts.length; i++) {
                if (app.parts[i].uniqid == uniqid) { 
                    app.parts[i].appr="1";
                    app.parts[i].category=part.cat;
                    app.parts[i].datasheet_url=part.url;
                    app.parts[i].desc=part.desc;
                    app.parts[i].ft_altium=part.ft_altium;
                    app.parts[i].ft_eagle=part.ft_eagle;
                    app.parts[i].ft_fpx=part.ft_fpx;
                    app.parts[i].ft_kicad=part.ft_kicad;
                    app.parts[i].gpn=part.gpn;
                    app.parts[i].imgurl=part.img;
                    app.parts[i].isHistory="1";
                    app.parts[i].mount_type=part.type;
                    app.parts[i].mpn=part.mpn;
                    app.parts[i].price = -2;
                    //app.parts[i].qty=part.
                    //app.parts[i].refdes=part.
                    //app.parts[i].review="0";
                    app.parts[i].stock=part.stock;
                    //app.parts[i].supply=part.  
                }
            }
        },
        maintainPart: function (part, oldmpn, isReplace) {
            var newParts = {};
            newParts._appr = "1";
            newParts._cat = part.cat;
            newParts._category_id = part.category_id;
            newParts._desc = part.desc;
            newParts._ft_altium = part.ft_altium;
            newParts._ft_eagle = part.ft_eagle;
            newParts._ft_fpx = part.ft_fpx;
            newParts._ft_kicad = part.ft_kicad;
            newParts._gpn = part.gpn;
            newParts._img = part.img;
            newParts._ipc = part.ipc;
            newParts._is_polarized = part.is_polarized;
            newParts._manuf = part.manuf;
            newParts._marking = part.marking;
            newParts._mpn = part.mpn;
            newParts._partId = part.partId;
            newParts._pol = part.pol;
            newParts._remark = part.remark;
            newParts._rohs = part.rohs;
            newParts._step_3d = part.step_3d;
            newParts._stock = part.stock;
            newParts._type = part.type;
            newParts._url = part.url;
            newParts._ver = part.ver;
            newParts._wrl_3d = part.wrl_3d;

            if (app.components.AssemblyData.Parts != "") {
                if (Array.isArray(app.components.AssemblyData.Parts.Part)) {
                    var isPartAdded = false;
                    for (var i = 0; i < app.components.AssemblyData.Parts.Part.length > 0; i++) {
                        if (app.components.AssemblyData.Parts.Part[i] != undefined) {
                            if (app.components.AssemblyData.Parts.Part[i]._mpn == oldmpn) {
                                isPartAdded = true;
                                app.components.AssemblyData.Parts.Part[i]._appr = "1";
                                app.components.AssemblyData.Parts.Part[i]._cat = part.cat;
                                app.components.AssemblyData.Parts.Part[i]._category_id = part.category_id;
                                app.components.AssemblyData.Parts.Part[i]._desc = part.desc;
                                app.components.AssemblyData.Parts.Part[i]._ft_altium = part.ft_altium;
                                app.components.AssemblyData.Parts.Part[i]._ft_eagle = part.ft_eagle;
                                app.components.AssemblyData.Parts.Part[i]._ft_fpx = part.ft_fpx;
                                app.components.AssemblyData.Parts.Part[i]._ft_kicad = part.ft_kicad;
                                app.components.AssemblyData.Parts.Part[i]._gpn = part.gpn;
                                app.components.AssemblyData.Parts.Part[i]._img = part.img;
                                app.components.AssemblyData.Parts.Part[i]._ipc = part.ipc;
                                app.components.AssemblyData.Parts.Part[i]._is_polarized = part.is_polarized;
                                app.components.AssemblyData.Parts.Part[i]._manuf = part.manuf;
                                app.components.AssemblyData.Parts.Part[i]._marking = part.marking;
                                app.components.AssemblyData.Parts.Part[i]._mpn = part.mpn;
                                app.components.AssemblyData.Parts.Part[i]._partId = part.partId;
                                app.components.AssemblyData.Parts.Part[i]._pol = part.pol;
                                app.components.AssemblyData.Parts.Part[i]._remark = part.remark;
                                app.components.AssemblyData.Parts.Part[i]._rohs = part.rohs;
                                app.components.AssemblyData.Parts.Part[i]._step_3d = part.step_3d;
                                app.components.AssemblyData.Parts.Part[i]._stock = part.stock;
                                app.components.AssemblyData.Parts.Part[i]._type = part.type;
                                app.components.AssemblyData.Parts.Part[i]._url = part.url;
                                app.components.AssemblyData.Parts.Part[i]._ver = part.ver;
                                app.components.AssemblyData.Parts.Part[i]._wrl_3d = part.wrl_3d;
                                break;
                            }
                        }
                    }
                    if (!isPartAdded) {
                        app.components.AssemblyData.Parts.Part.push(newParts);
                    }
                }
                else {
                    if (isReplace) {
                        if (app.components.AssemblyData.Parts.Part._mpn == oldmpn) {
                            isPartAdded = true;
                            app.components.AssemblyData.Parts.Part._appr = "1";
                            app.components.AssemblyData.Parts.Part._cat = part.cat;
                            app.components.AssemblyData.Parts.Part._category_id = part.category_id;
                            app.components.AssemblyData.Parts.Part._desc = part.desc;
                            app.components.AssemblyData.Parts.Part._ft_altium = part.ft_altium;
                            app.components.AssemblyData.Parts.Part._ft_eagle = part.ft_eagle;
                            app.components.AssemblyData.Parts.Part._ft_fpx = part.ft_fpx;
                            app.components.AssemblyData.Parts.Part._ft_kicad = part.ft_kicad;
                            app.components.AssemblyData.Parts.Part._gpn = part.gpn;
                            app.components.AssemblyData.Parts.Part._img = part.img;
                            app.components.AssemblyData.Parts.Part._ipc = part.ipc;
                            app.components.AssemblyData.Parts.Part._is_polarized = part.is_polarized;
                            app.components.AssemblyData.Parts.Part._manuf = part.manuf;
                            app.components.AssemblyData.Parts.Part._marking = part.marking;
                            app.components.AssemblyData.Parts.Part._mpn = part.mpn;
                            app.components.AssemblyData.Parts.Part._partId = part.partId;
                            app.components.AssemblyData.Parts.Part._pol = part.pol;
                            app.components.AssemblyData.Parts.Part._remark = part.remark;
                            app.components.AssemblyData.Parts.Part._rohs = part.rohs;
                            app.components.AssemblyData.Parts.Part._step_3d = part.step_3d;
                            app.components.AssemblyData.Parts.Part._stock = part.stock;
                            app.components.AssemblyData.Parts.Part._type = part.type;
                            app.components.AssemblyData.Parts.Part._url = part.url;
                            app.components.AssemblyData.Parts.Part._ver = part.ver;
                            app.components.AssemblyData.Parts.Part._wrl_3d = part.wrl_3d;
                        }
                        else {
                            var Parts = new Object();
                            Parts.Part = [];
                            Parts.Part.push(app.components.AssemblyData.Parts.Part);
                            Parts.Part.push(newParts);
                            app.components.AssemblyData.Parts = Parts;
                        }
                    }
                    else {
                        var Parts = new Object();
                        Parts.Part = [];
                        Parts.Part.push(app.components.AssemblyData.Parts.Part);
                        Parts.Part.push(newParts);
                        app.components.AssemblyData.Parts = Parts;
                    }
                }
            }
            else {
                var Parts = new Object();
                Parts.Part = newParts;
                app.components.AssemblyData.Parts = Parts;
            }
        },
        maintainPrice: function (part, oldmpn, isReplace) {

            var newPrice = {};
            newPrice._manPrice = "-1";
            newPrice._manPriceQty = "-1";
            newPrice._manProdQty = "-1";
            newPrice._manProdRefQty = "-1";
            newPrice._manQty = "-1";
            newPrice._manURL = "";
            newPrice._moq = "0";
            newPrice._mpn = part.mpn;
            //newPrice._ordQty = "1";//----* set previous value
            //newPrice._pcbQty = "1";//----*set previous value
            newPrice._price = "-2";
            //newPrice._prodQty = "1";//----*set previous value
            //newPrice._purchQty = "1"; //----*set previous value
            newPrice._sku = "";
            newPrice._stock = "0";
            newPrice._suppl = "";
            newPrice._val = "";

            if (app.components.AssemblyData.Prices != "") {
                if (Array.isArray(app.components.AssemblyData.Prices.Price)) {
                    var isPriceAdded = false;
                    for (var i = 0; i < app.components.AssemblyData.Prices.Price.length > 0; i++) {
                        if (app.components.AssemblyData.Prices.Price[i]._mpn != undefined) {
                            if (app.components.AssemblyData.Prices.Price[i]._mpn == oldmpn) {
                                isPriceAdded = true;
                                app.components.AssemblyData.Prices.Price[i]._manPrice = "-1";
                                app.components.AssemblyData.Prices.Price[i]._manPriceQty = "-1";
                                app.components.AssemblyData.Prices.Price[i]._manProdQty = "-1";
                                app.components.AssemblyData.Prices.Price[i]._manProdRefQty = "-1";
                                app.components.AssemblyData.Prices.Price[i]._manQty = "-1";
                                app.components.AssemblyData.Prices.Price[i]._manURL = "";
                                app.components.AssemblyData.Prices.Price[i]._moq = "0";
                                app.components.AssemblyData.Prices.Price[i]._mpn = part.mpn;
                                //app.components.AssemblyData.Prices.Price[i]._ordQty = "1";//----* set previous value
                                //app.components.AssemblyData.Prices.Price[i]._pcbQty = "1";//----*set previous value
                                app.components.AssemblyData.Prices.Price[i]._price = "-2";
                                //app.components.AssemblyData.Prices.Price[i]._prodQty = "1";//----*set previous value
                                //app.components.AssemblyData.Prices.Price[i]._purchQty = "1"; //----*set previous value
                                app.components.AssemblyData.Prices.Price[i]._sku = "";
                                app.components.AssemblyData.Prices.Price[i]._stock = "0";
                                app.components.AssemblyData.Prices.Price[i]._suppl = "";
                                app.components.AssemblyData.Prices.Price[i]._val = "";
                                break;
                            }
                        }
                    }
                    if (!isPriceAdded) {
                        app.components.AssemblyData.Prices.Price.push(newPrice);
                    }
                }
                else {
                    if (isReplace) {
                        if (app.components.AssemblyData.Prices.Price._mpn == oldmpn) {
                            app.components.AssemblyData.Prices.Price._manPrice = "-1";
                            app.components.AssemblyData.Prices.Price._manPriceQty = "-1";
                            app.components.AssemblyData.Prices.Price._manProdQty = "-1";
                            app.components.AssemblyData.Prices.Price._manProdRefQty = "-1";
                            app.components.AssemblyData.Prices.Price._manQty = "-1";
                            app.components.AssemblyData.Prices.Price._manURL = "";
                            app.components.AssemblyData.Prices.Price._moq = "0";
                            app.components.AssemblyData.Prices.Price._mpn = part.mpn;
                            //app.components.AssemblyData.Prices.Price._ordQty = "1";//----* set previous value
                            //app.components.AssemblyData.Prices.Price._pcbQty = "1";//----*set previous value
                            app.components.AssemblyData.Prices.Price._price = "-2";
                            //app.components.AssemblyData.Prices.Price._prodQty = "1";//----*set previous value
                            //app.components.AssemblyData.Prices.Price._purchQty = "1"; //----*set previous value
                            app.components.AssemblyData.Prices.Price._sku = "";
                            app.components.AssemblyData.Prices.Price._stock = "0";
                            app.components.AssemblyData.Prices.Price._suppl = "";
                            app.components.AssemblyData.Prices.Price._val = "";
                        }
                        else {
                            var Prices = new Object();
                            Prices.Price = [];
                            Prices.Price.push(app.components.AssemblyData.Prices.Price);
                            Prices.Price.push(newPrice);
                            app.components.AssemblyData.Prices = Prices;
                        }
                    }
                    else {
                        var Prices = new Object();
                        Prices.Price = [];
                        Prices.Price.push(app.components.AssemblyData.Prices.Price);
                        Prices.Price.push(newPrice);
                        app.components.AssemblyData.Prices = Prices;
                    }
                }
            }
            else {
                var Prices = new Object();
                Prices.Price = newPrice;
                app.components.AssemblyData.Prices = Prices;
            }
        },
        getSupplyByFromvalue: function (supplyValue) {
            if (supplyValue == "1") {
                return "Customer"
            }
            if (supplyValue == "2") {
                return "Not placed"
            }
            else {
                return "Assembler"
            }
        },
        getPart: function (mpn) {
            var objPart;
            if (Array.isArray(app.components.AssemblyData.Parts.Part)) {
                $.each(app.components.AssemblyData.Parts.Part, function (key, part) {
                    if (part._mpn.toUpperCase() === mpn.toUpperCase()) {
                        objPart = part;
                        return false;
                    }
                });
            }
            else {
                //objPart = app.components.AssemblyData.Parts.Part;
                if (app.components.AssemblyData.Parts != "" && app.components.AssemblyData.Parts.Part._mpn.toUpperCase() == mpn.toUpperCase()) {
                    return app.components.AssemblyData.Parts.Part;
                }
            }
            return objPart;
        },
        getPriceFromFile: function (mpn) {
            var objPrice;
            if (app.components.AssemblyData.Prices != "") {
                if (Array.isArray(app.components.AssemblyData.Prices.Price)) {
                    $.each(app.components.AssemblyData.Prices.Price, function (key, price) {
                        if (price._mpn.toUpperCase() === mpn.toUpperCase()) {
                            objPrice = price;
                            return false;
                        }
                    });
                }
                else {
                    if (app.components.AssemblyData.Prices.Price != "" && app.components.AssemblyData.Prices.Price._mpn.toUpperCase() == mpn.toUpperCase()) {
                        return app.components.AssemblyData.Prices.Price;
                    }
                }
            }
            return objPrice;
        },
        nextStep: function () {
            var isNotApproved = false;
            for (var i = 0; i < app.parts.length; i++) {
                if (app.parts[i].appr == "0") {
                    isNotApproved = true;
                }
            }
            if (isNotApproved) {
                $("#msgApprovedModal").modal("show");
                return;
            }
            app.nextStage();
        },
        removeNotApproved: function () {
            if (Array.isArray(app.components.AssemblyData.BOM.Line)) {
                for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
                    if (app.components.AssemblyData.BOM.Line[i]._appr == 0) {
                        delete app.components.AssemblyData.BOM.Line[i].MPN;
                    }
                }
            }
            else {
                delete app.components.AssemblyData.BOM.Line.MPN;
            }
            app.nextStage();
        },
        nextStage: function () {
            app.saveBOM();
             
            var totalPrice = app.totalComponentsPrice * app.currencyfactor;
            var isComponentIncompletePrice = false;
            if (app.partWithoutPrice > 0) {
                isComponentIncompletePrice = true;
            }
            var ComponentType = app.partTypeCount();

            axios.post('/shop/assembly/bom.aspx/componentprice', {
                sessionId: app.sessionId,
                price: totalPrice,
                isComponentIncomplete: isComponentIncompletePrice,
                ComponentTypeCount: ComponentType
            })
            .then(function (response) {
                window.location.href = '/shop/assembly/ecpartspcb.aspx?bsessionid=' + app.sessionId + '&boardqty=' + app.boardQuantity;
            })
            .catch(function (error) {
                console.log(error);
            });
        },
        updatebom: function () {
            app.saveBOM();
        },
        hideTypeList: function (type) {
            $("#lbltype").text(type);
            $("#type-subli").hide();
        },
        hideBottomTypeList: function (type) {
            $("#lblbottype").text(type);
            $("#bottype-subli").hide();
        },
        hideSupplyList: function (supply) {
            $("#lblsupply").text(supply);
            $("#supply-subli").hide();
        },
        hideBottomSupplyList: function (supply) {
            $("#lblbotsupply").text(supply);
            $("#botsupply-subli").hide();
        },
        addNewPart: function () {
            var mpn = $("#txtAddNewparts").val().trim();
            var qty = $("#txtqty").val();
            var refdes = $('#txtrefdesc').val();
            var mount_type = $('#lblbottype').text();
            var supply = $('#lblbotsupply').text();

            var isValidated = true;

            if (mpn == "") {
                isValidated = false;
                $("#txtAddNewparts").addClass("new-part-error");
                $("#addnewparterror").show();
                setTimeout(function () { $("#txtAddNewparts").removeClass("new-part-error"); $("#addnewparterror").hide(); }, 2000);
            }
            if (qty == "") {
                isValidated = false;
                $("#txtqty").addClass("new-part-error");
                $("#addnewparterror").show();
                setTimeout(function () { $("#txtqty").removeClass("new-part-error"); $("#addnewparterror").hide(); }, 2000);
            }
            if (refdes == "") {
                isValidated = false;
                $("#txtrefdesc").addClass("new-part-error");
                $("#addnewparterror").show();
                setTimeout(function () { $("#txtrefdesc").removeClass("new-part-error"); $("#addnewparterror").hide(); }, 2000);
            }

            if (!isValidated) {
                return;
            }

            if (qty == "") { qty = "1"; }
            if (mpn != "") {
                var searchData = [];
                var searchNode = {}
                searchNode["index"] = 0
                searchNode["mpn"] = mpn;
                searchNode["refdes"] = refdes;
                searchNode["qty"] = qty
                var spec = BOMUtility.sanatizeKeyword(mpn, 1);
                searchNode["specs"] = spec.specs;
                searchData.push(searchNode);

                var xmlData = [];
                var xmlNode = {}
                xmlNode["index"] = 0
                xmlNode["mpn"] = mpn;
                xmlNode["refdes"] = refdes;
                xmlNode["qty"] = qty
                xmlData.push(xmlNode);
                //var postsearchData = JSON.stringify(searchData);
                var postData = {
                    sessionId: app.sessionId,
                    searchData: JSON.stringify(searchData),
                    xmlData: JSON.stringify(xmlData)
                }
                axios.post('/shop/assembly/ecpartbomimport.aspx/AddtoBoM', postData)
                .then(function (response) {
                    var ismatch = false;

                    app.getPartList();
                    setTimeout(function () {
                        if ($("input[mpn='" + mpn + "']").length > 1) {
                            //when checkbox multiple with same mpn then check for refdes 
                            $("input[mpn='" + mpn + "'][refdes='" + refdes + "']").closest('tr').addClass('highlight-add');//----for animation
                            setTimeout(function () { $("input[mpn='" + mpn + "'][refdes='" + refdes + "']").closest('tr').removeClass('highlight-add'); }, 2000);//----for animation
                        }
                        else {
                            $("input[mpn='" + mpn + "']").closest('tr').addClass('highlight-add');//----for animation
                            setTimeout(function () { $("input[mpn='" + mpn + "']").closest('tr').removeClass('highlight-add'); }, 2000);//----for animation
                        }
                    }, 100);

                    $("#txtAddNewparts").val("");
                    $("#txtrefdesc").val("");
                    $("#txtqty").val("1");
                })
                .catch(function (error) {
                    console.log(error);
                });
            }
        },
        showBOMData: function (part) {
            $('#idBOMdata').modal('show');
            $('#bomMpn').text(part.mpn);
            $('#bom_mpn').text("-");
            $('#bom_spn').text("-");
            $('#bom_desc').text("-");
            $('#bom_pack').text("-");
            $('#bom_url').text("-");

            for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
                if (app.components.AssemblyData.BOM.Line[i].MPN != undefined) {
                    if (app.components.AssemblyData.BOM.Line[i].MPN._mpn == part.mpn) {

                        if (app.components.AssemblyData.BOM.Line[i]._mpn) {
                            $('#bom_mpn').text(app.components.AssemblyData.BOM.Line[i]._mpn)
                        }
                        if (app.components.AssemblyData.BOM.Line[i]._spn_1) {
                            $('#bom_spn').text(app.components.AssemblyData.BOM.Line[i]._spn_1)
                        }
                        if (app.components.AssemblyData.BOM.Line[i]._desc) {
                            $('#bom_desc').text(app.components.AssemblyData.BOM.Line[i]._desc)
                        }
                        if (app.components.AssemblyData.BOM.Line[i]._pack) {
                            $('#bom_pack').text(app.components.AssemblyData.BOM.Line[i]._pack)
                        }
                        if (app.components.AssemblyData.BOM.Line[i]._url) {
                            $('#bom_url').text(app.components.AssemblyData.BOM.Line[i]._url)
                        }
                        break;
                    }
                }
            }
        },
        reviewpoup: function () { 
            var src = $('#imgreview').attr("src");
            $('#imgreview').attr("src", "");
            $('#imgreview').attr("src", src);
            $("#reviewModal").modal("show");
        },
        findAlternatives: function (part) { 
            $('#popupunidentified').modal('show');
            var desc = "";
            app.alternativeParts = [];
            var searchData = [];
            var searchNode = {}
            if (part != undefined) { 
                $('#sp_mpn_alt_head').text(part.mpn);
                $('#alt_mpn').text(part.mpn);
                //$('#alt_refdesc').text(part.refdes); 
                $('#alt_desc').text(part.desc);
                $('#alt_category').text(part.category);
                if (typeof part.desc != "undefined" && part.desc != "") {
                    desc = part.desc;
                }
                $("#txtaltsearch").val('');
                app.searchPart = part;
                searchNode["cat_name"] = part.category; 
                if (part.mpn == "") {
                    if (Array.isArray(app.components.AssemblyData.BOM.Line)) {
                        for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
                            if (app.components.AssemblyData.BOM.Line[i] != undefined && app.components.AssemblyData.BOM.Line[i] != "") {
                                if (app.components.AssemblyData.BOM.Line[i].uniqid == part.uniqid) { 
                                    if (app.components.AssemblyData.BOM.Line[i]._spn_1) {
                                        $('#sp_mpn_alt_head').text(app.components.AssemblyData.BOM.Line[i]._spn_1);
                                        $('#alt_mpn').text(app.components.AssemblyData.BOM.Line[i]._spn_1);
                                    }
                                    if (app.components.AssemblyData.BOM.Line[i]._mpn) {
                                        $('#sp_mpn_alt_head').text(app.components.AssemblyData.BOM.Line[i]._mpn);
                                        $('#alt_mpn').text(app.components.AssemblyData.BOM.Line[i]._mpn);
                                    } 
                                    if (app.components.AssemblyData.BOM.Line[i]._desc) { 
                                        $('#alt_desc').text(app.components.AssemblyData.BOM.Line[i]._desc);
                                    }
                                    if (app.components.AssemblyData.BOM.Line[i]._cat) {
                                        $('#alt_category').text(app.components.AssemblyData.BOM.Line[i]._cat)
                                    } 
                                    break; 
                                } 
                            }
                        }
                    } 
                } 
            }
            else {
                desc = $("#txtaltsearch").val().trim();
            }
            searchNode["descr"] = desc;
            searchNode["index"] = 0;
            searchNode["start"] = 0;
            searchNode["length"] = 50;
            var spec = BOMUtility.sanatizeKeyword(desc, 1);
            searchNode["specs"] = spec.specs;
            searchData.push(searchNode);
            var postsearchData = JSON.stringify(searchData);
            app.alternativeParts = [];
            axios.post('/shop/assembly/searchpart.aspx?opencomponentsearch=true &isMulti=1', postsearchData)
          .then(function (data) {
              for (var i = 0; i < data.data.Parts.length; i++) {
                  app.alternativeParts.push(data.data.Parts[i]);
              }
              if (app.alternativeParts.length > 0) {
                  app.alternativeParts = app.sortByKeyDesc(app.alternativeParts, "stock");
                  $("#alternativeParts th").removeClass("sorting_desc");
                  $("#alternativeParts th").removeClass("sorting_asc");
                  $(".availablity").addClass("sorting_asc");
              }
          })
          .catch(function (error) {
              console.log(error);
          });
        },
        sortByKeyDesc: function (array, key) {
            return array.sort(function (a, b) {
                var x = a[key]; var y = b[key]; 
                return ((x > y) ? -1 : ((x < y) ? 1 : 0));
            });
        },
        sortByKeyAsc: function (array, key) {
            return array.sort(function (a, b) {
                var x = a[key]; var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        },
        showQtyBox: function () {
            var qty = $("#ctl00_mainContent_lblbomqty").text();
            $("#txtBoardQty").val(qty);
            $(".edit-brd-qty").hide();
            $(".BoardQty").show();
            $(".boardqty-save").show();
        },
        hideQtyBox: function () {
            $(".edit-brd-qty").show();
            $(".BoardQty").hide();
            $(".boardqty-save").hide();
        },
        getParameterByName: function (key) {
            return decodeURIComponent(window.location.href.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
        },
        getUTCDate: function () {
            var now = new Date();
            var nowUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
            return nowUTC;
        },
        updatePartModal: function (part, type) { 
            $('#sp_mpn_update').text(part.mpn);
            $('#sp_uniqid_update_head').text(part.uniqid);
            $('#sp_mpn_update_head').text(part.mpn);
            $('#txtupdateqty').val(part.qty);
            if (part.mount_type != "") {
                $('#lbltype').text(part.mount_type.toUpperCase());
            }
            if (part.supply != "") {
                $('#lblsupply').text(part.supply);

            }
            $('textarea#txtupdaterefdesc').val(part.refdes);
            $('#imgupdate').attr('src', part.imgurl);


            if (type == "0") {
                $("#txtupdaterefdesc").addClass("part-hl");
                setTimeout(function () { $("#txtupdaterefdesc").removeClass("part-hl"); }, 2000);
            }
            else if (type == "1") {
                $(".cls-type-ul").addClass("part-hl");
                setTimeout(function () { $(".cls-type-ul").removeClass("part-hl"); }, 2000);
            }
            else if (type == "2") {
                $(".cls-supply-ul").addClass("part-hl");
                setTimeout(function () { $(".cls-supply-ul").removeClass("part-hl"); }, 2000);
            }
            else if (type == "3") {
                $("#txtupdateqty").addClass("part-hl");
                setTimeout(function () { $("#txtupdateqty").removeClass("part-hl"); }, 2000);
            }


            $('textarea#txtupdaterefdesc').val(part.refdes);
            $('#imgupdate').attr('src', part.imgurl);

            $('#updatedata').modal('show');
        },
        UpdateMPNData: function () { 
            var uniqid = $('#sp_uniqid_update_head').text();
            var mpn = $('#sp_mpn_update').text();
            var qty = $('#txtupdateqty').val();
            var mount_type = $('#lbltype').text();
            var supply = $('#lblsupply').text();
            var refdes = $('#txtupdaterefdesc').val();
            var supplyBY = "0";
            if (supply == "Customer") {
                supplyBY = "1";
            }
            else if (supply == "Customer") {
                supplyBY = "2";
            }
            for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
                if (app.components.AssemblyData.BOM.Line[i].MPN != undefined) {
                    if (app.components.AssemblyData.BOM.Line[i].uniqid == uniqid) {
                        app.components.AssemblyData.BOM.Line[i]._refdes = refdes;
                        app.components.AssemblyData.BOM.Line[i]._supply = supplyBY;
                        app.components.AssemblyData.BOM.Line[i]._qty = qty;
                        app.components.AssemblyData.BOM.Line[i].Comp = app.updateCom(app.components.AssemblyData.BOM.Line[i].Comp, refdes);
                        break;
                    }
                }
            }
            for (var i = 0; i < app.parts.length; i++) {
                if (app.parts[i].uniqid === uniqid) {
                    app.parts[i].qty = refdes.split(',').length;
                    app.parts[i].mount_type = mount_type;
                    app.parts[i].supply = supply;
                    app.parts[i].refdes = refdes;
                    app.parts[i].price = -2;
                }
            }
            $('#updatedata').modal('hide');

            $("input[uniqid='" + uniqid + "']").closest('tr').addClass('highlight');//----for animation
            setTimeout(function () { $("input[uniqid='" + uniqid + "']").closest('tr').removeClass('highlight'); }, 2000);//----for animation


            app.saveBOM();
        },
        switchReviewMode: function (e) {

            if (app.reviewMode) {
                app.reviewMode = false;
            }
            else {
                app.reviewMode = true;
            }

        },
        reviewMPN: function (part) {
            //for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
            //    if (app.components.AssemblyData.BOM.Line[i].MPN != undefined) {
            //        if (app.components.AssemblyData.BOM.Line[i].MPN._mpn == part.mpn) {
            //            if (part.review == 1) {
            //                app.components.AssemblyData.BOM.Line[i]._review = 0;
            //            }
            //            else {
            //                app.components.AssemblyData.BOM.Line[i]._review = 1;
            //            }
            //            break;
            //        }
            //    }
            //}
            //for (var i = 0; i < app.parts.length; i++) {
            //    if (app.parts[i].mpn === part.mpn) {
            //        if (part.review == 1) {
            //            app.parts[i].review = 0;
            //        }
            //        else {
            //            app.parts[i].review = 1;
            //        }
            //    }
            //}  
            if (Array.isArray(app.components.AssemblyData.BOM.Line)) {
                for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
                    if (app.components.AssemblyData.BOM.Line[i].MPN != undefined) {
                        if (app.components.AssemblyData.BOM.Line[i].uniqid == part.uniqid) {
                            if (part.review == 1) {
                                app.components.AssemblyData.BOM.Line[i]._review = 0;
                            }
                            else {
                                app.components.AssemblyData.BOM.Line[i]._review = 1;
                            }
                            break;
                        }
                    }
                }
            }
            else {
                if (part.review == 1) {
                    app.components.AssemblyData.BOM.Line._review = 0;
                }
                else {
                    app.components.AssemblyData.BOM.Line._review = 1;
                }
            }
            
            for (var i = 0; i < app.parts.length; i++) {
                if (app.parts[i].uniqid === part.uniqid) {
                    if (part.review == 1) {
                        app.parts[i].review = 0;
                    }
                    else {
                        app.parts[i].review = 1;
                    }
                }
            }
            app.saveBOM();
        },
        updateCom: function (comp, refdes) {
            var Comp = []
            var arrRefdes = refdes.split(',');
            if (!Array.isArray(comp)) {
                for (var i = 0; i < arrRefdes.length; i++) {
                    if (comp._refdes.toUpperCase() == arrRefdes[i].toUpperCase()) {
                        Comp.push(comp);
                    }
                    else if (arrRefdes[i] != "") {
                        var cmp = {}
                        cmp._refdes = arrRefdes[i]
                        //var comp={}
                        cmp._appr = "0";
                        cmp._layer = "top";
                        cmp._rot = "";
                        cmp._x = "NaN";
                        cmp._y = "NaN";
                        Comp.push(cmp);
                    }
                }
            }
            else {
                for (var i = 0; i < arrRefdes.length; i++) {
                    var newcom = false;
                    for (var j = 0; j < comp.length; j++) {
                        if (comp[j]._refdes.toUpperCase() == arrRefdes[i].toUpperCase()) {
                            newcom = false;
                            Comp.push(comp[j]);
                        }
                    }

                    if (!newcom) {
                        var cmp = {}
                        cmp._refdes = arrRefdes[i]
                        //var comp={}
                        cmp._appr = "0";
                        cmp._layer = "top";
                        cmp._rot = "";
                        cmp._x = "NaN";
                        cmp._y = "NaN";
                        Comp.push(cmp);
                    }
                }
            }
            return Comp;
            //if (comp != undefined) {

            //if(comp._refdes==)
            ////var comp={}
            ////comp._appr
            ////comp._layer
            ////comp._refdes
            ////comp._rot
            ////comp._x
            ////comp._y
        },
        saveBoardQty: function () {
            var boardqty = $("#txtBoardQty").val();
            var oldqty = $("#ctl00_mainContent_lblbomqty").text();
            if (boardqty != "" && boardqty != oldqty) {
                axios.post('/shop/assembly/bom.aspx/updatebomqty', {
                    sessionId: this.sessionId,
                    boardqty: boardqty
                })
                .then(function (response) {
                    $("#ctl00_mainContent_lblbomqty").text(boardqty);
                    app.boardQuantity = boardqty;
                    $(".edit-brd-qty").show();
                    $(".BoardQty").hide();
                    $(".boardqty-save").hide();
                })
                .catch(function (error) {
                    console.log(error);
                });
            }
            else {
                $(".edit-brd-qty").show();
                $(".BoardQty").hide();
                $(".boardqty-save").hide();
            }
        },
        getPartList: function (mpn) {
            this.showLoading = true;
            axios.post('/shop/assembly/bom.aspx/getbomfilelistdata', {
                sessionid: this.sessionId
            })
            .then(function (response) {

                app.showLoading = false;
                var x2js = new X2JS();
                app.showLoading = false;
                app.components = x2js.xml_str2json(response.data.d);
                //app.components
                app.gePartsFromComponents(mpn);
                app.bindChart();
                app.updatePrice();
            })
            .catch(function (error) {
                app.showLoading = false;
                console.log(error);
            });
        },
        searchAlternatives: function (e) {
            if (e.which == 13) {
                app.findAlternatives();
            }
        },
        updatePrice: function () {
            var data = "";
            for (var i = 0; i < app.parts.length; i++) {
                if (app.parts[i].price == -2) {
                    data = data + '<Part mpn="' + app.parts[i].mpn + '" gpn="' + app.parts[i].gpn + '" ordQty="' + app.parts[i].qty * app.boardQuantity + '"/>'
                }
            }
            if (data != "") {
                app.getPrice(data)
                setTimeout(function () { app.updatePrice() }, 3000);
            }
            app.calculateTotalPrice();
        },
        calculateTotalPrice: function () {
            var totalPrice = 0;
            app.partWithprice = 0;
            app.partWithoutPrice = 0
            for (var i = 0; i < app.parts.length; i++) {
                if ((app.parts[i].price > 0) || (app.parts[i].supply == "Customer" && app.parts[i].price == 0)) {
                    totalPrice = totalPrice + Math.round(app.parts[i].price * 100) / 100;
                    app.partWithprice++;
                }
                else if (app.parts[i].price == 0 && app.parts[i].gpn == 1) {
                    app.partWithprice++;
                }
                else if (app.parts[i].price == -1) {
                    app.partWithoutPrice++;
                }
            }
            app.totalComponentsPrice = Math.round(totalPrice * 100) / 100;
        },
        getPrice: function (data) {
            var data = '<Parts>' + data + '</Parts>';
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "/shop/assembly/updatePrice.aspx?id=AssemblyFirst",
                "method": "POST",
                "headers": {
                    "Content-Type": "text/xml",
                    "cache-control": "no-cache"
                },
                "data": data
            }
            $.ajax(settings).done(function (response) {
                var xmlDoc = $.parseXML(response),
                           $xml = $(xmlDoc);
                var part = $xml.find("Part"); //get church names from xml file
                $(part).each(function (id, item) {
                    var price = $(item).attr('price')
                    var mpn = $(item).attr('mpn')
                    var supply = "";
                    app.priceLoading = false;
                    for (var i = 0; i < app.parts.length; i++) {
                        if (app.parts[i].mpn === mpn) {
                            supply = app.parts[i].supply;
                            if (app.parts[i].supply == "Customer" || app.parts[i].gpn == 1) {
                                if (price > 0) {
                                    app.parts[i].price = Math.round(((price * 20) / 100) * 100) / 100;
                                }
                                else {
                                    app.parts[i].price = 0;
                                }
                            }
                            else if (price != 0) {
                                app.parts[i].price = Math.round(price * 100) / 100;
                            }
                            else {
                                app.priceLoading = true;
                            }
                        }
                    }
                    if (Array.isArray(app.components.AssemblyData.Prices.Price)) {
                        $.each(app.components.AssemblyData.Prices.Price, function (key, price) {
                            if (price._mpn.toUpperCase() === mpn.toUpperCase()) {
                                price._prodQty = $(item).attr('prodQty') ? $(item).attr('prodQty') : "0";
                                price._purchQty = $(item).attr('purchQty') ? $(item).attr('purchQty') : "0";
                                price._moq = $(item).attr('moq') ? $(item).attr('moq') : "0";
                                price._price = $(item).attr('price') ? $(item).attr('price') : "-1";
                                price._suppl = $(item).attr('suppl') ? $(item).attr('suppl') : "";
                                price._sku = $(item).attr('sku') ? $(item).attr('sku') : "";
                                price._ordQty = $(item).attr('ordQty') ? $(item).attr('ordQty') : "";
                                price._val = $(item).attr('val') ? $(item).attr('val') : "";
                                price._stock = $(item).attr('stock') ? $(item).attr('stock') : "0";
                            }
                        });
                    }
                    else {
                        if (app.components.AssemblyData.Prices.Price._mpn.toUpperCase() === mpn.toUpperCase()) {
                            price._prodQty = $(item).attr('prodQty') ? $(item).attr('prodQty') : "0";
                            price._purchQty = $(item).attr('purchQty') ? $(item).attr('purchQty') : "0";
                            price._moq = $(item).attr('moq') ? $(item).attr('moq') : "0";
                            price._price = $(item).attr('price') ? $(item).attr('price') : "-1";
                            price._suppl = $(item).attr('suppl') ? $(item).attr('suppl') : "";
                            price._sku = $(item).attr('sku') ? $(item).attr('sku') : "";
                            price._ordQty = $(item).attr('ordQty') ? $(item).attr('ordQty') : "";
                            price._val = $(item).attr('val') ? $(item).attr('val') : "";
                            price._stock = $(item).attr('stock') ? $(item).attr('stock') : "0";
                        }
                    }
                });
                app.calculateTotalPrice();
            });

        },
        datasheetURL: function (datasheet_url) {
            if (datasheet_url != null && datasheet_url != "") {
                window.open(datasheet_url, '_blank');
            }
        },
        downloadpoup: function () {
            $('#DownloadBomfiles').modal('show');
            $('#chkBOM').attr('checked', true);
        },
        getFormattedTime: function () {
            var today = new Date();
            var y = today.getFullYear();
            // JavaScript months are 0-based.
            var m = today.getMonth() + 1;
            var d = today.getDate();
            var h = today.getHours();
            var mi = today.getMinutes();
            var s = today.getSeconds();
            return d + "" + m + "" + y + "" + h + "" + mi + "" + s;
        },
        partcheck: function (part) {
            //if ($('[mpn="' + part.mpn + '"]').prop("checked")) {
            //    $('[mpn="' + part.mpn + '"]').prop('checked', true);
            //    $('[mpn="' + part.mpn + '"]').attr('checked', 'checked');
            //    app.headerbtn(true);
            //}
            //else {
            //    $('[mpn="' + part.mpn + '"]').prop('checked', false);
            //    $('[mpn="' + part.mpn + '"]').removeAttr('checked');

            //    var anychecked = false;
            //    $('.chkmpn').each(function (index, obj) {
            //        if (this.checked === true) {
            //            anychecked = true;
            //        }
            //    });
            //    if (!anychecked) { app.headerbtn(false); }
            //} 
            if ($('[uniqid="' + part.uniqid + '"]').prop("checked")) {
                $('[uniqid="' + part.uniqid + '"]').prop('checked', true);
                $('[uniqid="' + part.uniqid + '"]').attr('checked', 'checked');
                app.headerbtn(true);
            }
            else {
                $('[uniqid="' + part.uniqid + '"]').prop('checked', false);
                $('[uniqid="' + part.uniqid + '"]').removeAttr('checked');

                var anychecked = false;
                $('.chkmpn').each(function (index, obj) {
                    if (this.checked === true) {
                        anychecked = true;
                    }
                });
                if (!anychecked) { app.headerbtn(false); }
            }
        },
        toggleAll: function () {
            if ($("#allchk").prop("checked")) {
                $(".chkmpn").prop('checked', true);
                $(".chkmpn").attr('checked', 'checked');
                app.headerbtn(true);
            }
            else {
                $(".chkmpn").prop('checked', false);
                $(".chkmpn").removeAttr('checked');
                app.headerbtn(false);
            }
        },
        showaddlist: function (e) {
            $("#list-detail").show('show');
            e.preventDefault()
            e.stopPropagation();
        },
        addtolist: function (session_id) {
            var data = []
            var searchData = [];
            var xmlData = [];
            //$('.chkmpn').each(function (index, obj) {
            //    if (this.checked === true) {
            //        var mpn = $(this).attr("mpn");
            //        for (var i = 0; i < app.parts.length; i++) {
            //            if (app.parts[i].mpn === mpn) {

            //                data.push(app.parts[i]);
            //                var searchNode = {};
            //                var xmlNode = {};
            //                searchNode["index"] = i
            //                searchNode["mpn"] = app.parts[i].mpn;
            //                searchNode["refdes"] = "";
            //                searchNode["qty"] = "";
            //                var spec = BOMUtility.sanatizeKeyword(app.parts[i].mpn, 1);
            //                searchNode["specs"] = spec.specs;
            //                searchData.push(searchNode);

            //                xmlNode["index"] = i
            //                xmlNode["mpn"] = app.parts[i].mpn;
            //                xmlNode["refdes"] = "";
            //                xmlNode["qty"] = "";
            //                xmlData.push(xmlNode);
            //                break;
            //            }
            //        }
            //    }
            //});
            $('.chkmpn').each(function (index, obj) {
                if (this.checked === true) {
                    var uniqid = $(this).attr("uniqid");
                    for (var i = 0; i < app.parts.length; i++) {
                        if (app.parts[i].uniqid === uniqid) {

                            data.push(app.parts[i]);
                            var searchNode = {};
                            var xmlNode = {};
                            searchNode["index"] = i
                            searchNode["mpn"] = app.parts[i].mpn;
                            searchNode["refdes"] = app.parts[i].refdes;
                            searchNode["qty"] = "";
                            var spec = BOMUtility.sanatizeKeyword(app.parts[i].mpn, 1);
                            searchNode["specs"] = spec.specs;
                            searchData.push(searchNode);

                            xmlNode["index"] = i
                            xmlNode["mpn"] = app.parts[i].mpn;
                            xmlNode["refdes"] = app.parts[i].refdes;
                            xmlNode["qty"] = "";
                            xmlData.push(xmlNode);
                            break;
                        }
                    }
                }
            });
            var postData = {
                sessionId: session_id,
                searchData: JSON.stringify(searchData),
                xmlData: JSON.stringify(xmlData)
            }
            axios.post('/shop/assembly/ecpartbomimport.aspx/AddtoBoM', postData)
            .then(function (data) {
                //---reload 
                $(".chkmpn").prop('checked', false);
                $(".chkmpn").removeAttr('checked');
                app.headerbtn(false);
            })
            .catch(function (error) {
                console.log(error);
            });

            e.preventDefault()
        },
        headerbtn: function (check) {
            if (check == true) {
                $("#btnremove").removeAttr("disabled");
                $("#btncopyto").removeAttr("disabled");

                $("#btnremove").removeClass("btn-disabled");
                $("#btncopyto").removeClass("btn-disabled");

                $("#btnremove").addClass("btn-select-top clr-red");
                $("#btncopyto").addClass("btn-select-top");
            }
            else {
                $("#btnremove").attr("disabled", "disabled");
                $("#btncopyto").attr("disabled", "disabled");

                $("#btnremove").addClass("btn-select-top btn-disabled");
                $("#btncopyto").addClass("btn-select-top btn-disabled");
            }
        },
        scrolldown: function () {
            $('html, body').animate({
                scrollTop: $(document).height()
            }, 'slow');
        },
        removerow: function (e) {
            $("#remove-list-detail").show();
            e.preventDefault()
            e.stopPropagation();
        },
        removedata: function (e) {
            var isdelete = false;
            var totalSelected = 0;
            $('.chkmpn').each(function (index, obj) {
                if (this.checked === true) {
                    totalSelected++;
                }
            });
            var totalLoopSelected = 0;
            for (var i = 0; i < app.parts.length; i++) {
                //if (app.parts[i].uniqid === uniqid) {
                var uniqid = app.parts[i].uniqid;
                var isselected = app.checkSelected(uniqid);
                if (isselected) {
                    totalLoopSelected++;
                    //----for animation 
                    $("input[uniqid='" + uniqid + "']").closest('tr').addClass('highlight-remove');//----for animation
                    if (totalSelected == totalLoopSelected) {
                        setTimeout(function () { app.removeFromList(e); }, 1000);//----for animation   
                    }
                }
            }
            e.preventDefault()
            e.stopPropagation();
        },
        removeFromList: function (e) {
            var isdeleted = false;
            $("input[class='chkmpn']").closest('tr').removeClass('highlight-remove');
            for (var i = 0; i < app.parts.length; i++) {
                var uniqid = app.parts[i].uniqid;
                var isselected = app.checkSelected(uniqid);
                if (isselected) {
                    app.removeAssemblyData(uniqid);
                    app.parts.splice(i, 1);
                    i--;
                    isdeleted = true;
                }
            }
            if (isdeleted) {
                app.saveBOM();
                $(".chkmpn").prop('checked', false);
                $(".chkmpn").removeAttr('checked');
                app.headerbtn(false);
                $("#remove-list-detail").hide();
                app.gePartsFromComponents();
                app.bindChart();
            }
            e.preventDefault()
            e.stopPropagation();
        },
        checkSelected: function (uniqid) {
            var isSelected = false;
            $('.chkmpn').each(function (index, obj) {
                if (this.checked === true) {
                    var chkuniqid = $(this).attr("uniqid");
                    if (uniqid == chkuniqid) {
                        isSelected = true;
                    }
                }
            });
            return isSelected;
        },
        removeAssemblyData: function (uniqid) {
            if (Array.isArray(app.components.AssemblyData.BOM.Line)) {
                for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
                    if (app.components.AssemblyData.BOM.Line[i].uniqid == uniqid) {
                        if (app.components.AssemblyData.BOM.Line[i].MPN != undefined) {
                            app.removePartData(app.components.AssemblyData.BOM.Line[i].MPN._mpn);
                        }
                        app.components.AssemblyData.BOM.Line.splice(i, 1);
                        break;
                    }
                }
            }
            else {
                app.components.AssemblyData.BOM = "";
            }
        },
        removePartData: function (mpn) {
            if (Array.isArray(app.components.AssemblyData.Parts.Part)) {
                for (var i = 0; i < app.components.AssemblyData.Parts.Part.length > 0; i++) {
                    if (app.components.AssemblyData.Parts.Part[i] != undefined) {
                        if (app.components.AssemblyData.Parts.Part[i]._mpn == mpn) {
                            app.removePriceData(mpn);
                            app.components.AssemblyData.Parts.Part.splice(i, 1);
                            break;
                        }
                    }
                }
            }
            else {
                app.components.AssemblyData.Parts = "";
            }
        },
        removePriceData: function (mpn) { 
            if (Array.isArray(app.components.AssemblyData.Prices.Price)) {
                for (var i = 0; i < app.components.AssemblyData.Prices.Price.length > 0; i++) {
                    if (app.components.AssemblyData.Prices.Price[i]._mpn != undefined) {
                        if (app.components.AssemblyData.Prices.Price[i]._mpn == mpn) {
                            app.components.AssemblyData.Prices.Price.splice(i, 1);
                            break;
                        }
                    }
                }
            }
            else {
                app.components.AssemblyData.Prices = "";
            }
        },
        hidepopup: function (e) {
            $("#remove-list-detail").hide();
            e.preventDefault()
            e.stopPropagation();
        },
        isNumber: function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if ((charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 46))) {
                evt.preventDefault();;
            } else {
                return true;
            }
        },
        alldownload: function (parts) {
            var $box = $('#chkBOM').attr('checked', true);
            if ($box.is(":checked")) {
                var filename = "BoM_" + this.getFormattedTime() + ".csv";
                //var filenamenew = this.getParameterByName('filename');
                //if (typeof filenamenew !== "undefined") {
                //    filename = filenamenew;
                //}
                this.export_csv(parts, this.filename + ".csv");
            }
            var uid = "";
            var footprint = "";
            var isChecked = "false";
            this.showLoading = true;
            for (i = 0; i < parts.length; i++) {
                var row = parts[i];
                if ($("#chkAltium").prop("checked") == true) {
                    footprint = "footprint-Altium";
                    isChecked = "true";
                    if (parts[i].ft_altium != null && parts[i].ft_altium != "" && parts[i].ft_altium != "false") {
                        uid += parts[i].ft_altium + "|";
                    }
                }

                //Eagle
                if ($("#chkEagle").prop("checked") == true) {
                    footprint = "footprint-Eagle";
                    isChecked = "true";
                    if (parts[i].ft_eagle != null && parts[i].ft_eagle != "" && parts[i].ft_eagle != "false") {
                        uid += parts[i].ft_eagle + "|";
                    }
                }

                //FPX
                if ($("#chkFPX").prop("checked") == true) {
                    footprint = "footprint-FPX";
                    isChecked = "true";
                    if (parts[i].ft_fpx != null && parts[i].ft_fpx != "" && parts[i].ft_fpx != "false") {
                        uid += parts[i].ft_fpx + "|";
                    }
                }

                //KiCAD
                if ($("#chkKiCAD").prop("checked") == true) {
                    footprint = "footprint-KiCAD";
                    isChecked = "true";
                    if (parts[i].ft_kicad != null && parts[i].ft_kicad != "" && parts[i].ft_kicad != "false") {
                        uid += parts[i].ft_kicad + "|";
                    }
                }
            }
            //---------------- Download footprint zip file
            if (uid != "") {
                var dataValue = { "uids": uid, "footprint": footprint };
                $.ajax({
                    type: "post",
                    url: "/shop/assembly/dft.aspx",
                    data: dataValue,
                    contenttype: "application/x-www-form-urlencoded",
                    datatype: 'xml',
                    success: function (data) {
                        this.showLoading = false;
                        if (data != "") {
                            window.open("/shop/assembly/dft.aspx?sessionid=" + data + "&footprint=" + footprint);
                        }
                    }
                });
            }
            else if (uid == "" && isChecked == "true") {
                $("#lblBomDownloaderror").removeClass("hide");
                this.showLoading = false;
                setTimeout(function () {
                    $("#lblBomDownloaderror").addClass("hide");
                }, 3000);
            }
            else {
                this.showLoading = false;
            }
        },
        export_csv: function (parts, filename) {
            var csv = this.ConvertDatatableToCSV(parts, true);
            this.download_csv(csv, filename);
        },
        download_csv: function (csv, filename) {
            var csvFile;
            var downloadLink;
            csvFile = new Blob([csv], { type: "text/csv" });
            downloadLink = document.createElement("a");
            downloadLink.download = filename;
            downloadLink.href = window.URL.createObjectURL(csvFile);
            downloadLink.style.display = "none";
            document.body.appendChild(downloadLink);
            downloadLink.click();
        },
        saveBOM: function () { 
            //if (Array.isArray(app.components.AssemblyData.BOM.Line)) {
            //    for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
            //        delete app.components.AssemblyData.BOM.Line[i].uniqid;
            //    }
            //}
            //else {
            //    if (app.components.AssemblyData.BOM != "") {
            //        delete app.components.AssemblyData.BOM.Line.uniqid;
            //    } 
            //} 
            app.priceLoading = true;
            var x2js = new X2JS();
            var data = x2js.json2xml_str(app.components)
            var postData = {
                sessionId: app.sessionId,
                data: data
            }
            axios.post('/shop/assembly/bom.aspx/saveupdatedbom', postData)
            .then(function (data) {
                app.updatePrice();
            })
            .catch(function (error) {
                console.log(error);
            });

        },
        ConvertDatatableToCSV: function (parts, isexport) {
            var csv = '';
            var ecpartlist = [];

            var totalRows = parts.length;
            if (isexport == true) {
                if (totalRows > 0) {
                    ecpartlist.push({
                        mpn: 'MPN',
                        category: 'Category',
                        descr: 'Description',
                        ipcname: 'IPC Name',
                        manufacturer: 'Manufacturer',
                    })
                }
                for (i = 0; i < totalRows; i++) {
                    var row = parts[i];
                    ecpartlist.push({
                        mpn: row.mpn,
                        category: row.category,
                        descr: row.descr,
                        ipcname: row.ipcname,
                        manufacturer: row.manufacturer,
                    })
                }
            }
            var jsonObject = JSON.stringify(ecpartlist);
            var csv = this.convertToCSV(jsonObject);
            return csv;
        },
        convertToCSV: function (objArray) {
            var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
            var str = '';
            for (var i = 0; i < array.length; i++) {
                var line = '';
                for (var index in array[i]) {
                    if (line != '') line += '|'

                    line += array[i][index];
                }
                str += line + '\r\n';
            }
            return str;
        },
        downloadfootprint: function (part) {
            var path = "/shop/assembly/dft.aspx?uid=";

            //Altium
            if (part.ft_altium == "" || part.ft_altium == "false") {
                $('#btnfootprint1').addClass('disabled');
            }
            else {
                $('#btnfootprint1').attr("href", path + part.ft_altium);
                $('#btnfootprint1').removeClass('disabled');
            }

            //Eagle
            if (part.ft_eagle == "" || part.ft_eagle == "false") {
                $('#btnfootprint2').addClass('disabled');
            }
            else {
                $('#btnfootprint2').attr("href", path + part.ft_eagle);
                $('#btnfootprint2').removeClass('disabled');
            }

            //FPX
            if (part.ft_fpx == "" || part.ft_fpx == "false") {
                $('#btnfootprint3').addClass('disabled');
            }
            else {
                $('#btnfootprint3').attr("href", path + part.ft_fpx);
                $('#btnfootprint3').removeClass('disabled');
            }

            //KiCAD
            if (part.ft_kicad == "" || part.ft_kicad == "false") {
                $('#btnfootprint4').addClass('disabled');
            }
            else {
                $('#btnfootprint4').attr("href", path + part.ft_kicad);
                $('#btnfootprint4').removeClass('disabled');
            }

            $('#myModalfootprint').modal('show');
        },
        bindChart: function () {

            if (app.parts.length > 0) {
                var ecstock = 0;
                var ondemand = 0;
                var matchedecpart = 0;
                var unidentifiedpart = 0;
                var matchwostock = 0;
                var sugggested = 0;

                for (var i = 0; i < app.parts.length; i++) {
                    if (app.parts[i].appr == "2") {
                        unidentifiedpart++;
                    }
                    else {

                        if (app.parts[i].appr == "0") {
                            sugggested++;
                        }

                        if (app.parts[i].stock == '1') {
                            ecstock++;
                        } else {
                            ondemand++;
                        }
                    }
                }

                $("#sp_chart_ecstock").addClass("ch_ecstock");
                $("#sp_chart_ondemand").addClass("ch_ondemand");
                $("#sp_chart2_matchedecpart").addClass("ch2_ecstock");
                $("#sp_chart2_unidentifiedpart").addClass("ch2_unidentfied");
                $("#sp_chart2_withoutstock").addClass("ch2_match");
                $("#sp_chart2_suggestedpart").addClass("ch2_suggest");

                var chart = $('#chart').highcharts();
                chart.series[0].setData([ecstock, ondemand], false);
                $('#chart').highcharts().redraw();
                $('#sp_chart_ecstock').text(ecstock);
                $('#sp_chart_ondemand').text(ondemand);

                var chartpartstatus = $('#chart-partStatus').highcharts();
                chartpartstatus.series[0].setData([ecstock, unidentifiedpart, ondemand, sugggested], false);
                $('#chart-partStatus').highcharts().redraw();
                $('#sp_chart2_matchedecpart').text(ecstock);
                $('#sp_chart2_unidentifiedpart').text(unidentifiedpart);
                $('#sp_chart2_withoutstock').text(ondemand);
                $('#sp_chart2_suggestedpart').text(sugggested);
                $("#btnPlaceOrder").removeAttr("disabled");

            }
            else {
                app.priceLoading = false;
                var chart = $('#chart').highcharts();
                chart.series[0].setData([0, 0], false);
                $('#chart').highcharts().redraw();

                var chartpartstatus = $('#chart-partStatus').highcharts();
                chartpartstatus.series[0].setData([{
                    y: 1,
                    color: "#d2d2d2"
                }], false);

                $('#chart-partStatus').highcharts().redraw();

                $("#sp_chart_ecstock").removeClass("ch_ecstock");
                $("#sp_chart_ondemand").removeClass("ch_ondemand");
                $("#sp_chart2_matchedecpart").removeClass("ch2_ecstock");
                $("#sp_chart2_unidentifiedpart").removeClass("ch2_unidentfied");
                $("#sp_chart2_withoutstock").removeClass("ch2_match");
                $("#sp_chart2_suggestedpart").removeClass("ch2_suggest");

                $(".lable-counter").addClass("c-gray");

                $("#btnPlaceOrder").attr("disabled", "disabled");

            }
        },
        altSorting: function (e, sortType) {
            if ($(e.target).hasClass("sorting_asc")) {
                $("#alternativeParts th").removeClass("sorting_desc");
                $("#alternativeParts th").removeClass("sorting_asc");

                $(e.target).addClass("sorting_desc");

                if (sortType == "desc") {
                    if (app.alternativeParts.length > 0) {
                        app.alternativeParts = app.sortByKeyAsc(app.alternativeParts, "desc");
                    }
                }
                else {
                    if (app.alternativeParts.length > 0) {
                        app.alternativeParts = app.sortByKeyAsc(app.alternativeParts, "stock");
                    }
                }
            }
            else {
                $("#alternativeParts th").removeClass("sorting_desc");
                $("#alternativeParts th").removeClass("sorting_asc");
                $(e.target).addClass("sorting_asc");
                if (sortType == "desc") {
                    if (app.alternativeParts.length > 0) {
                        app.alternativeParts = app.sortByKeyDesc(app.alternativeParts, "desc");
                    }
                }
                else {
                    if (app.alternativeParts.length > 0) {
                        app.alternativeParts = app.sortByKeyDesc(app.alternativeParts, "stock");
                    }
                }
            }
        },
        partTypeCount: function () {
            var AssemblyUniqueParts = 0;
            var AssemblyPartCountBGA = 0;
            var AssemblyPartCountBGAFP = 0;
            var AssemblyPartCountLGA = 0;
            var AssemblyPartCountMixed = 0;
            var AssemblyPartCountQFN = 0;
            var AssemblyPartCountQFNFP = 0;
            var AssemblyPartCountSMD = 0;
            var AssemblyPartCountSMDFP = 0;
            var AssemblyPartCountTH = 0;
            var AssemblyPartCountUnknown = 0;
            var AssemblyComponentCount = 0;
            var AssemblyComponentCountBGA = 0;
            var AssemblyComponentCountBGAFP = 0;
            var AssemblyComponentCountLGA = 0;
            var AssemblyComponentCountMixed = 0;
            var AssemblyComponentCountQFN = 0;
            var AssemblyComponentCountQFNFP = 0;
            var AssemblyComponentCountSMD = 0;
            var AssemblyComponentCountSMDFP = 0;
            var AssemblyComponentCountTH = 0;
            var AssemblyComponentCountUnknown = 0;

            for (var i = 0; i < app.parts.length; i++) {
                if (typeof app.parts[i].mount_type != "undefined") {
                    if (app.parts[i].mount_type.toUpperCase() == "BGA".toUpperCase()) {
                        AssemblyUniqueParts++;
                        AssemblyComponentCount++;
                        AssemblyPartCountBGA++;
                        AssemblyComponentCountBGA++;
                    }
                    if (app.parts[i].mount_type.toUpperCase() == "BGA FinePitch".toUpperCase()) {
                        AssemblyUniqueParts++;
                        AssemblyComponentCount++;
                        AssemblyPartCountBGAFP++;
                        AssemblyComponentCountBGAFP++;
                    }
                    if (app.parts[i].mount_type.toUpperCase() == "LGA".toUpperCase()) {
                        AssemblyUniqueParts++;
                        AssemblyComponentCount++;
                        AssemblyPartCountLGA++;
                        AssemblyComponentCountLGA++;
                    }
                    if (app.parts[i].mount_type.toUpperCase() == "Mixed".toUpperCase()) {
                        AssemblyUniqueParts++;
                        AssemblyComponentCount++;
                        AssemblyPartCountMixed++;
                        AssemblyComponentCountMixed++;
                    }
                    if (app.parts[i].mount_type.toUpperCase() == "QFN".toUpperCase()) {
                        AssemblyUniqueParts++;
                        AssemblyComponentCount++;
                        AssemblyPartCountQFN++;
                        AssemblyComponentCountQFN++;
                    }
                    if (app.parts[i].mount_type.toUpperCase() == "QFN FinePitch".toUpperCase()) {
                        AssemblyUniqueParts++;
                        AssemblyComponentCount++;
                        AssemblyPartCountQFNFP++;
                        AssemblyComponentCountQFNFP++;
                    }
                    if (app.parts[i].mount_type.toUpperCase() == "SMD".toUpperCase()) {
                        AssemblyUniqueParts++;
                        AssemblyComponentCount++;
                        AssemblyPartCountSMD++;
                        AssemblyComponentCountSMD++;
                    }
                    if (app.parts[i].mount_type.toUpperCase() == "SMD FinePitch".toUpperCase()) {
                        AssemblyUniqueParts++;
                        AssemblyComponentCount++;
                        AssemblyPartCountSMDFP++;
                        AssemblyComponentCountSMDFP++;
                    }
                    if ((app.parts[i].mount_type.toUpperCase() == "TH".toUpperCase()) || (app.parts[i].mount_type.toUpperCase() == "Mechanical".toUpperCase())) {
                        AssemblyUniqueParts++;
                        AssemblyComponentCount++;
                        AssemblyPartCountTH++;
                        AssemblyComponentCountTH++;
                    }
                    if (app.parts[i].mount_type.toUpperCase() == "Unknown".toUpperCase()) {
                        AssemblyUniqueParts++;
                        AssemblyComponentCount++;
                        AssemblyPartCountUnknown++;
                        AssemblyComponentCountUnknown++;
                    }
                }
            }

            return JSON.stringify({
                AssemblyUniqueParts: AssemblyUniqueParts,
                AssemblyPartCountBGA: AssemblyPartCountBGA,
                AssemblyPartCountBGAFP: AssemblyPartCountBGAFP,
                AssemblyPartCountLGA: AssemblyPartCountLGA,
                AssemblyPartCountMixed: AssemblyPartCountMixed,
                AssemblyPartCountQFN: AssemblyPartCountQFN,
                AssemblyPartCountQFNFP: AssemblyPartCountQFNFP,
                AssemblyPartCountSMD: AssemblyPartCountSMD,
                AssemblyPartCountSMDFP: AssemblyPartCountSMDFP,
                AssemblyPartCountTH: AssemblyPartCountTH,
                AssemblyPartCountUnknown: AssemblyPartCountUnknown,
                AssemblyComponentCount: AssemblyComponentCount,
                AssemblyComponentCountBGA: AssemblyComponentCountBGA,
                AssemblyComponentCountBGAFP: AssemblyComponentCountBGAFP,
                AssemblyComponentCountLGA: AssemblyComponentCountLGA,
                AssemblyComponentCountMixed: AssemblyComponentCountMixed,
                AssemblyComponentCountQFN: AssemblyComponentCountQFN,
                AssemblyComponentCountQFNFP: AssemblyComponentCountQFNFP,
                AssemblyComponentCountSMD: AssemblyComponentCountSMD,
                AssemblyComponentCountSMDFP: AssemblyComponentCountSMDFP,
                AssemblyComponentCountTH: AssemblyComponentCountTH,
                AssemblyComponentCountUnknown: AssemblyComponentCountUnknown
            });
        },
        partApprove: function (part) {
            if (Array.isArray(app.components.AssemblyData.BOM.Line)) {
                for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
                    if (app.components.AssemblyData.BOM.Line[i].MPN != undefined) {
                        if (app.components.AssemblyData.BOM.Line[i].MPN._mpn == part.mpn) {
                            app.components.AssemblyData.BOM.Line[i]._appr = "1";
                        }
                    }
                }
            }
            else {
                app.components.AssemblyData.BOM.Line._appr = "1";
            }

            for (var i = 0; i < app.parts.length; i++) {
                if (app.parts[i].mpn === part.mpn) {
                    part.appr = "1";
                }
            }
            app.saveBOM();
        },        
        getUniqid: function () {
            this.length = 8;
            this.timestamp = +new Date;
            var ts = this.timestamp.toString();
            var parts = ts.split("").reverse();
            var id = "";

            for (var i = 0; i < this.length; ++i) {
                var index = app.getRandomInt(0, parts.length - 1);
                id += parts[index];
            } 
            return "af" + id;
        },
        getRandomInt: function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },
        resetReviewPopup: function (e) {
            $("#reset-review-detail").show();
            e.preventDefault()
            e.stopPropagation();
        },
        resetReview: function (e) { 
            if (Array.isArray(app.components.AssemblyData.BOM.Line)) {
                for (var i = 0; i < app.components.AssemblyData.BOM.Line.length > 0; i++) {
                    if (app.components.AssemblyData.BOM.Line[i].MPN != undefined) {  
                           app.components.AssemblyData.BOM.Line[i]._review = 0; 
                    }
                }
            }
            else {
                app.components.AssemblyData.BOM.Line._review = 0;
            }

            for (var i = 0; i < app.parts.length; i++) { 
                    app.parts[i].review = 0; 
            }
            app.saveBOM();
            $("#reset-review-detail").hide();
            e.preventDefault();
            e.stopPropagation();
        },
        hideresetReviewPopup: function (e) {
            $("#reset-review-detail").hide();
            e.preventDefault();
            e.stopPropagation();
        },
        showondemand: function (part, event) { 
            var target = $(event.target);
            $('[data-toggle="popover"]').popover({
                html: true,
                content: function () {
                    return $('#popover-content').html();
                }
            })
            //$(event.target).popover({
            //    html: true,
            //    content: function () {
            //        $('.popover').popover('hide');
            //        return $('#popover-content').html();
            //    }
            //});   
            event.preventDefault();
            event.stopPropagation();
        }
        //showondemand: function (part, event) {
        //    var target = $(event.target); 
        //    var offset = target.offset(); 
             
        //    //var t = $(event.target).offset().top;
        //    //var l = $(event.target).offset().left; 
             
        //    //var position = $(event.target).position();
        //    debugger;
        //    //$(".ondemand-list-detail").hide();
        //    var uniqid = part.uniqid; 
        //    //$("#ondemand-list-detail-" + uniqid).offset({ top: offset.top, left: offset.left });
        //    //$("#ondemand-list-detail-" + uniqid).css({ top: (t - $(event.target)[0].offsetTop - 35), left: (l - $(event.target)[0].offsetLeft) });
        //    //$("#ondemand-list-detail-" + uniqid).css({ top: position.top, left: position.left });
        //    //$("#ondemand-list-detail-" + uniqid).css({ 'top': event.pageY - 500, 'left': event.pageX - 170 });
        //    //$("#ondemand-list-detail-" + uniqid).css({ top: offset.top - 490, left: offset.left - 150 }); 

        //    //$("#ondemand-list-detail-" + uniqid).show();
        //    event.preventDefault();
        //    event.stopPropagation();
        //}
    }
});

