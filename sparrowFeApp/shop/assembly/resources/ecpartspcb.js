﻿var ecPartsPcb = {
    bsessionid: "",
    events: function () {
        $("#btnCalculate-index").click(function () {
            var boardqty = $("#txt-noof-board").val();
            var deliveryterms = $("#boardwk-lbl").attr("days");
            var layer = $("#boardlayer-lbl").attr("layers");

            var boardwidth = $("#txt-calc-bwidth").val();
            var boardheight = $("#txt-calc-bheight").val();

            if (boardheight.trim() == "") {
                boardheight = "80";
            }
            if (boardwidth.trim() == "") {
                boardwidth = "100";
            } 
            if (ecPartsPcb.bsessionid == "") {
                window.location.href = '/shop/pcb/price.aspx?boardpricecal=true&layer=' + layer + '&boardqty=' + boardqty + '&boardwidth=' + boardwidth + '&boardheight=' + boardheight + '&deliveryterms=' + deliveryterms;
            }
            else {
                window.location.href = '/shop/pcb/price.aspx?bsessionid=' + ecPartsPcb.bsessionid + '&boardpricecal=true&layer=' + layer + '&boardqty=' + boardqty + '&boardwidth=' + boardwidth + '&boardheight=' + boardheight + '&deliveryterms=' + deliveryterms + '&fromecpart=true';
            }
        });

        $(".numbers-only").keypress(function (e) {
            if (e.which == 46) {
                if ($(this).val().indexOf('.') != -1) {
                    return false;
                }
            }

            if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        $(".boardprice-qty-home").click(function () {
            var value = $(this).attr("value");
            $("#lblboardqty-home").text(value);
            $("#boardqty-li-home").hide();
        });

        $(".boardwk-qty-home").click(function () {
            var boardqty = $(this).attr("qty")
            if (boardqty == "more") {
                window.location.href = '../../shop/orders/configurator.aspx?loadfrom=web&service=standardpool&deliverycountry=IN&invcountry=IN&country=IN&lang=en&quantity=2&dimX=100&dimY=80&layers=2&deliveryTerm=3';
            }
            else if (boardqty != "0") {
                $("#boardwk-lbl").text(boardqty + " Working days");
                $("#boardwk-lbl").attr("days", boardqty);
            }
        });


        $(".boardlayer-qty-home").click(function () {
            var boardqty = $(this).attr("qty")
            if (boardqty == "more") {
                window.location.href = '../../shop/orders/configurator.aspx?loadfrom=web&service=standardpool&deliverycountry=IN&invcountry=IN&country=IN&lang=en&quantity=2&dimX=100&dimY=80&layers=2&deliveryTerm=3';
            }
            else {
                $("#boardlayer-lbl").text(boardqty + " Layers");
                $("#boardlayer-lbl").attr("layers", boardqty);
            }
        });

        $(document).on("click", "#rblayer2", function () {
            $("#lbllayer2").addClass("font-bold-brdcal");
            $("#lbllayer4").removeClass("font-bold-brdcal");
        });
        $(document).on("click", "#rblayer4", function () {
            $("#lbllayer2").removeClass("font-bold-brdcal");
            $("#lbllayer4").addClass("font-bold-brdcal");
        });


        $(document).click(function (e) {
            var target = $(event.target);
            if (target.is(".boardlayer-li-home") || target.is(".boardlayer-lbl-span") || target.is(".boardlayer-lbl-home") || target.is(".boardlayer-lbl-qty") || target.is(".cls-boardlayer-ul-home") || target.is("#lblboardlayers-home") || target.is(".cls-boardlayer-caret-home") || target.is("#boardlayer-li-homeg"))  // if target div is not the one you want to exclude then add the class hidden
            {
                $("#boardlayer-li-home").show();
            }
            else {
                $("#boardlayer-li-home").hide();
            }
            if (target.is(".boardwk-li-home") || target.is(".boardwk-lbl-span") || target.is(".boardwk-lbl-home") || target.is(".boardwk-lbl-qty") || target.is(".cls-boardwk-ul-home") || target.is("#lblboardwk-home") || target.is(".cls-boardwk-caret-home") || target.is("#boardwk-li-homeg"))  // if target div is not the one you want to exclude then add the class hidden
            {
                $("#boardwk-li-home").show();
            }
            else {
                $("#boardwk-li-home").hide();
            }

        });
    },
    bindBreadcrumb: function () {
        var url = '/shop/pcb/pcbservice.ashx?isBreadcrumb=true&sessionId=' + ecPartsPcb.bsessionid + '&bomstage=addpcb';
        $.ajax({
            type: 'GET',
            url: url,
            dataType: "text",
            success: function (data) {
                if (data == "bomeditor" || data == "addpcb" || data == "orderpreview") {
                    $("#bomeditor").attr("href", "/shop/assembly/bom.aspx?sessionid=" + ecPartsPcb.bsessionid);
                }
                if (data == "addpcb" || data == "orderpreview") {
                    $("#addpcb").attr("href", "/shop/assembly/ecpartspcb.aspx?bsessionid=" + ecPartsPcb.bsessionid);
                }
                if (data == "orderpreview") {
                    $("#orderpreview").attr("href", "/shop/pcb/board.aspx?sessionid=" + ecPartsPcb.bsessionid + '&fromecpart=true');
                }
            }
        });
    },
    init: function () {
        ecPartsPcb.bsessionid = ecPartsPcb.getUrlParameter("bsessionid");
        $("#bomeditor").attr("href", "/shop/assembly/bom.aspx?sessionid=" + ecPartsPcb.bsessionid);
        var boardqty = ecPartsPcb.getUrlParameter("boardqty");
        if (typeof boardqty != "undefined" && boardqty != "") {
            $("#txt-noof-board").val(boardqty);
        } 
        ecPartsPcb.events();
        ecPartsPcb.bindBreadcrumb();
    },
    getUrlParameter: function (sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    }
}
$(document).ready(function () {
    ecPartsPcb.init();
});