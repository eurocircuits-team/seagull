﻿
var GPLIST = {
    ecpartfinallist: [],
    ecpartlist: [],
    isclosematch: true,
    cId: 0,
    bindDataTable: function (value, postsearchdata, isMulti) {
        $('#ecbomlist').show();
        $('#ecbomlist').dataTable({
            'responsive': true,
            'destroy': true,
            'filter': false,
            'bProcessing': true,
            'bServerSide': true,
            'searching': false,
            'pagingType': 'simple_numbers',
            'bAutoWidth': false,
            'bPaginate': false,
            'bInfo': false,
            'language': {
                'zeroRecords': 'Your serach for -<b>' + value + '</b>- did not match any parts.<br/>But, you can search with examples below:<br/><br/><div style="padding-left:15px;"><ul><li>A Part Number - ex: <a href="#" id="searchdefault">USB Type B</a>.</li><li>Category - ex: <a href="#" id="searchdefault1">Trimmer</a>.</li></ul></div>'
            },
            'columns': [
                {
                    'sTitle': 'MPN', 'sName': 'mpn', 'render': function (data, type, full, meta) {

                        //return '<input type="checkbox" id="collect" name="chkpart" partid="' + partid + '" value="' + $('<div/>').text(data).html() + '" ' + ischecked + ' >';

                        return '<div><span class="mpnNumber">' + data + '</span> <p class="extraInfo">' + full[2] + '</p></din>' + GPLIST.addfeatures(full[0]);
                    }
                },

                   // { 'sTitle': 'MPN', 'sName': 'mpn', 'width': '100px' },
                {
                    'sTitle': 'Image', 'sName': 'imgurl', 'render': function (data, type, full, meta) {
                        return "<img src=\"" + data + "\" onError=\"this.src='/shop/assembly/resources/no_image-01.png'; \"  class='imgecpartlist'/>";
                    }
                },

                { 'sTitle': 'Category', 'sName': 'category', 'visible': false },
                {
                    'sTitle': 'Description', 'sName': 'descr', 'render': function (data, type, full, meta) {

                        var roh = "";
                        if (full[6] == "Compliant") {
                            roh = '<p>RoHS : <img class="ecpartrohs" data-toggle="tooltip" data-placement="top" title="RoHS Compliant" src="/shop/assembly/resources/rohs_g.png"  ></img></p>';
                        }
                        return '<div><p style="width: 400px;word-wrap: break-word;">' + data + '</p> ' + roh + '<div>';
                    }
                },
                { 'sTitle': 'IPC Name', 'sName': 'ipcname', 'visible': false },
                {
                    'sTitle': 'Availability', 'sName': 'stock', 'width': '100px', 'render': function (data, type, full, meta) {
                        if (data != "0" && data != "") {

                            return '<div><p class="ecpartlbl"> eC-Stock</p><i class="fa fa-check-circle clsstock" aria-hidden="true"></i><div>';
                        }
                        else {
                            return 'On-demand';
                        }
                    }
                },
                {
                    'sTitle': 'RoHS Compliant', 'sName': 'specs.rohs_status', 'visible': false
                },
                {
                    'sTitle': 'Manufacturer', 'sName': 'manufacturer', 'render': function (data, type, full, meta) {


                        return '<div><span class="manufacturer">' + data + '</span> <p class="extraInfo-ipc"><span class="ipc">IPC#</span>' + full[4] + '</p></din>'

                    }
                },
                {
                    'sTitle': 'Media', 'sName': 'datasheet_url', 'width': '150px', 'render': function (data, type, full, meta) {

                        if (full.length > 14) {
                            var strImagdatasheet = "";
                            var strfootprint = "";
                            var str3d_outline = "";
                            var datasheet = "";
                            var footprint = "";
                            var str3d = "";
                            if (full[9] == "") {
                                strImagdatasheet = '/shop/assembly/resources/datasheet_outline.png';
                                datasheet = 'Datasheet not available';
                            }
                            else {
                                strImagdatasheet = '/shop/assembly/resources/datasheet.png';
                                datasheet = 'Datasheet available';
                            }

                            if ((full[10] == "" || full[10] == "false") && (full[11] == "" || full[11] == "false") && (full[12] == "" || full[12] == "false") && (full[13] == "" || full[13] == "false")) {
                                strfootprint = '/shop/assembly/resources/footprint_outline.png';
                                footprint = 'Footprint not available';
                            }
                            else {
                                strfootprint = '/shop/assembly/resources/footprint.png';
                                footprint = 'Footprint available';
                            }
                            if (full[14] == "" || full[14] == "false") {
                                str3d_outline = '/shop/assembly/resources/3d_outline.png';
                                str3d = '3D model not available';
                            }
                            else {
                                str3d_outline = '/shop/assembly/resources/3d.png';
                                str3d = '3D model available';
                            }
                            return '<img class="ecpartDA dataafterlogin" data-toggle="tooltip" data-placement="top" title="' + datasheet + '" src="' + strImagdatasheet + '?v=0.2"></img> &nbsp;&nbsp;' +
                                '<img class="ecpartDA dataafterlogin"   data-toggle="tooltip" data-placement="top" title="' + footprint + '" src="' + strfootprint + '?v=0.2"></img>&nbsp;&nbsp;' +
                                '<img class="ecpartDA dataafterlogin" data-toggle="tooltip" data-placement="top" title="' + str3d + '" src="' + str3d_outline + '?v=0.2"></img> &nbsp;&nbsp;';
                        }

                    }

                },
                { 'sTitle': 'ft_altium', 'sName': 'ft_altium', 'visible': false },
                { 'sTitle': 'ft_eagle', 'sName': 'ft_eagle', 'visible': false },
                { 'sTitle': 'ft_fpx', 'sName': 'ft_fpx', 'visible': false },
                { 'sTitle': 'ft_kicad', 'sName': 'ft_kicad', 'visible': false },
                { 'sTitle': 'wrl_3d', 'sName': 'wrl_3d', 'visible': false },
                { 'sTitle': 'is_polarized', 'sName': 'is_polarized', 'visible': false },
                { 'sTitle': 'is_poolable', 'sName': 'is_poolable', 'visible': false },
                { 'sTitle': 'mount_type', 'sName': 'mount_type', 'visible': false },
                { 'sTitle': 'is_generic', 'sName': 'is_generic', 'visible': false },
                { 'sTitle': 'part_id', 'sName': 'part_id', 'visible': false },
                { 'sTitle': 'category_id', 'sName': 'category_id', 'visible': false },
                { 'sTitle': 'data_Id', 'sName': 'data_Id', 'visible': false },
                { 'sTitle': 'data_Id_0', 'sName': 'data_Id_0', 'visible': false }
            ],


            'fnServerData': function (sSource, aoData, fnCallback, oSettings) {

                var draw = $.grep(aoData, function (v, i) { return v.name === 'draw'; })[0].value;
                var columns = $.grep(aoData, function (v, i) { return v.name === 'columns'; })[0].value;
                var order = $.grep(aoData, function (v, i) { return v.name === 'order'; })[0].value;
                var start = $.grep(aoData, function (v, i) { return v.name === 'start'; })[0].value;
                var length = $.grep(aoData, function (v, i) { return v.name === 'length'; })[0].value;
                var search = $.grep(aoData, function (v, i) { return v.name === 'search'; })[0].value;

                //var specs = BOMUtility.sanatizeKeyword(value, 1);
                //var postData = JSON.stringify({ query: value, start: 0, length: 50, extrasearch: searchdata });
                $(".pageloader").show()
                //$(".OverLay").show(); 
                oSettings.jqXHR = $.post("/shop/assembly/searchpart.aspx?opencomponentsearch=true &isMulti=" + isMulti, postsearchdata, function (data) {
                    var dataSet = [];
                    //var response = advancesearch(); 
                    var totaldata = 50;

                    if (data != null && data.Parts != null && data.Parts.length > 0) {
                        $('#tbldata').show();
                        //$('#bomUploadholder').hide();

                        data.Parts.forEach(function (hit) {
                            if (hit.total != null) {
                                return true;
                            }
                            var row = [];
                            columns.forEach(function (col) {
                                if (hit[col.name] == undefined) {
                                    row.push("");
                                }
                                else {
                                    row.push(hit[col.name]);
                                }
                            });
                            dataSet.push(row);
                        });
                    }
                    else {
                        //$('#bomUploadholder').show();
                        $('#tbldata').hide();
                    }
                    $(".pageloader").hide()
                    $('html, body').animate({
                        scrollTop: $('#tbldata').offset().top - 100
                    }, 'slow')
                    //$(".OverLay").hide();
                    fnCallback({
                        'draw': draw,
                        'recordsTotal': totaldata,
                        'recordsFiltered': totaldata,
                        'data': dataSet
                    });
                }, 'json');
            },
        });

    },
    bomSubmited: function (sessionid) {
        //
        // $('#divProcessing').hide();
        //$('#iframeImport').show();
        document.location.href = '/shop/assembly/bom.aspx?sessionid=' + $('#ctl00_mainContent_hdbomsessionId').val();;
        //$('#mdlImport').modal('hide');
        $("#iframeImport").attr("src", '');
    },

    saveBOMFile: function () {
        var searchData = [];
        var xmlData = [];

        var lines = $('#txtfile').val().split('\n');
        for (var i = 0; i < lines.length; i++) {
            var searchNode = {
                index: i,
                mpn: lines[i],
                spn: '',
                package: '',
                descr: '',
                category: '',
            };
            var xmlNode = {};
            xmlNode['index'] = i;
            xmlNode['mpn'] = lines[i];
            xmlNode['refdes'] = "";
            xmlNode['qty'] = "1";
            xmlData.push(xmlNode);
            searchData.push(searchNode);
        }
        var url = "/shop/assembly/ecpartbomimport.aspx/SaveBOM";
        var postData = {
            listName: $("#txtListName").val(),
            sessionId: $('#ctl00_mainContent_hdbomsessionId').val(),
            searchData: JSON.stringify(searchData),
            xmlData: JSON.stringify(xmlData),
            mapData: "",
            noAlternatives: "1"
        }

        Core.post(url, postData, function (result) {
            
            var data = result;
            if (data.Status == 0) {
                //Core.showMessage("errorMsg", true, BOMPART.captions.Error);  //Error occurred.
                console.log(data.Msg)
                return;
            }
            GPLIST.bomSubmited($('#ctl00_mainContent_hdbomsessionId').val());
        });

    },
    events: function () {
        $(document).ready(function (e) {
            $('.add-to-cart-button').click(function () {
                var target = $('.clsbomicon').first(),
                    target_offset = target.offset();

                var target_x = target_offset.left,
                    target_y = target_offset.top;

                //console.log('target: ' + target_x + ', ' + target_y);

                var obj_id = 1 + Math.floor(Math.random() * 100000),
                    obj_class = 'cart-animation-helper',
                    obj_class_id = obj_class + '_' + obj_id;

                var obj = $("<div>", {
                    'class': obj_class + ' ' + obj_class_id
                });

                //$(this).parent().parent().append(obj);
                $(this).parent().append(obj);

                var obj_offset = obj.offset(),
                    dist_x = target_x - obj_offset.left + 10,
                    dist_y = target_y - obj_offset.top + 10,
                    delay = 0.7; // seconds

                //console.log('obj_off: ' + obj_offset.left + ', ' + obj_offset.top);

                setTimeout(function () {
                    obj.css({
                        'transition': 'transform ' + delay + 's ease-in',
                        'transform': "translateX(" + dist_x + "px)"
                    });
                    $('<style>.' + obj_class_id + ':after{ \
				transform: translateY(' + dist_y + 'px); \
				opacity: 1; \
				border-radius: 100%; \
				max-height: 20px; \
				max-width: 20px; margin: 0; \
			}</style>').appendTo('head');
                }, 0);


                obj.show(1).delay((delay + 0.02) * 1000).hide(1, function () {
                    $(obj).remove();
                    $("#lblcirpartsCount").html(GPLIST.ecpartfinallist.length);
                    $("#lblcirpartsCount").css('display', 'block');
                    $(".clsbomicon").effect("shake", { times: 2, distance: 5 }, 500);
                });
            });

        });
        $(window).on("load", function () {

            //---------------Load List data from session storage
            if (typeof (Storage) !== "undefined") {
                if (localStorage.ecparts) {
                    var data = JSON.parse(localStorage.ecparts);
                    GPLIST.ecpartlist = data.Parts;
                    if (typeof (GPLIST.ecpartlist) !== "undefined") {
                        if (GPLIST.ecpartlist.length > 0) {
                            $("#callaction").css('display', 'block');
                            $("#lblcirpartsCount").text(GPLIST.ecpartlist.length);
                            $("#lblcirpartsCount").css('display', 'block');
                            $(".bomcartlabel").css('display', 'inline-block');
                            $("img#imgcoll").attr("src", "/shop/assembly/resources/colle_3g.png");
                        }
                        else {
                            $("#callaction").css('display', 'none');
                            $(".bomcartlabel").css('display', 'none');
                            $("#lblcirpartsCount").text("");
                            $("#lblcirpartsCount").css('display', 'none');
                            $("#imgcoll").attr("src", "/shop/assembly/resources/colle_3b.png");
                        }
                    }
                    else {
                        $("#callaction").css('display', 'none');
                        $(".bomcartlabel").css('display', 'none');
                        $("#lblcirpartsCount").text("");
                        $("#lblcirpartsCount").css('display', 'none');
                        $("#imgcoll").attr("src", "/shop/assembly/resources/colle_3b.png");
                    }
                }
            }

            var url = $(location).attr('href'),
            parts = url.split("/"),
            categoryname = parts[parts.length - 1];
            if (categoryname != "ecparts") {
                GPLIST.loadurlparm(categoryname);
                GPLIST.loaddata();
            }
            else {
                GPLIST.loaddata();
            }


        });

        $('#ecbomlist').on('click', 'tbody td:first-child', function (e) {
            var target = $(e.target);
            if (target.length > 0) {
                var chkName = target[0].name;
                if (chkName == 'chkpart') {
                    var partlist = [];
                    var table = $('#ecbomlist').DataTable();
                    partlist = table.row(this.closest('tr')).data();
                    if (e.target.checked == true)//if selected then add in list
                    {
                        GPLIST.ecpartlist.push({
                            imgurl: partlist[1],
                            mpn: partlist[2],
                            category: partlist[3],
                            descr: partlist[4],
                            ipcname: partlist[5],
                            stock: partlist[6],
                            rohs_compliant: partlist[7],
                            manufacturer: partlist[8],
                            datasheet_url: partlist[9],
                            ft_altium: partlist[10],
                            ft_eagle: partlist[11],
                            ft_fpx: partlist[12],
                            ft_kicad: partlist[13],
                            wrl_3d: partlist[14],
                            is_polarized: partlist[15],
                            is_poolable: partlist[16],
                            mount_type: partlist[17],
                            is_generic: partlist[18],
                            part_id: partlist[19],
                            category_id: partlist[20],
                            data_Id: partlist[21],
                            data_Id_0: partlist[22]
                        })
                    }
                    else {
                        //if not selected then remove item from list by part_id  
                        for (i = GPLIST.ecpartlist.length - 1; i >= 0; --i) {
                            if (GPLIST.ecpartlist[i]["part_id"] === partlist[19]) {
                                GPLIST.ecpartlist.splice(i, 1);
                            }
                        }
                    }

                    var strjson = "{\"Parts\":" + JSON.stringify(GPLIST.ecpartlist) + "}";
                    localStorage.setItem('ecparts', strjson);

                    if (GPLIST.ecpartlist.length > 0) {
                        $("#callaction").css('display', 'block');
                    }
                    else {
                        $("#callaction").css('display', 'none');
                        $(".bomcartlabel").css('display', 'none');
                        $("#lblcirpartsCount").text("");
                        $("#lblcirpartsCount").css('display', 'none');
                        $("#imgcoll").attr("src", "/shop/assembly/resources/colle_3b.png");
                    }
                }
            }
        });

        $('#ecbomlist').on('click', 'tbody td:last-child', function (e) {
            var target = $(e.target);
            if (target.length > 0) {
                if (e.target.classList.contains("dataafterlogin")) {
                    $("#lblmsg").html("Media can be downloaded from your saved BoM.");
                    $('#msgModal').modal('show');
                }
            }
        });

        $('#ecbomlist').on('click', 'tbody td', function (e) {
            var target = $(e.target);
            if (target[0].id == "searchdefault") {
                $('#txtsearch').val(target[0].innerText);
                window.location.href = "/shop/assembly/ecparts/category?keywords=USB Type B";
            }
            else if (target[0].id == "searchdefault1") {
                $('#txtsearch').val("");
                window.location.href = "/shop/assembly/ecparts/trimmer";
            }
        });

        $('#collectbtn').click(function () {
            $("img#imgcoll").attr("src", "/shop/assembly/resources/colle_3g.png");
            if ($(".chkbox").find("input[type=checkbox]").is(':checked')) {
                $(this).prop('checked', false);
            }
            GPLIST.ecpartfinallist = [];
            GPLIST.ecpartfinallist = GPLIST.ecpartlist.slice();
            //$("#lblcirpartsCount").html(GPLIST.ecpartfinallist.length);
            //$("#lblcirpartsCount").css('display', 'block');
            $(".bomcartlabel").css('display', 'inline-block');
            var strjson = "{\"Parts\":" + JSON.stringify(GPLIST.ecpartfinallist) + "}";
            localStorage.setItem('ecparts', strjson);

        });

        //$('#createList').click(function () {
        //    $("#btnCreateListSaveBOM").hide();
        //    $("#btnsaveList").show();
        //    $('#list-detail').hide();
        //    $('#modalcreatelist').modal('show');
        //});

        //$('#btnsaveList').click(function () {
        //    GPLIST.saveList();
        //});
        $('#btnCreateListSaveBOM').click(function () {
            $("#btnCreateListSaveBOM").hide();
            $("#divmdlBody").hide();
            $("#divProcessingBOM").show();
            GPLIST.saveBOMFile();
        });



        $('#imgcoll').click(function () {
            GPLIST.redirectnextpage();
        });
        $('#lblcirpartsCount').click(function () {
            GPLIST.redirectnextpage();
        });
        $('.bomcartlabel').click(function () {
            GPLIST.redirectnextpage();
        });

        //$('#btnsearch').click(function () {
        //    GPLIST.redirecturlparm("");

        //});
        //$("#txtsearch").keydown(function (event) {

        //    if (event.which == 13) {
        //        event.preventDefault();
        //        GPLIST.redirecturlparm("");
        //    }
        //});

        //$("#txtmdsearch").keydown(function (event) {
        //    if (event.which == 13) {
        //        event.preventDefault();
        //        GPLIST.redirecturlmdparm();
        //        //GPLIST.loadPopupdata();
        //    }
        //});
        //$('.ddlcategorycss').on('change', function (e) {
        //    $("#ddlallcategorynew").css("color", "");
        //    $(".ddlcategorycss").css("color", "");
        //    var value = e.target.value;
        //    $("#ddlallcategorynew").val(value);
        //});
        $('#exporttocsv').click(function () {
            var html = document.querySelector("#ecbomlist").outerHTML;
            GPLIST.export_table_to_csv(html, "ecparts.csv");

        });
        $('#exporttocsvbottom').click(function () {
            $('#exporttocsv').trigger("click");
        });

        //$('.rohschk').click(function () {
        //    if (this.checked) {
        //        $(".rohschk").attr("disabled", true);
        //        $(this).removeAttr("disabled");
        //    }
        //    else {
        //        $(".rohschk").removeAttr("disabled");
        //    }
        //});



        //$("#iframebom").contents().find("head").append($("<style type='text/css'> .bom-body{ overflow-y: auto; } .divTable { overflow-x: scroll; height: 50%!important;} .divTable::-webkit-scrollbar { width: 5px; } ::-webkit-scrollbar-track { background: #b9b7b7;  }" +
        //            +" " +
        //            " ::-webkit-scrollbar-thumb { background: #555; } </style>"));

        $('#uploadbom').click(function () {
            $("#iframebom").empty();
            var bomsessionid = "";
            bomsessionid = $('#ctl00_mainContent_hdbomsessionId').val();
            $('#myModalimport').modal('show');
            $('#iframebom').attr('src', '/shop/assembly/cmbomImport.aspx?isfromsearchui=true&bomsessionid=' + bomsessionid);
            setTimeout(function () {
                $("#iframebom").contents().find("head").append($("<style type='text/css'> .bom-body{ overflow-y: auto; } .divTable { overflow-x: scroll; height: 50%!important;} </style>"));
            }, 3000);
        });

        $('body').click(function () {
            $('#list-detail').hide();
        });

        $('#Continue').click(function (event) {

            $("#btnCreateListSaveBOM").show();
            $("#btnsaveList").hide();

            $('#modalcreatelist').modal('show');
            // GPLIST.loaddata("1");
            event.stopPropagation();
        });


        $('#list-detail').click(function (event) {
            event.stopPropagation();
        });
        $('.part-save').click(function (event) {
            GPLIST.savePartInList();
            event.stopPropagation();
        });
        //$('#spmodal').click(function () {
        //    var txtval = $('#txtsearch').val();
        //    if (txtval != "") {
        //        $('#txtmdsearch').val(txtval);
        //    }

        //    var ddlvalue = $('#ddlallcategory option:selected').text();
        //    if (ddlvalue != "" && ddlvalue != "All categories") {
        //        $("#ddlcategory2 option").attr('selected', true);
        //        $('#ddlcategory2 option').filter(function () {
        //            return ($(this).text() == ddlvalue);
        //        }).prop('selected', true);
        //        $(".ddlcategorycss").css("color", "");
        //    }
        //    else {
        //        $(this).css("color", "#555");
        //    }

        //    $('#myModal').modal('show');
        //});
        //$('#btnmdsearch').click(function () {
        //    GPLIST.redirecturlmdparm();
        //    //GPLIST.loadPopupdata();
        //});
    },

    addToLIst: function (element) {
       
        // $(element).css('left', event.offsetleft);
        $("#partId").val(element.attributes['id'].value);
        $("#list-detail").show('show');
        var t = $(element).offset().top;
        var l = $(element).offset().left;
        $("#list-detail").css({ top: (t - element.offsetTop - 35), left: (l - element.offsetLeft) });
        event.stopPropagation();
    },
    redirecturlparm: function (mpn) {
         
        var category = $('#ddlallcategorynew').children("option:selected").attr('catattr');
        if (mpn != "") {
            $('#txtsearch').val(mpn);
        }
        var txtvalue = $('#txtsearch').val();
        if (typeof category === "undefined") {
            if (txtvalue.trim() != "") {
                window.location.href = "/shop/assembly/ecparts/?keywords=" + txtvalue;
            }
            else {
                window.location.href = "/shop/assembly/ecparts";
            }
        }
        else {
            if (txtvalue.trim() != "") {
                window.location.href = "/shop/assembly/ecparts/?category=" + category + "&keywords=" + txtvalue;
            }
            else {
                window.location.href = "/shop/assembly/ecparts/?category=" + category;
            }
        }
    },
    redirecturlmdparm: function () {
        var category = $('#ddlallcategorynew').children("option:selected").attr('catattr');
        var txtvalue = $('#txtmdsearch').val();
        var extraparm = "";
        $(".chkbox").find("input[type=checkbox]").each(function () {
            if ($(this).is(':checked') == true) {
                exts = 1;
                var key = $(this).val();
                extraparm += "&" + key + "=" + 1;
            }
        });

        $(".chkbox").find("input[type=radio]").each(function () {
            if ($(this).is(':checked') == true) {
                exts = 1;
                var key = $(this).val();
                extraparm += "&" + key + "=" + 1;
            }
        });
        if (typeof category === "undefined") {
            if (txtvalue.trim() != "") {
                window.location.href = "/shop/assembly/ecparts/?keywords=" + txtvalue + "" + extraparm;
            }
            else {
                extraparm = extraparm.substring(1);
                window.location.href = "/shop/assembly/ecparts/?" + extraparm;
            }
        }
        else {
            if (txtvalue.trim() != "") {
                window.location.href = "/shop/assembly/ecparts/?category=" + category + "&keywords=" + txtvalue + "" + extraparm;
            }
            else {
                extraparm = extraparm.substring(1);
                window.location.href = "/shop/assembly/ecparts/?category=" + category + "&" + extraparm;
            }

        }
    },
    redirectnextpage: function () {
        var imgsrc = $('#imgcoll').attr('src');
        if (imgsrc != "/shop/assembly/resources/colle_3g.png") {
            return;
        }
        var strjson = "{\"Parts\":" + JSON.stringify(GPLIST.ecpartfinallist) + "}";
        sessionStorage.setItem('ecparts', strjson);
        window.open("/shop/assembly/bom.aspx", '_blank');
    },
    savePartInList: function () {
        var patrId = $("#partId").val();
        var partlist = [];
        var table = $('#ecbomlist').DataTable();
        partlist = table.row($("#" + patrId).closest('tr')).data();
        GPLIST.ecpartlist.push({
            imgurl: partlist[1],
            mpn: partlist[2],
            category: partlist[3],
            descr: partlist[4],
            ipcname: partlist[5],
            stock: partlist[6],
            rohs_compliant: partlist[7],
            manufacturer: partlist[8],
            datasheet_url: partlist[9],
            ft_altium: partlist[10],
            ft_eagle: partlist[11],
            ft_fpx: partlist[12],
            ft_kicad: partlist[13],
            wrl_3d: partlist[14],
            is_polarized: partlist[15],
            is_poolable: partlist[16],
            mount_type: partlist[17],
            is_generic: partlist[18],
            part_id: partlist[19],
            category_id: partlist[20],
            data_Id: partlist[21],
            data_Id_0: partlist[22]
        })
    },
    saveList: function () {
        var url = "/shop/assembly/ecparts.aspx/SaveList";
        var postData = {
            cId: GPLIST.cId,
            listName: $("#txtListName").val(),
            description: $("#txtlistDescription").val(),
            isDefault: "0"
        }
        Core.post(url, postData, function (result) {
            var data = result;
            if (data.Status == 0) {

                return;
            }
            else {
                var markup = '<tr><td><a lid="' + data.ListId + '" class="part-save"><i class="fa fa-star"></i>' + data.ListName + '</a></td></tr>';
                $("#tblList tbody").append(markup);
                $("#txtListName").val("");
                $("#txtlistDescription").val("");
                $('#modalcreatelist').modal('hide');

            }
        });
    },


    loadurlparm: function (categoryname) {
        categoryname = GPLIST.getUrlParameter('category');

        //----------------Set Dropdown color and value

        if (categoryname != "") {
            $("[catattr=" + categoryname + "]").addClass("bgcolorset");
            var value = $("[catattr=" + categoryname + "]").attr("value")
        }

        var searchvalue = "";
        var keywords = GPLIST.getUrlParameter('keywords');
        if (typeof keywords !== "undefined") {
            $('#txtsearch').val(keywords);
        }

        var search = {};
        var url = $(location).attr('href');
        var searchIndex = url.indexOf("?");
        if (searchIndex != -1) {
            var sPageURL = url.substring(searchIndex + 1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] != "keywords") {
                    search[sParameterName[0]] = sParameterName[1];
                    //---------select checkbox
                    $(".chkbox").find("input[type=checkbox]").each(function () {
                        var key = $(this).val();
                        if (key == sParameterName[0]) {
                            $(this).prop('checked', true);

                            //-----------Check for rohs checkbox enable disable
                            if ($(this).hasClass("rohschk")) {
                                $(".rohschk").attr("disabled", true);
                                $(this).removeAttr("disabled");
                            }
                        }
                    });
                }
            }
        }

        $("#ddlallcategorynew").css("color", "");
        $(".ddlcategorycss").css("color", "");
        $("#ddlallcategorynew").val(value);
        $('#ddlallcategorynew option[value=' + value + ']').attr('selected', 'selected');
        $('.ddlcategorycss option[value=' + value + ']').attr('selected', 'selected');
        $(".ddlcategorycss").val(value);
        $('#ddlallcategorynew option[value=' + value + ']').attr('selected', 'selected');
    },
    loaddata: function (fromFile) {

        var searchData = [];

        var isMulti = "0";
        if (fromFile == "1") {
            var lines = $('#txtfile').val().split('\n');
            for (var i = 0; i < lines.length; i++) {
                //code here using lines[i] which will give you each line
                var searchNode = {}
                var mpnList = lines[i];
                searchNode["descr"] = mpnList;
                searchNode["index"] = 0;
                searchNode["start"] = 0;
                searchNode["length"] = 50;

                var spec = BOMUtility.sanatizeKeyword(mpnList, 1);

                searchNode["specs"] = spec.specs;
                searchData.push(searchNode);
            }
            isMulti = "1";
        }
        else {
            var searchNode = {}
            var txtvalue = $('#txtsearch').val();
            $('#txtmdsearch').val("");
            var ddlval = $('#ddlallcategorynew option:selected').attr("value");

            $("#spnsearchFor").text(txtvalue);

            searchNode["descr"] = txtvalue;
            searchNode["category"] = ddlval;

            if ($("#chkstock").is(':checked') == true) {
                searchNode["stock"] = true;
            }
            if ($("#chkondemand").is(':checked') == true) {
                searchNode["ondemand"] = true;
            }
            if ($("#chkdatasheet").is(':checked') == true) {
                searchNode["datasheet"] = true;
            }
            if ($("#chkfootprints").is(':checked') == true) {
                searchNode["footprints"] = true;
            }
            if ($("#chk3dmodel").is(':checked') == true) {
                searchNode["3DModel"] = true;
            }
            if ($("#chkrohscompliant").is(':checked') == true) {
                searchNode["rohscompliant"] = true;
            }
            if ($("#chknonrohscomliant").is(':checked') == true) {
                searchNode["nonrohscomliant"] = true;
            }

            searchNode["index"] = 0;
            searchNode["start"] = 0;
            searchNode["length"] = 50;

            var spec = BOMUtility.sanatizeKeyword(txtvalue, 1);

            searchNode["specs"] = spec.specs;
            searchData.push(searchNode);
        }

        var postsearchData = JSON.stringify(searchData);
        //var postsearchData = JSON.stringify(searchNode);


        GPLIST.bindDataTable(txtvalue, postsearchData, isMulti);

        //var value = "";
        //var search = {}
        //var txtvalue = $('#txtsearch').val();
        //$('#txtmdsearch').val("");
        //var ddlval = $('#ddlallcategorynew option:selected').attr("value");
        //if (txtvalue != "") {
        //    value = txtvalue;
        //}
        //if (ddlval != "" && ddlval != "All categories") {
        //    var key = "category";
        //    search[key] = ddlval;
        //}
        //$(".chkbox").find("input[type=checkbox]").each(function () {
        //    if ($(this).is(':checked') == true) {
        //        exts = 1;
        //        var key = $(this).val();
        //        search[key] = exts
        //    }
        //});
        //GPLIST.bindDataTable(value, search);

    },
    getUrlParameter: function (sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    },
    addfeatures: function (partId) {
        //<span id="btnFav" class="fav"><i class="fa fa-star-o" aria-hidden="true"></i>FAV</span> 
        return '<div class="features"> <span onclick="GPLIST.addToLIst(this)" id="btnList-' + partId + '" partId="' + partId + '" class="list"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Add to list</span></div>'
    },
    loadPopupdata: function () {
        var searchNode = {}

        var txtvalue = $('#txtmdsearch').val();
        $('#txtmdsearch').val("");
        var ddlval = $('#ddlallcategorynew option:selected').attr("value");

        searchNode["descr"] = txtvalue;
        searchNode["category"] = ddlval;

        if ($("#chkstock").is(':checked') == true) {
            searchNode["stock"] = true;
        }
        if ($("#chkondemand").is(':checked') == true) {
            searchNode["ondemand"] = true;
        }
        if ($("#chkdatasheet").is(':checked') == true) {
            searchNode["datasheet"] = true;
        }
        if ($("#chkfootprints").is(':checked') == true) {
            searchNode["footprints"] = true;
        }
        if ($("#chk3dmodel").is(':checked') == true) {
            searchNode["3DModel"] = true;
        }
        if ($("#chkrohscompliant").is(':checked') == true) {
            searchNode["rohscompliant"] = true;
        }
        if ($("#chknonrohscomliant").is(':checked') == true) {
            searchNode["nonrohscomliant"] = true;
        }

        searchNode["start"] = 0;
        searchNode["length"] = 50;

        var spec = BOMUtility.sanatizeKeyword(txtvalue, 1);

        searchNode["specs"] = spec.specs;
        searchData.push(searchNode);

        var postsearchData = JSON.stringify(searchData);
        $(".topbtn").css('display', 'block');
        $(".bottumbtn").css('display', 'block');
        $("body").removeClass("disableScroll");
        $('#myModal').modal('hide');
        $('#myModal').css("display", "none");
    },
    download_csv: function (csv, filename) {
        var csvFile;
        var downloadLink;
        csvFile = new Blob([csv], { type: "text/csv" });
        downloadLink = document.createElement("a");
        downloadLink.download = filename;
        downloadLink.href = window.URL.createObjectURL(csvFile);
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
        downloadLink.click();
    },
    init: function (event) {

        GPLIST.events();
    }
}

$(document).ready(function () {
    $("title").text("Eurocircuits :: Part search");
    $("#ctl00_hrfBasket").find("img").attr('src', '../assembly/resources/collect_w.png');
    $("#ctl00_lblCount").html(GPLIST.partCount);
    $("#spnlng").hide();
    $("#Span4").hide();
    $("#ddlallcategorynew").css("color", "#555");
    $(".ddlcategorycss").css("color", "#555");
    GPLIST.init();
});

//var Core = {
//    getParameterByName: function (name) {
//        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
//        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
//            results = regex.exec(location.search);
//        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
//    },
//    post: function (url, postData, callback, contentType) {
//        processData = true;
//        data = postData;
//        if (contentType || contentType === undefined) {
//            contentType = "application/json; charset=utf-8";
//            data = JSON.stringify(postData)
//        }
//        else {
//            processData = false;
//            contentType = false
//        }
//        $('.pageloader, .loaderBG').removeClass('hide');
//        $.ajax({
//            url: url,
//            type: "POST",
//            data: data,
//            contentType: contentType,
//            processData: processData,
//            success: function (data) {
//                $('.pageloader,.loaderBG').addClass('hide');
//                $('.btnNext').removeClass('hide');
//                if (callback) {
//                    if (url.toLowerCase().indexOf(".ashx") > 0) {
//                        callback($.parseJSON(data));
//                    }
//                    else {
//                        callback($.parseJSON(data.d));
//                    }
//                }
//            },
//            error: function (errorData) {
//                $('.pageloader,.loaderBG').addClass('hide');
//                console.log(errorData);
//                if (callback) {
//                    callback({ Status: 0 });
//                }
//            }
//        });
//    },
//    showMessage: function (id, error, msg, time) {
//        $('#' + id).text(msg).removeClass()

//        if (error) {
//            $('#' + id).html(msg).addClass('error').show();
//        } else {
//            $('#' + id).html(msg).addClass('success').show();
//        }

//        if (time) {
//            setTimeout(function () {
//                $('#' + id).hide()
//            }, time * 1000);
//        }
//    }
//}