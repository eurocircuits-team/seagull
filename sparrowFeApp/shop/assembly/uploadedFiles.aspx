﻿<%@ Page Language="C#" AutoEventWireup="true" Debug="true" %>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Collections.Specialized" %>
<%@ Import Namespace="System.Net" %> 

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        string entitynr = Request["id"] ?? "";
        string deleteUploadedBOM = Request["deleteUploadedBOM"] ?? "0";
        string deleteUploadedCPL = Request["deleteUploadedCPL"] ?? "0";

        if (entitynr != "")
        {
            if (deleteUploadedBOM == "1")
            {
                DeleteBOMData(entitynr, "CM");
            }
            if (deleteUploadedCPL == "1")
            {
                DeleteBOMData(entitynr, "CPL");
            }
            BuildXMLString(entitynr);
        }
    }

    public void DeleteBOMData(string entitynr, string fileType)
    {
        try
        {
            //string FolderPath = "";
            //string filePath = "";
            //EC09WebApp.API.gd objgd = new EC09WebApp.API.gd();
            //const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
            //string strsql = string.Format("exec spVisualizerfilepath '{0}','' ", entitynr);
            //DataSet ds = objgd.GetData(strsql, Keydata);
            //if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(ds.Tables[0].Rows[0]["FolderPath"]) != "")
            //{
            //    if (IsImpersonation())
            //    {
            //        FolderPath = Convert.ToString(ds.Tables[0].Rows[0]["FolderPath"]);

            //        string[] filePaths = Directory.GetFiles(FolderPath);
            //        if (Directory.Exists(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType))
            //        {
            //            filePaths = Directory.GetFiles(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType);
            //            if (filePaths != null)
            //            {
            //                if (fileType == "CM")
            //                {
            //                    if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_")).Count() > 0)
            //                    {
            //                        filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_"))[0] ?? null;
            //                        File.Delete(filePath);
            //                        insertDeleteFileLog("UPLOADED_BOM_" + entitynr, filePath);
            //                    }
            //                }
            //                else
            //                {
            //                    if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_")).Count() > 0)
            //                    {
            //                        filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_"))[0] ?? null;
            //                        File.Delete(filePath);
            //                        insertDeleteFileLog("UPLOADED_CPL_" + entitynr, filePath);
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (fileType == "CM")
            //            {
            //                if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_")).Count() > 0)
            //                {
            //                    filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_"))[0] ?? null;
            //                    File.Delete(filePath);
            //                    insertDeleteFileLog("UPLOADED_BOM_" + entitynr, filePath);
            //                }
            //            }
            //            else
            //            {
            //                if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_")).Count() > 0)
            //                {
            //                    filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_"))[0] ?? null;
            //                    File.Delete(filePath);
            //                    insertDeleteFileLog("UPLOADED_CPL_" + entitynr, filePath);
            //                }
            //            }
            //        }
            //    }
            //}

        }
        catch
        {
        }

    }


    public void insertDeleteFileLog(string msg, string path)
    {
        try
        {

        }
        catch
        {
        }


    }
    private void BuildXMLString(string entitynr)
    {
        try
        {
            string BomFileName = GetFilePath(entitynr, "CM", true);
            string CPLFileName = GetFilePath(entitynr, "CPL", true);

            string UserName = "";
            string From = "";
            string Uploadedon = "";
            try
            {
                if(BomFileName.Trim() != "" || CPLFileName.Trim() != "")
                {
                    string dbBomFileName = BomFileName;
                    string dbCPLFileName = CPLFileName;

                    if (dbBomFileName.StartsWith("UPLOADED_BOM_"))
                        dbBomFileName = dbBomFileName.Replace("UPLOADED_BOM_","");

                    if (dbBomFileName.StartsWith("SUBMITTED_BOM_"))
                        dbBomFileName = dbBomFileName.Replace("SUBMITTED_BOM_","");

                    if (dbCPLFileName.StartsWith("UPLOADED_CPL_"))
                        dbCPLFileName = dbCPLFileName.Replace("UPLOADED_CPL_","");

                    if (dbCPLFileName.StartsWith("SUBMITTED_CPL_"))
                        dbCPLFileName = dbCPLFileName.Replace("SUBMITTED_CPL_","");

                    //EC09WebApp.API.gd objgd = new EC09WebApp.API.gd();
                    //const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
                    //string strsql = string.Format("exec sp_GetBOMCPLUplodedHistoryDetail '{0}','{1}','{2}'", entitynr,dbBomFileName.Trim(),dbCPLFileName.Trim());
                    //DataSet ds = objgd.GetData(strsql, Keydata);
                    //if (ds != null && ds.Tables[0].Rows.Count > 0)
                    //{
                    //    UserName = ds.Tables[0].Rows[0]["Username"].ToString();
                    //    From = ds.Tables[0].Rows[0]["Origin"].ToString();
                    //    Uploadedon = ds.Tables[0].Rows[0]["Uploadedon"].ToString();
                    //}

                    string filename = "";
                    if(dbBomFileName.Trim() != "" &&  dbCPLFileName.Trim() != "")
                    {
                        filename = dbBomFileName;
                    }
                    else if(dbBomFileName.Trim() == "" &&  dbCPLFileName.Trim() != "")
                    {
                        filename = dbCPLFileName;
                    }
                    else if(dbBomFileName.Trim() != "" &&  dbCPLFileName.Trim() == "")
                    {
                        filename = dbBomFileName;
                    }


                    Database.DA DA = new Database.DA();
                    string strsql = string.Format("select created_date,created_by from sales_bomfile_history where file_name ='{0}' and entity_num = '{1}' order  by id desc", entitynr,filename.Trim());
                    DataSet ds = DA.GetDataSet(strsql);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        UserName = "";
                        long lngCreateBy = 0;
                        if(lngCreateBy != 0)
                        {
                            long.TryParse(ds.Tables[0].Rows[0]["created_by"].ToString(),out lngCreateBy );
                            string strUsername = string.Format("select username from auth_user where id = {0}", lngCreateBy);
                            DataSet dsUsername = DA.GetDataSet(strUsername);
                            if(dsUsername != null && dsUsername.Tables.Count > 0 && dsUsername.Tables[0].Rows.Count > 0)
                            {
                                UserName = dsUsername.Tables[0].Rows[0]["username"].ToString();
                            }
                        }
                        From = "FEApp";
                        Uploadedon = ds.Tables[0].Rows[0]["created_date"].ToString();
                    }
                }
            }
            catch { }

            string str = "<Uploaded />";
            if (BomFileName != "" && CPLFileName != "")
            {
                str = "<Uploaded bom=\"" + BomFileName + "\" cpl=\"" + CPLFileName + "\" user_name=\"" + UserName + "\" from=\"" + From + "\" uploaded_on=\"" + Uploadedon + "\" />";
            }
            else if (BomFileName == "" && CPLFileName != "")
            {
                str = "<Uploaded cpl=\"" + CPLFileName + "\" user_name=\"" + UserName + "\" from=\"" + From + "\" uploaded_on=\"" + Uploadedon + "\" />";
            }
            else if (BomFileName != "" && CPLFileName == "")
            {
                str = "<Uploaded bom=\"" + BomFileName + "\" user_name=\"" + UserName + "\" from=\"" + From + "\" uploaded_on=\"" + Uploadedon + "\" />";
            }
            Response.ClearHeaders();
            Response.AddHeader("content-type", "text/xml");
            Response.Write(str);
            Response.End();
        }
        catch  (Exception ex)
        {
            string str = ex.Message.ToString();
        }
    }


    public string GetFilePath(string entitynr, string fileType, bool checkCustFile)
    {
        string FolderPath = "";
        string filePath = "";
        //EC09WebApp.API.gd objgd = new EC09WebApp.API.gd();
        //const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
        //string strsql = string.Format("exec spVisualizerfilepath '{0}','' ", entitynr);
        //DataSet ds = objgd.GetData(strsql, Keydata);
        //if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(ds.Tables[0].Rows[0]["FolderPath"]) != "")
        //{

        FolderPath = sparrowFeApp.core.Common.getFilePath(entitynr);
        if (FolderPath != "")
        {
            System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = null;
            impersonateOnFileServer = sparrowFeApp.core.Impersonation.ImpersonateFileServer(sparrowFeApp.core.Constant.FileServerUsername, sparrowFeApp.core.Constant.FileServerPassword, sparrowFeApp.core.Constant.FileServerDomain);
            if (impersonateOnFileServer != null)
            {
                string[] filePaths = Directory.GetFiles(FolderPath);
                if (Directory.Exists(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType))
                {
                    filePaths = Directory.GetFiles(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType);
                    if (filePaths != null)
                    {
                        if (fileType == "CM")
                        {
                            if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_")).Count() > 0)
                            {
                                filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_"))[0] ?? null;
                            }
                            else if (Array.FindAll(filePaths, x => x.Contains("SUBMITTED_BOM_")).Count() > 0)
                            {
                                filePath = Array.FindAll(filePaths, x => x.Contains("SUBMITTED_BOM_"))[0] ?? null;
                            }
                        }
                        else
                        {
                            if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_")).Count() > 0)
                            {
                                filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_"))[0] ?? null;
                            }
                            else if (Array.FindAll(filePaths, x => x.Contains("SUBMITTED_CPL_")).Count() > 0)
                            {
                                filePath = Array.FindAll(filePaths, x => x.Contains("SUBMITTED_CPL_"))[0] ?? null;
                            }
                        }
                    }
                }
                else
                {
                    if (fileType == "CM")
                    {
                        if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_")).Count() > 0)
                        {
                            filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_"))[0] ?? null;
                        }
                        else if (Array.FindAll(filePaths, x => x.Contains("SUBMITTED_BOM_")).Count() > 0)
                        {
                            filePath = Array.FindAll(filePaths, x => x.Contains("SUBMITTED_BOM_"))[0] ?? null;
                        }
                    }
                    else
                    {
                        if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_")).Count() > 0)
                        {
                            filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_"))[0] ?? null;
                        }
                        else if (Array.FindAll(filePaths, x => x.Contains("SUBMITTED_CPL_")).Count() > 0)
                        {
                            filePath = Array.FindAll(filePaths, x => x.Contains("SUBMITTED_CPL_"))[0] ?? null;
                        }
                    }
                }
            }
        }
        return Path.GetFileName(filePath);
    }
</script>
