﻿<%@ Page Language="C#" AutoEventWireup="true" Debug="true" %>

<%@ Import Namespace="System.IO" %>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        string internalReq = Request.QueryString["internal"] ?? "0";
        string opencomponentsreq = Request.QueryString["opencomponentsearch"]?? "";
        if (opencomponentsreq == "")
        {

            string searchInput = "";
            using (StreamReader streamReader = new StreamReader(Request.InputStream))
            {
                searchInput = streamReader.ReadToEnd().Trim();
            }
            dynamic jsonData = Newtonsoft.Json.JsonConvert.DeserializeObject(searchInput);
            string keyword = jsonData.keyword.ToString().Trim();

            List<Dictionary<string, object>> search = new List<Dictionary<string, object>>();
            Dictionary<string, object> searchInfo = new Dictionary<string, object>();
            searchInfo.Add("index", 0);
            searchInfo.Add("mpn", keyword);
            searchInfo.Add("descr", keyword);
            searchInfo.Add("specs", jsonData.specs.ToString());
            searchInfo.Add("spn", keyword);
            searchInfo.Add("package", "");

            search.Add(searchInfo);

            string searchData = Newtonsoft.Json.JsonConvert.SerializeObject(search);
            string result = "";
            try
            {
                dynamic dtParts = sparrowFeApp.shop.assembly.CmBomImport.GetMatchedPartsV2(searchData, 2, 0, internalReq);
                result = Newtonsoft.Json.JsonConvert.SerializeObject(new { Status = 1, Parts = dtParts });
            }
            catch (Exception ex)
            {
                result = Newtonsoft.Json.JsonConvert.SerializeObject(new { Status = 0, Msg = ex.Message });
            }

            Response.Write(result);
        }
        else
        {
            try
            {
                string searchInput = "";
                using (StreamReader streamReader = new StreamReader(Request.InputStream))
                {
                    searchInput = streamReader.ReadToEnd().Trim();
                }
                string isMulti=  Request.QueryString["isMulti"] ?? "0";
                long page_start = 0;
                long page_length = 50;

                long.TryParse(Request.QueryString["start"] ?? "0",out page_length);

                long.TryParse(Request.QueryString["length"] ?? "50",out page_length);
                bool multiSearch = false;
                if (isMulti == "1")
                {
                    multiSearch = true;
                }
                string result = "";
                try
                {
                    searchInput = searchInput.Replace("3d_wrl", "wrl_3d");
                    searchInput = searchInput.Replace("3d_step", "step_3d");
                    int totalData = 0;
                    System.Data.DataTable dtParts = sparrowFeApp.shop.assembly.CmBomImport.GetopenComponentparts(searchInput,ref totalData,page_start,page_length,multiSearch);
                    if (dtParts != null)
                    {
                        try
                        {
                            dtParts.Columns["rel_id"].ColumnName = "partId";
                        }
                        catch { }
                    }
                    result = Newtonsoft.Json.JsonConvert.SerializeObject(new { Status = 1, Parts = dtParts, total=totalData });
                }
                catch (Exception ex)
                {
                    result = Newtonsoft.Json.JsonConvert.SerializeObject(new { Status = 0, Msg = ex.Message });
                }

                Response.Write(result);
            }
            catch {

            }
        }
    }
</script>
